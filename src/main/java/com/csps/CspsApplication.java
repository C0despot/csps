package com.csps;

import com.csps.gabriel.services.CompanyService;
import com.csps.gabriel.usefull.Session;
import org.modelmapper.ModelMapper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class CspsApplication  extends SpringBootServletInitializer {

	public static void main(String[] args) {
		SpringApplication.run(CspsApplication.class, args);
	}
	@Bean
	public Session session(){return new Session();}

	@Bean
	public ModelMapper modelMapper(){return new ModelMapper();}

	@Bean
	public CompanyService companyService(){return new CompanyService();}

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(CspsApplication.class);
	}
}
