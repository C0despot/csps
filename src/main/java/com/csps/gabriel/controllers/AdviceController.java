package com.csps.gabriel.controllers;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.ModelAttribute;

@org.springframework.web.bind.annotation.ControllerAdvice
public class AdviceController {

    @Value("${spring.datasource.url}")
    private String environment;


    @Value("${csps.app.version}")
    private String applicationVersion;

    @ModelAttribute("applicationVersion")
    public String getApplicationVersion() {
        return applicationVersion;
    }

    @ModelAttribute("environment")
    public String getEnvironment() {
        return environment;
    }
}