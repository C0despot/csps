package com.csps.gabriel.controllers;

import com.csps.gabriel.entities.Claim;
import com.csps.gabriel.repositories.ClaimRepository;
import com.csps.gabriel.services.FilterService;
import com.csps.gabriel.usefull.EntityFilter;
import com.csps.gabriel.usefull.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 *  Created by Jose A Rodriguez on 6/24/2017.
 */
@RestController
public class ClaimController {

    @Autowired
    ClaimRepository claimRepository;

    @Autowired
    FilterService filterService;

    @RequestMapping(value = "/save-claim-document", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody
    Response<Claim> saveDoc(@RequestBody Claim claim) {
        return new Response<>(this.claimRepository.save(claim), true, "Successfully saved", "SUCCESS");
    }

    @RequestMapping(value = "/get-all-claim", method = RequestMethod.GET)
    public @ResponseBody
    Response<List<Claim>> getAllDocuments() {
        List<Claim> claims = this.claimRepository.findAll();
        return new Response<>(claims, true, "Successful", "SUCCESS");
    }

    @RequestMapping(value = "/do-claim-filter", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody
    Response<?> getByFilters(@RequestBody EntityFilter filter) {
        List<Claim> claims = new ArrayList<>();
        switch (filter.getCriteria()){
            case 0: claims = this.claimRepository.findByDocumentNoContaining(filter.getSearchObject());
            break;
            case 1:
                claims = filterService.getClaimWithNameLike(filter.getSearchObject());
                break;
            case 2: claims = this.filterService.getClaimWithPolicyLike(filter.getSearchObject());
                break;
            default: break;
        }
        return new Response<List<?>>(claims, true, "SUCCESSFUL", "SUCCESS");
    }
}
