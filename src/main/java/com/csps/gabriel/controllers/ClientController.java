package com.csps.gabriel.controllers;

import com.csps.gabriel.entities.Client;
import com.csps.gabriel.entities.User;
import com.csps.gabriel.repositories.ClientRepository;
import com.csps.gabriel.usefull.Response;
import com.csps.gabriel.usefull.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 *  Created by Jose A Rodriguez on 5/13/2017.
 */
@RestController
public class ClientController {

    @Autowired
    ClientRepository clientRepository;

    @RequestMapping(value = "/save-client", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody
    Response<Client> saveClient(@RequestBody Client client) {

        client.setPhoneNumber(client.getPhoneNumber3());
        if (client.getPhoneNumber2()!=null && !client.getPhoneNumber2().equals(""))
            client.setPhoneNumber(client.getPhoneNumber2());
        if (client.getPhoneNumber1()!=null && !client.getPhoneNumber1().equals(""))
            client.setPhoneNumber(client.getPhoneNumber1());
        client.setFullName(client.getFirstName()+" "+client.getLastName());
        if (client.getClientType().equals("Empresa"))
            client.setFullName(client.getEnterprise());

        return new Response<Client>( clientRepository.save(client),true, "Cliente modificado");
    }

    @RequestMapping(value = "/get-all-clients-person", method = RequestMethod.GET)
    public @ResponseBody
    Response<List<Client>> allClientsPerson() {
        return new Response <List<Client>> (clientRepository.findAllByClientType("Persona Fisica"),true, "Success");
    }

    @RequestMapping(value = "/get-all-clients", method = RequestMethod.GET)
    public @ResponseBody
    Response<List<Client>> allClients() {
        return new Response <> (clientRepository.findAll(),true, "Success");
    }

    @RequestMapping(value = "/get-all-clients-company", method = RequestMethod.GET)
    public @ResponseBody
    Response<List<Client>> allClientsCompany() {
        return new Response <List<Client>> (clientRepository.findAllByClientType("Empresa"),true, "Success");
    }

    @RequestMapping(value = "/get-one/{clientId}", method = RequestMethod.GET)
    public @ResponseBody
    Response<Client> getOne(@PathVariable("clientId")Long id) {
        return new Response <> (clientRepository.findOne(id),true, "Success");
    }



}
