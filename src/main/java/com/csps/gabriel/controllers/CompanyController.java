package com.csps.gabriel.controllers;

import com.csps.gabriel.dtos.CompanyExtendedDto;
import com.csps.gabriel.entities.Branch;
import com.csps.gabriel.entities.Company;
import com.csps.gabriel.repositories.BranchRepository;
import com.csps.gabriel.services.CompanyService;
import com.csps.gabriel.usefull.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 *  Created by Jose A Rodriguez on 7/9/2017.
 */

@RestController
public class CompanyController {

    @Autowired
    CompanyService companyService;

    @Autowired
    BranchRepository branchRepository;

    @RequestMapping(value = "/save-company", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public
    @ResponseBody
    Response<?> saveReceivingDocument(@RequestBody CompanyExtendedDto companyExtended) {
        return this.companyService.saveCompany(companyExtended);
    }

    @RequestMapping(value = "/get-all-companies", method = RequestMethod.GET)
    public @ResponseBody
    Response<List<Company>> allCompanies() {
        return this.companyService.findAllCompanies();
    }

    @RequestMapping(value = "/get-company-by-id/{id}", method = RequestMethod.GET)
    public @ResponseBody
    Response<CompanyExtendedDto> getCompanyById(@PathVariable("id")Long id) {
        return this.companyService.findOne(id);
    }

    @RequestMapping(value = "/save-branch", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public
    @ResponseBody
    Response<?> saveBranch(@RequestBody String description) {
        Branch branch = new Branch();
        branch.setBranchDescription(description);
        branch = this.branchRepository.save(branch);
        return new Response<>(branch, true, "Successful");
    }

    @RequestMapping(value = "/get-all-branches", method = RequestMethod.GET)
    public @ResponseBody
    Response<List<Branch>> allBranches() {
        return new Response<>(this.branchRepository.findAll(), true, "Successful");
    }
}
