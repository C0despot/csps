package com.csps.gabriel.controllers;

import com.csps.gabriel.entities.Credit;
import com.csps.gabriel.entities.Invoice;
import com.csps.gabriel.repositories.CreditsRepository;
import com.csps.gabriel.repositories.InvoiceRepository;
import com.csps.gabriel.services.FilterService;
import com.csps.gabriel.usefull.EntityFilter;
import com.csps.gabriel.usefull.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 *  Created by Jose A Rodriguez on 6/24/2017.
 */
@RestController
public class CreditsController {

    @Autowired
    CreditsRepository creditsRepository;

    @Autowired
    InvoiceRepository invoiceRepository;

    @Autowired
    FilterService filterService;

    @RequestMapping(value = "/save-document", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody
    Response<Credit> saveDoc(@RequestBody Credit credit) {
        if (credit.getAmount()==0.0){
            return new Response<> (null, false, "ERROR");
        }
        Invoice invoice = this.invoiceRepository.findOne(credit.getInvoiceNumber());
        if (credit.getCreditId() == null) {
            invoice.setTotalPrime(invoice.getTotalPrime() - credit.getAmount());
        }else {
            invoice.setTotalPrime(invoice.getTotalPrime() -(credit.getAmount() - this.creditsRepository.findOne(credit.getCreditId()).getAmount()));
        }
        credit.setPrime(invoice.getTotalPrime());

        credit = this.creditsRepository.save(credit);
        if (credit.getDocumentNo()== null){
            credit.setDocumentNo("CRE-"+credit.getCreditId());
        }
        return new Response<>(this.creditsRepository.save(credit), true, "Successfully saved", "SUCCESS");
    }

    @RequestMapping(value = "/get-all-credit", method = RequestMethod.GET)
    public @ResponseBody
    Response<List<Credit>> getAllDocuments() {
        List<Credit> credits = this.creditsRepository.findAll();
        return new Response<>(credits, true, "Successful", "SUCCESS");
    }

    @RequestMapping(value = "/do-credit-filter", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody
    Response<?> getByFilters(@RequestBody EntityFilter filter) {
        List<Credit> credits = new ArrayList<>();
        switch (filter.getCriteria()){
            case 0: credits = this.creditsRepository.findByDocumentNoContaining(filter.getSearchObject());
            break;
            case 1: credits = filterService.getCreditWithNameLike(filter.getSearchObject());
                break;
            case 2: credits = this.filterService.getCreditWithPolicyLike(filter.getSearchObject());
                break;
            default: break;
        }
        return new Response<List<?>>(credits, true, "SUCCESSFUL", "SUCCESS");
    }
}

