package com.csps.gabriel.controllers;

import com.csps.gabriel.entities.*;
import com.csps.gabriel.repositories.*;
import com.csps.gabriel.usefull.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class EntityValidatorController {

    @Autowired
    PolicyRepository policyRepository;

    @Autowired
    ClientRepository clientRepository;

    @Autowired
    InvoiceRepository invoiceRepository;

    @Autowired
    ReceiptsRepository receiptsRepository;

    @Autowired
    CreditsRepository creditsRepository;

    @Autowired
    ClaimRepository claimRepository;

    @RequestMapping(value = "/validate-policy/{policyNo}", method = RequestMethod.GET)
    public @ResponseBody
    Response<Boolean> validatePolicy(@PathVariable("policyNo") String policyNo) {
        Policy policy = this.policyRepository.findByPolicyNumber(policyNo);
        return new Response<>(policy == null, true, "Successful", "SUCCESS");
    }

    @RequestMapping(value = "/validate-client/{clientId}", method = RequestMethod.GET)
    public @ResponseBody
    Response<Boolean> validateClient(@PathVariable("clientId") String clientId) {
        Client client = this.clientRepository.findByRncEqualsOrIdCardEquals(clientId, clientId);
        return new Response<>(client == null, true, "Successful", "SUCCESS");
    }

    @RequestMapping(value = "/validate-invoice/{invoiceNo}", method = RequestMethod.GET)
    public @ResponseBody
    Response<Boolean> validateInvoice(@PathVariable("invoiceNo") String invoiceNo) {
        Invoice invoice = this.invoiceRepository.findByInvoiceNumber(invoiceNo);
        return new Response<>(invoice == null, true, "Successful", "SUCCESS");
    }

    @RequestMapping(value = "/validate-receipt/{receiptNo}", method = RequestMethod.GET)
    public @ResponseBody
    Response<Boolean> validateReceipt(@PathVariable("receiptNo") String receiptNo) {
        Receipt receipt = this.receiptsRepository.findByReceiptNo(receiptNo);
        return new Response<>(receipt == null, true, "Successful", "SUCCESS");
    }

    @RequestMapping(value = "/validate-credit/{creditNo}", method = RequestMethod.GET)
    public @ResponseBody
    Response<Boolean> validateCredit(@PathVariable("creditNo") String creditNo) {
        Credit credit = this.creditsRepository.findByDocumentNo(creditNo);
        return new Response<>(credit == null, true, "Successful", "SUCCESS");
    }

    @RequestMapping(value = "/validate-claim/{claimNo}", method = RequestMethod.GET)
    public @ResponseBody
    Response<Boolean> validateClaim(@PathVariable("claimNo") String claimNo) {
        return new Response<>(this.claimRepository.findByDocumentNo(claimNo) == null, true, "Successful", "SUCCESS");
    }
}
