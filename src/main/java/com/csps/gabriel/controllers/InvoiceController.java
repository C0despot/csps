package com.csps.gabriel.controllers;

import com.csps.gabriel.entities.Credit;
import com.csps.gabriel.entities.Invoice;
import com.csps.gabriel.entities.Policy;
import com.csps.gabriel.repositories.InvoiceRepository;
import com.csps.gabriel.repositories.PolicyRepository;
import com.csps.gabriel.services.FilterService;
import com.csps.gabriel.usefull.EntityFilter;
import com.csps.gabriel.usefull.Response;
import org.apache.commons.collections.list.UnmodifiableList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.persistence.criteria.CriteriaBuilder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Jose A Rodriguez on 6/3/2017.
 */
@RestController
public class InvoiceController {

    @Autowired
    InvoiceRepository invoiceRepository;

    @Autowired
    FilterService filterService;

    @Autowired
    PolicyRepository policyRepository;

    @RequestMapping(value = "/save-invoice", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody
    Response<Invoice> saveInvoice(@RequestBody Invoice invoice) {
        invoice = this.invoiceRepository.save(invoice);
        if (invoice.getInvoiceNumber() == null) {
            invoice.setInvoiceNumber("FT-" + invoice.getInvoiceId() + "");
            invoice = this.invoiceRepository.save(invoice);
        }
        if (invoice.getInvRegType().equals("1")) {
            Policy policy = this.policyRepository.findOne(invoice.getPolicy());
            policy.setStartDate(invoice.getBeginningDate());
            policy.setEndDate(invoice.getDueDate());
            policy.setPrime(invoice.getTotalPrime());
            if (policy.getBranch() == 100000003) {
                policy.setStatus("1");
            } else {
                policy.setStatus("Vigente");
            }
            this.policyRepository.save(policy);
        }
        return new Response<>(invoice, true, "Invoice Saved");
    }

    @RequestMapping(value = "/get-invoice-by-invoice-number/{invoiceNumber}", method = RequestMethod.GET)
    public @ResponseBody
    Response<Invoice> findByNumber(@PathVariable("invoiceNumber") Long invoiceNumber) {
        Invoice invoice = this.invoiceRepository.findOne(invoiceNumber);
        if (invoice != null)
            return new Response<>(invoice, true, "Succeed", "SUCCESS");
        else
            return new Response<>(null, false, "Failure", "FAILS");
    }

    @RequestMapping(value = "/get-all-invoices", method = RequestMethod.GET)
    public @ResponseBody
    Response<List<Invoice>> getAllInvoices() {
        List<Invoice> invoice = this.invoiceRepository.findAll();
        return new Response<>(invoice, true, "Successful", "SUCCESS");
    }

    @RequestMapping(value = "/get-all-invoices/{page}", method = RequestMethod.GET)
    public @ResponseBody
    Response<Page<Invoice>> getAllInvoices(@PathVariable("page") int page) {
        Pageable pageable = new PageRequest(page, 9, new Sort(Sort.Direction.ASC, "dueDate"));
        Page<Invoice> invoice = this.invoiceRepository.findAllByTotalPrimeGreaterThan(5.0, pageable);
        return new Response<>(invoice, true, "Successful", "SUCCESS");
    }

    @RequestMapping(value = "/get-by-policy/{policy}", method = RequestMethod.GET)
    public @ResponseBody
    Response<List<Invoice>> getByPolicy(@PathVariable("policy") Long policy) {
        return new Response<>(invoiceRepository.findByPolicyLike(policy), true, "SUCCESSFUL", "SUCCESS");
    }

    @RequestMapping(value = "/do-invoice-filter", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody
    Response<?> getByFilters(@RequestBody EntityFilter filter) {
        List<Invoice> invoices = new ArrayList<>();
        switch (filter.getCriteria()) {
            case 0:
                invoices = this.invoiceRepository.findByInvoiceNumberContaining(filter.getSearchObject());
                break;
            case 1:
                invoices = filterService.getInvoiceWithNameLike(filter.getSearchObject());
                break;
            case 2:
                invoices = this.filterService.getInvoiceWithPolicyLike(filter.getSearchObject());
                break;
            default:
                break;
        }
        return new Response<List<?>>(invoices, true, "SUCCESSFUL", "SUCCESS");
    }

}

