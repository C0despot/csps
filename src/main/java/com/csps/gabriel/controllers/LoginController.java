package com.csps.gabriel.controllers;

import com.csps.gabriel.entities.User;
import com.csps.gabriel.repositories.UserRepo;
import com.csps.gabriel.usefull.Response;
import com.csps.gabriel.usefull.Session;
import com.csps.security.repository.UserRepository;
import com.google.gson.Gson;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

/**
 * Created by Jose A Rodriguez on 5/8/2017.
 */
@RestController
public class LoginController {

    @Autowired
    UserRepo userRepo;

    @Autowired
    UserRepository userRepository;

    @Autowired
    BCryptPasswordEncoder encoder;

    @Autowired
    ModelMapper modelMapper;

    @Autowired
    Gson gson;

    @Autowired
    private Session session;

    @RequestMapping(value = "/user", method = RequestMethod.GET)
    public Principal principal(Principal principal) {
        return principal;
    }

    @RequestMapping(value = "/sign-in", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody
    Response<String> doLogin(@RequestBody User user) {
        try {
            User users = userRepo.findOneByPasswordLikeAndUsernameLike(user.getPassword(), user.getUsername());
            session = gson.fromJson(gson.toJson(users), Session.class);
            Response response = new Response<>(session, true, "Login Successful");

            return users.isNotEmpty() ? response : new Response<>(false, "Can't find this username or password");
        } catch (Exception e) {
            return new Response<>(null, false, e.getMessage(), "FAILS");
        }
    }

    @RequestMapping(value = "/save-user", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody
    Response<com.csps.security.model.User> createUser(@RequestBody com.csps.security.model.User user) {
        if (this.userRepository.findByUsername(user.getUsername()) == null) {
            user.setPassword(encoder.encode(user.getPassword()));
            user.setPasswordConfirm(user.getPassword());
            this.userRepository.save(user);
            return new Response<>(user, true, "Usuario Guardado", "SUCCESS");
        } else if (user.getId() != null) {
            if (user.getPassword().equals("") || user.getPassword() == null) {
                user.setPassword(this.userRepository.findOne(user.getId()).getPassword());
            }
            user.setPassword(encoder.encode(user.getPassword()));
            user.setPasswordConfirm(user.getPassword());
            user = this.userRepository.save(user);
            user.setPassword("");
            return new Response<>(user, true, "Usuario Guardado", "SUCCESS");

        }
        return new Response<>(null, false, "Nombre de usuario ya existe", "WARN");
    }

    @RequestMapping(value = "/session", method = RequestMethod.GET)
    public @ResponseBody
    Response isLoggedIn() {

        org.springframework.security.core.userdetails.User userDetails = (org.springframework.security.core.userdetails.User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        try {
            return new Response<>(userDetails, true, "Logged");
        } catch (Exception e) {
            return new Response<>(false, "User must be logged in first");
        }
    }

    @RequestMapping(value = "/user/{username}", method = RequestMethod.GET)
    public @ResponseBody
    Response<?> getUser(@PathVariable("username") String username) {
        com.csps.security.model.User user = this.userRepository.findByUsername(username);
        user.setPassword("");
        return new Response<>(user, true, "User Found", "SUCCESS");
    }

    @RequestMapping(value = "/sign-out", method = RequestMethod.DELETE)
    public @ResponseBody
    Response doLogout() {
        session.flush();
        return new Response<>(true, "User logged out");
    }

    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public @ResponseBody
    Response logout() {
        session.flush();
        return new Response<>(true, "User logged out");
    }

    @RequestMapping(value = "/all-accounts", method = RequestMethod.GET)
    public @ResponseBody
    Response<List<?>> getAll() {
        return new Response<>(this.userRepository.findAll(), true);
    }

    @RequestMapping(value = "/get-account-by-id/{id}", method = RequestMethod.GET)
    public @ResponseBody
    Response<?> getOne(@PathVariable("id") Long id) {
        return new Response<>(this.userRepository.findOne(id), true);
    }

//    @GetMapping("login")
//    public String getMessage(Model model) {
//        model.addAttribute("name", "John");
//        return "redirect:login";
//    }


}
