package com.csps.gabriel.controllers;

import com.csps.gabriel.entities.*;
import com.csps.gabriel.dtos.PolicyDto;
import com.csps.gabriel.repositories.*;
import com.csps.gabriel.services.FilterService;
import com.csps.gabriel.services.PolicyService;
import com.csps.gabriel.services.StatusService;
import com.csps.gabriel.usefull.*;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

//import java.awt.print.Pageable;
import java.text.SimpleDateFormat;
import java.util.*;

/**
  Created by Jose A Rodriguez on 5/18/2017.
 */

@RestController
public class PolicyController {

    @Autowired
    PolicyRepository policyRepository;

    @Autowired
    BranchRepository branchRepository;

    @Autowired
    GoodsRepository goodsRepository;

    @Autowired
    ClientRepository clientRepository;

    @Autowired
    ModelMapper modelMapper;

    @Autowired
    UserRepo userRepo;

    @Autowired
    PolicyService policyService;

    @Autowired
    FilterService filterService;

    @Autowired
    DependencyRepository dependencyRepository;

    @Autowired
    LocalRepository localRepository;

    @Autowired
    StatusService statusService;

    @Autowired
    InvoiceRepository invoiceRepository;

    @RequestMapping(value = "/save-policy", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody
    Response<Policy> savePolicy(@RequestBody Policy policy) {
        policy = this.policyRepository.save(policy);
        if (policy.getPolicyNumber()== null) {
            policy.setPolicyNumber("PL-"+policy.getPolicyId());
            policy = this.policyRepository.save(policy);
        }

        if (policy.getPolicyId() == null){
            policy.setModifiedDate(new Date());
        }
        return new Response<>(policy, true, "Successful");
    }

    @RequestMapping(value = "/save-goods", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody
    Response<Goods> saveGoodList(@RequestBody Goods goods) {
        return new Response<Goods>(this.goodsRepository.save(goods), true, "Successful");
    }

    @RequestMapping(value = "/save-locals", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody
    Response<?> saveLocals(@RequestBody List<Local> locals) {
        List<Local>localList = new ArrayList<>();
        locals.forEach(local -> localList.add(this.localRepository.save(local)));
        return new Response<>(localList, true, "Successful");
    }

    @RequestMapping(value = "/save-dependencies", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody
    Response<?> saveDependencyList(@RequestBody List<Dependent> dependents) {
        List<Dependent> dependentList = new ArrayList<>();
        dependents.forEach((dependent -> dependentList.add(this.dependencyRepository.save(dependent))));
        return new Response<List<Dependent>>(dependentList, true, "Successful");
    }

    @RequestMapping(value = "/read-policy/{id}", method = RequestMethod.GET)
    public @ResponseBody
    Response<PolicyDto> readPolicy(@PathVariable("id")Long id) {
        Policy policy = policyRepository.findOne(id);
        PolicyDto policyDto = modelMapper.map(policy, PolicyDto.class);

        String brandDesc ="";
        try {
            brandDesc = this.branchRepository.findOne(policy.getBranch()).getBranchDescription().toLowerCase();
        }catch (Exception ignored){}
        if (brandDesc.contains("salud")){
            List<Dependent> dependents = this.dependencyRepository.findAllByPolicyLinked(policy.getPolicyId());
            policyDto.setDependentList(dependents);
        }else if(brandDesc.contains("incendio")){
            List<Local> localList = this.localRepository.findAllByPolicyLinked(policy.getPolicyId());
            policyDto.setLocals(localList);
        }
        else {
            List<Goods> goodsList = this.goodsRepository.findByPolicyLinked(policy.getPolicyId());
            policyDto.setGoodss(goodsList);
        }
        return new Response<>(policyDto, true, "Successful");
    }

    @RequestMapping(value = "/filtering-policy", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody
    Response <List<Policy>> getByFilters(@RequestBody Filter filter) {
        List<Policy> policies = null;

        policies = this.policyRepository.findAllByPolicerContainingAndEndDateGreaterThanEqualAndEndDateLessThanEqual(
                filter.getEnterprise() == null ? "" : filter.getEnterprise(),
                filter.getStartDate() == null ? this.addDate(-50): filter.getStartDate(),
                filter.getEndDate() == null ? this.addDate(50): filter.getEndDate());
        return new Response<>(policies, true, "Successful", "SUCCESS");
    }

    @RequestMapping(value = "/get-all-policy/{page}", method = RequestMethod.GET)
    public @ResponseBody
    Response<?> getAllPolicy(@PathVariable("page") int page) {
        Pageable pageable = new PageRequest(page-1, 6, new Sort(Sort.Direction.ASC, "endDate"));
        Page<Policy> listPage = policyRepository.findAllByPrimeGreaterThanAndStatusEqualsOrderByEndDateAsc(10d, "Vigente",pageable);
        return new Response<>(listPage, true, "Successful");
    }

    @RequestMapping(value = "/get-all-policy", method = RequestMethod.GET)
    public @ResponseBody
    Response<List<Policy>> getAllPolicy() {
        return new Response<>(policyRepository.findAllByPrimeGreaterThanOrderByEndDateAsc(0.0), true, "Successful");
    }

    @RequestMapping(value = "/delete-one-record/{id}/{user}", method = RequestMethod.DELETE)
    public @ResponseBody
    Response <String> deleteOneRecord(@PathVariable("id") Long id, @PathVariable("user") String username ) {
        User user = this.userRepo.findByUsername(username);
        if (user.getRole().equals("1")){
            this.goodsRepository.delete(id);
            return new Response<>("Record Deleted", true, "Objecto borrado");
        }
        return new Response<>(true, "ACCION NO DISPONIBLE");
    }

    @RequestMapping(value = "/delete-one-record/{id}", method = RequestMethod.DELETE)
    public @ResponseBody
    Response <?> deleteOneRecordHealth(@PathVariable("id") Long id) {
        this.dependencyRepository.delete(id);
        return new Response<>("Record Deleted", true, "Eliminado");
    }

    @RequestMapping(value = "/delete-policy/{id}", method = RequestMethod.DELETE)
    public @ResponseBody
    Response <String> deletePolicy(@PathVariable("id") Long id) {
        Policy policy = this.policyRepository.findOne(id);
        this.policyRepository.delete(id);
//        List<Invoice> invoices = this.invoiceRepository.findByPolicyEquals();
        return new Response<String>(null, true, "Objecto borrado", "SUCCESS");
    }

    @RequestMapping(value = "/get-by-client/{clientId}", method = RequestMethod.GET)
    public @ResponseBody
    Response <List<Policy>> getByClient(@PathVariable("clientId") Long clientId) {
        return new Response<>(policyRepository.findByClient(this.clientRepository.findOne(clientId)), true, "Successful");
    }

    @RequestMapping(value = "get-policy-by-criteria/{searchBy}/{criteria}", method = RequestMethod.GET)
    public @ResponseBody
    Response <List<Policy>> getByClient(@PathVariable("searchBy") int searchBy, @PathVariable("criteria") String criteria) {

        try {

            List<Policy> policyList;

            switch (searchBy){
                case 0: policyList = this.policyService.searchByCarReg(criteria);
                break;
                case 1: policyList = this.policyService.searchByChassis(criteria);
                break;
                case 2: policyList = this.policyService.searchByRNC(criteria);
                break;
                case 3: policyList = this.policyService.searchByIdCard(criteria);
                break;
                default: policyList = null;
                break;
            }

            return new Response<>(policyList, true, "Successful", "Success".toUpperCase());
        }catch (Exception e){
            return new Response<>((null), false, "Not data found", "Fail");
        }
    }

    @RequestMapping(value = "/do-policy-filter", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody
    Response<?> getByFilters(@RequestBody EntityFilter filter) {
        List<Policy> policies = new ArrayList<>();
        switch (filter.getCriteria()){
            case 1: policies = filterService.getPolicyWithNameLike(filter.getSearchObject());
                break;
            case 2: policies = this.filterService.getPolicyByPolicyNumber(filter.getSearchObject());
                break;
            case 3: policies = this.filterService.getPolicyWithPhoneLike(filter.getSearchObject());
                break;
            case 4: policies = this.filterService.getPolicyWithBranchLike(filter.getSearchObject());
                break;
            default: break;
        }
        return new Response<List<?>>(policies, true, "SUCCESSFUL", "SUCCESS");
    }

    @RequestMapping(value = "get-client-state/{clientId}", method = RequestMethod.GET)
    public @ResponseBody
    Response <?> accountState(@PathVariable("clientId") Long clientId) {
        List<PolicyDocument> policyDocuments = new ArrayList<>();
        List<Policy> policyList = this.policyRepository.findByClient(this.clientRepository.findOne(clientId));
        policyList.forEach(policy -> policyDocuments.add(statusService.getPolicyDoc(policy)));
        return new Response<>(policyDocuments);
    }

    @RequestMapping(value = "get-current-client-state/{clientId}", method = RequestMethod.GET)
    public @ResponseBody
    Response <?> currentAccountState(@PathVariable("clientId") Long clientId) {
        List<PolicyDocument> policyDocuments = new ArrayList<>();
        List<Policy> policyList = this.policyRepository.findAllByClientAndEndDateGreaterThanEqualAndStartDateLessThanEqual(this.clientRepository.findOne(clientId), new Date(), new Date());
        policyList.forEach(policy -> policyDocuments.add(statusService.getPolicyDoc(policy)));
        return new Response<>(policyDocuments);
    }

    private Date addDate(int years){
        Calendar cal = Calendar.getInstance();
        Date today = new Date();
        cal.setTime(today);
        cal.add(Calendar.YEAR, years);
        return cal.getTime();
    }


}

