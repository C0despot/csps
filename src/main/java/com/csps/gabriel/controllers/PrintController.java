package com.csps.gabriel.controllers;

import com.csps.gabriel.entities.Client;
import com.csps.gabriel.entities.Invoice;
import com.csps.gabriel.entities.Policy;
import com.csps.gabriel.repositories.*;
import com.csps.gabriel.usefull.PolicyPrintable;
import com.csps.gabriel.usefull.Response;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.persistence.criteria.CriteriaBuilder;
import javax.websocket.ClientEndpoint;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@RestController
public class PrintController {

    private final
    ModelMapper mapper;

    private final
    PolicyRepository policyRepository;

    private final
    BranchRepository branchRepository;

    private final
    ClientRepository clientRepository;

    private final
    CompanyRepository companyRepository;

    private final
    InvoiceRepository invoiceRepository;

    @Autowired
    public PrintController(ModelMapper mapper, PolicyRepository policyRepository, BranchRepository branchRepository, ClientRepository clientRepository, CompanyRepository companyRepository, InvoiceRepository invoiceRepository) {
        this.mapper = mapper;
        this.policyRepository = policyRepository;
        this.branchRepository = branchRepository;
        this.clientRepository = clientRepository;
        this.companyRepository = companyRepository;
        this.invoiceRepository = invoiceRepository;
    }

    @RequestMapping(value = "/print-all-policy/{company}/{startDate}/{endDate}", method = RequestMethod.GET)
    public @ResponseBody
    Response<?> readPrintablePolicy(@PathVariable("company") String company,
                                    @PathVariable("startDate") String startDate,
                                    @PathVariable("endDate") String endDate) throws ParseException {

        DateFormat sourceFormat = new SimpleDateFormat("dd-MM-yyyy");
        Date filterStartDate = sourceFormat.parse(startDate);
        Date filterEndDate = sourceFormat.parse(endDate);

        List<Policy> policies = this.policyRepository.findAllByPolicerAndEndDateGreaterThanEqualAndEndDateLessThanEqual(company, filterStartDate, filterEndDate);
        List<PolicyPrintable> printableList = new ArrayList<>();
        policies.forEach((policy -> printableList.add(getPrintableRow(policy))));
        return new Response<>(printableList, true, "Successful");
    }

    private String getBranchInfo(Long id) {
        return this.branchRepository.findOne(id).getBranchDescription().toUpperCase();
    }

    private String getCompanyInfo(Long id) {
        return this.companyRepository.findOne(id).getName();
    }

    Double sum = 0.0;

    private String debtsGestor(Long policyId) {
        List<Invoice> invoiceList = this.invoiceRepository.findByPolicyEquals(policyId);
        invoiceList.forEach(invoice -> increaseSum(invoice.getTotalPrime()));
        return String.format("%,.2f", sum);
    }

    private void increaseSum(Double value) {
        sum += value;
    }

    private String dateString(Date date) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        return sdf.format(date);
    }

    private PolicyPrintable getPrintableRow(Policy policy) {
        this.sum = 0d;
        PolicyPrintable printable = new PolicyPrintable();
        printable.setBranch(getBranchInfo(policy.getBranch()));
        printable.setCurrency(getCurrency(policy.getCurrency()));
        printable.setClient(policy.getClient().getFullName().toUpperCase());
        printable.setPhone(policy.getClient().getPhoneNumber());
        printable.setCelPhone(policy.getClient().getPhoneNumber2());
        printable.setCompany(getCompanyInfo(Long.parseLong(policy.getPolicer())));
        printable.setDebts(debtsGestor(policy.getPolicyId()));
        printable.setEndDate(dateString(policy.getEndDate()));
        printable.setStartDate(dateString(policy.getStartDate()));
        printable.setPolicyNumber(policy.getPolicyNumber().toUpperCase());
        printable.setPrime(String.format("%,.2f", policy.getPrime()));
        return printable;
    }

    private String getCurrency(String currency) {
        if (currency == null)
            return "- - -";
        if (currency.length() > 2) {
            currency = currency.substring(0, 2);
        }
        switch (currency.toUpperCase()) {
            case "RD":
            case "1": return "PESO";
            case "US":
            case "2": return "DOLAR";
            case "EU":
            case "3": return "EURO";
        }
        return "- - -";
    }
}
