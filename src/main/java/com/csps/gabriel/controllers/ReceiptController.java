package com.csps.gabriel.controllers;

import com.csps.gabriel.entities.Invoice;
import com.csps.gabriel.entities.Receipt;
import com.csps.gabriel.repositories.InvoiceRepository;
import com.csps.gabriel.repositories.ReceiptsRepository;
import com.csps.gabriel.services.CashService;
import com.csps.gabriel.services.FilterService;
import com.csps.gabriel.usefull.EntityFilter;
import com.csps.gabriel.usefull.Filter;
import com.csps.gabriel.usefull.Response;
import com.csps.security.model.User;
import com.csps.security.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

/**
 *  Created by Jose A Rodriguez on 6/9/2017.
 */

@RestController
public class ReceiptController {

    private final
    ReceiptsRepository receiptsRepository;

    private final
    InvoiceRepository invoiceRepository;

    private final
    FilterService filterService;

    private final
    UserRepository userRepository;

    private final
    CashService cashService;


    private String deletionPassword;

    @Autowired
    public ReceiptController(@Value("${item.deletion.password}") String deletionPassword, ReceiptsRepository receiptsRepository, InvoiceRepository invoiceRepository, FilterService filterService, UserRepository userRepository, CashService cashService) {
        this.deletionPassword = deletionPassword;
        this.receiptsRepository = receiptsRepository;
        this.invoiceRepository = invoiceRepository;
        this.filterService = filterService;
        this.userRepository = userRepository;
        this.cashService = cashService;
    }

    @RequestMapping(value = "/save-receipt", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody
    Response<Receipt> saveReceipt(@RequestBody Receipt receipt, Principal principal) {
        if (receipt.getAmount()== 0.0){
            return new Response<> (null, false, "ERROR");
        }
        User user = this.userRepository.findByUsername(principal.getName());
        Invoice invoice = this.invoiceRepository.findOne(receipt.getInvoiceNumber());
        if (receipt.getId()== null) {
            invoice.setTotalPrime(invoice.getTotalPrime() - receipt.getAmount());
        }else {
            invoice.setTotalPrime(invoice.getTotalPrime() -(receipt.getAmount() - this.receiptsRepository.findOne(receipt.getId()).getAmount()));
        }
        this.invoiceRepository.save(invoice);
        receipt.setUser(receipt.getUser() == null ? user : receipt.getUser() );
        return new Response<>(this.receiptsRepository.save(receipt), true, "Successful");
    }

    @RequestMapping(value = "/get-all-receipt", method = RequestMethod.GET)
    public @ResponseBody
    Response<List<Receipt>> getAllDocuments() {
        List<Receipt> receipts = this.receiptsRepository.findAll();
        return new Response<>(receipts, true, "Successful", "SUCCESS");
    }

    @RequestMapping(value = "/get-all-receipt/{page}", method = RequestMethod.GET)
    public @ResponseBody
    Response<Page<Receipt>> getAllDocuments(@PathVariable("page") int page) {
        Pageable pageable = new PageRequest(page, 8, new Sort(Sort.Direction.DESC, "endDate"));
        Page<Receipt> receipts = this.receiptsRepository.findAll(pageable);
        return new Response<>(receipts, true, "Successful", "SUCCESS");
    }

    @RequestMapping(value = "/do-receipt-filter", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody
    Response<?> getByFilters(@RequestBody EntityFilter filter) {
        List<Receipt> receipts = new ArrayList<>();
        switch (filter.getCriteria()){
            case 1: receipts = this.receiptsRepository.findByReceiptNoContaining(filter.getSearchObject());
            break;
            case 2: receipts = filterService.getReceiptWithNameLike(filter.getSearchObject());
            break;
            case 3: receipts = this.filterService.getReceiptWithPolicyLike(filter.getSearchObject());
            break;
            default: break;
        }
        return new Response<List<?>>(receipts, true, "SUCCESSFUL", "SUCCESS");
    }

    @RequestMapping(value = "/get-user-cashing", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody
    Response<?> getByUserDigit(@RequestBody Filter filter) {
        return new Response<>(this.cashService.getAllDocs(filter));
    }

    @DeleteMapping(value = "delete/receipt/{receipt}/password/{password}")
    private ResponseEntity deleteReceipt(@PathVariable(value = "receipt")Long receiptId,
                                         @PathVariable(value = "password")String password){
        if (password.equals(deletionPassword)){
            this.receiptsRepository.delete(receiptId);
            return ResponseEntity.ok("DELETED");
        }
        return ResponseEntity.badRequest().body("Password does not match");
    }
}
