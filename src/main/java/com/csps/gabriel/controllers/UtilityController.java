package com.csps.gabriel.controllers;

import com.csps.gabriel.repositories.*;
import com.csps.gabriel.usefull.Integrity;
import com.csps.gabriel.usefull.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *  Created by Jose A Rodriguez on 7/22/2017.
 */

@RestController
public class UtilityController {

    @Autowired
    PolicyRepository policyRepository;

    @Autowired
    InvoiceRepository invoiceRepository;

    @Autowired
    CreditsRepository creditsRepository;

    @Autowired
    ClaimRepository claimRepository;

    @Autowired
    ReceiptsRepository receiptsRepository;


    @RequestMapping(value = "next-integrity-number", method = RequestMethod.GET)
    public @ResponseBody
    Response<Integrity> getIntegrity() {
        Integrity integrity = new Integrity();
        integrity.setNextCreditNumber("CDT-"+(this.creditsRepository.findHigherId() == null ? "110000000" : this.creditsRepository.findHigherId() + 1));
        integrity.setNextPolicyNumber("PLZ-"+(this.policyRepository.findHigherId() == null ? "110100000" : this.policyRepository.findHigherId() +1));
        integrity.setNextInvoiceNumber("FTR-"+(this.invoiceRepository.findHigherId() == null ? "111001000" : this.invoiceRepository.findHigherId()+1));
        integrity.setNextClaimNumber("RCM-"+(this.claimRepository.findHigherId() == null ? "100100000" : this.claimRepository.findHigherId() +1));
        integrity.setNextReceiptNumber("RCB-"+(this.receiptsRepository.findHigherId() == null ? "111011000" : this.receiptsRepository.findHigherId()+1));
        return new Response<>(integrity, true, "Successful", "SUCCESS");
    }
}
