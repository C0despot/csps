package com.csps.gabriel.dtos;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 *  Created by Jose A Rodriguez on 7/9/2017.
 */
@JsonIgnoreProperties
public class BankAccountDto {

    private String bankAccountNumber;
    private String bank;
    private String label;
    private String notes;

    public String getBankAccountNumber() {
        return bankAccountNumber;
    }

    public void setBankAccountNumber(String bankAccountNumber) {
        this.bankAccountNumber = bankAccountNumber;
    }

    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }
}
