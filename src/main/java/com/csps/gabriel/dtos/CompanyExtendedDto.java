package com.csps.gabriel.dtos;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

/**
 *  Created by Jose A Rodriguez on 7/9/2017.
 */
@JsonIgnoreProperties
public class CompanyExtendedDto {

    private Long id;
    private String name;
    private String code;
    private String contact;
    private String phoneNumber;
    private List<PhoneDto> phoneList;
    private List<BankAccountDto> bankAccountList;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public List<PhoneDto> getPhoneList() {
        return phoneList;
    }

    public void setPhoneList(List<PhoneDto> phoneList) {
        this.phoneList = phoneList;
    }

    public List<BankAccountDto> getBankAccountList() {
        return bankAccountList;
    }

    public void setBankAccountList(List<BankAccountDto> bankAccountList) {
        this.bankAccountList = bankAccountList;
    }
}
