package com.csps.gabriel.dtos;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 *  Created by Jose A Rodriguez on 5/21/2017.
 */
@Entity
public class IndexMapperDto implements Serializable {

    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private Integer id;

    private String name;

    private Long index;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getIndex() {
        return index;
    }

    public void setIndex(Long index) {
        this.index = index;
    }
}
