package com.csps.gabriel.dtos;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 *  Created by Jose A Rodriguez on 7/9/2017.
 */
@JsonIgnoreProperties
public class PhoneDto {

    private String phoneNumber;
    private String branch;
    private String extraInfo;
    private String notes;

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public String getExtraInfo() {
        return extraInfo;
    }

    public void setExtraInfo(String extraInfo) {
        this.extraInfo = extraInfo;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }
}
