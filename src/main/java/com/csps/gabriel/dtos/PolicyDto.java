package com.csps.gabriel.dtos;

import com.csps.gabriel.entities.Client;
import com.csps.gabriel.entities.Dependent;
import com.csps.gabriel.entities.Goods;
import com.csps.gabriel.entities.Local;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *  Created by Jose A Rodriguez on 5/26/2017.
 */
public class PolicyDto {

    private Long policyId;

    private Client client;

    private String policyNumber;

    private Long branch;

    private Long policer;

    private Double prime;

    private String currency;

    private Date startDate;

    private Date endDate;

    private String status;

    private String itbis;

    private String notes;

    private String bigNote;

    private String descriptions;

    private String buildingType;

    private String businessType;

    private String local;

    private List<Goods> goodss = new ArrayList<>();

    private List<Dependent> dependentList = new ArrayList<>();

    private List<Local> locals = new ArrayList<>();

    public List<Dependent> getDependentList() {
        return dependentList;
    }

    public void setDependentList(List<Dependent> dependentList) {
        this.dependentList = dependentList;
    }

    public Long getPolicyId() {
        return policyId;
    }

    public void setPolicyId(Long policyId) {
        this.policyId = policyId;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public String getPolicyNumber() {
        return policyNumber;
    }

    public void setPolicyNumber(String policyNumber) {
        this.policyNumber = policyNumber;
    }

    public Long getBranch() {
        return branch;
    }

    public void setBranch(Long branch) {
        this.branch = branch;
    }

    public Long getPolicer() {
        return policer;
    }

    public void setPolicer(Long policer) {
        this.policer = policer;
    }

    public Double getPrime() {
        return prime;
    }

    public void setPrime(Double prime) {
        this.prime = prime;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getItbis() {
        return itbis;
    }

    public void setItbis(String itbis) {
        this.itbis = itbis;
    }

    public List<Goods> getGoodss() {
        return goodss;
    }

    public void setGoodss(List<Goods> goodss) {
        this.goodss = goodss;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getDescriptions() {
        return descriptions;
    }

    public void setDescriptions(String descriptions) {
        this.descriptions = descriptions;
    }

    public String getBuildingType() {
        return buildingType;
    }

    public void setBuildingType(String buildingType) {
        this.buildingType = buildingType;
    }

    public String getBusinessType() {
        return businessType;
    }

    public void setBusinessType(String businessType) {
        this.businessType = businessType;
    }

    public String getLocal() {
        return local;
    }

    public void setLocal(String local) {
        this.local = local;
    }

    public List<Local> getLocals() {
        return locals;
    }

    public void setLocals(List<Local> locals) {
        this.locals = locals;
    }

    public String getBigNote() {
        return bigNote;
    }

    public void setBigNote(String bigNote) {
        this.bigNote = bigNote;
    }
}
