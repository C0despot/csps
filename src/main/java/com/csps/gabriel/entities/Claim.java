package com.csps.gabriel.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 *  Created by Jose A Rodriguez on 6/24/2017.
 */
@Entity
@Table(name = "claims")
public class Claim implements Serializable{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "CLAIM_ID")
    private Long claimId;

    @Column(name = "DOCUMENT_NUMBER")
    private String documentNo;

    @Column(name = "CREATION_DATE")
    private Date createDate;

    @Column(name = "ADJUSTMENT")
    private String adjust;

    @Column(name = "MECHANICAL")
    private String mechanic;

    @ManyToOne
    @JoinColumn(name = "CLIENT")
    private Client client;

    @Column(name = "POLICY")
    private Long policy;

    @Column(name = "POLICY_COMPANY")
    private Long policer;

    @Column(name = "SOME_NOTES")
    private String notes;

    public Long getClaimId() {
        return claimId;
    }

    public void setClaimId(Long claimId) {
        this.claimId = claimId;
    }

    public String getDocumentNo() {
        return documentNo;
    }

    public void setDocumentNo(String documentNo) {
        this.documentNo = documentNo;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getAdjust() {
        return adjust;
    }

    public void setAdjust(String adjust) {
        this.adjust = adjust;
    }

    public String getMechanic() {
        return mechanic;
    }

    public void setMechanic(String mechanic) {
        this.mechanic = mechanic;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public Long getPolicy() {
        return policy;
    }

    public void setPolicy(Long policy) {
        this.policy = policy;
    }

    public Long getPolicer() {
        return policer;
    }

    public void setPolicer(Long policer) {
        this.policer = policer;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }
}
