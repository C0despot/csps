package com.csps.gabriel.entities;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Jose A Rodriguez on 5/13/2017.
 */
@Entity
@Table(name = "table_clients")
public class Client implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "client_id")
    private Long clientId;

    @Column(name = "client_name")
    private String firstName;

    @Column(name = "client_last_name")
    private String lastName;

    @Column(name = "client_id_card")
    private String idCard;

    @Column(name = "client_buss_name")
    private String enterprise;

    @Column(name = "client_buss_contact")
    private String contactBus;

    @Column(name = "client_buss_rnc")
    private String rnc;

    @Column(name = "address")
    private String address;

    @Column(name = "sector")
    private String sector;

    @Column(name = "city")
    private String city;

    @Column(name = "phone_1")
    private String phoneNumber1;

    @Column(name = "phone_2")
    private String phoneNumber2;

    @Column(name = "phone_3")
    private String phoneNumber3;

    @Column(name = "client_email")
    private String email;

    @Column(name = "client_contact")
    private String clientContact;

    @Column(name = "client_type")
    private String clientType;

    @Column(name = "notes")
    private String notes;

    @Column(name = "account_state")
    private String status;

    @Column(name = "phone_number_3")
    private String phoneNumber;

    @Column(name = "full_name")
    private String fullName;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getClientId() {
        return clientId;
    }

    public void setClientId(Long clientId) {
        this.clientId = clientId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getIdCard() {
        return idCard;
    }

    public void setIdCard(String idCard) {
        this.idCard = idCard;
    }

    public String getEnterprise() {
        return enterprise;
    }

    public void setEnterprise(String enterprise) {
        this.enterprise = enterprise;
    }

    public String getContactBus() {
        return contactBus;
    }

    public void setContactBus(String contactBus) {
        this.contactBus = contactBus;
    }

    public String getRnc() {
        return rnc;
    }

    public void setRnc(String rnc) {
        this.rnc = rnc;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getSector() {
        return sector;
    }

    public void setSector(String sector) {
        this.sector = sector;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPhoneNumber1() {
        return phoneNumber1;
    }

    public void setPhoneNumber1(String phoneNumber1) {
        this.phoneNumber1 = phoneNumber1;
    }

    public String getPhoneNumber2() {
        return phoneNumber2;
    }

    public void setPhoneNumber2(String phoneNumber2) {
        this.phoneNumber2 = phoneNumber2;
    }

    public String getPhoneNumber3() {
        return phoneNumber3;
    }

    public void setPhoneNumber3(String phoneNumber3) {
        this.phoneNumber3 = phoneNumber3;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getClientContact() {
        return clientContact;
    }

    public void setClientContact(String clientContact) {
        this.clientContact = clientContact;
    }

    public String getClientType() {
        return clientType;
    }

    public void setClientType(String clientType) {
        this.clientType = clientType;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getFullName() {
        return fullName.toUpperCase();
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }
}
