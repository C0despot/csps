package com.csps.gabriel.entities;

import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 *  Created by Jose A Rodriguez on 6/24/2017.
 */
@Entity
@Table(name = "credit")
public class Credit implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "CREDIT_ID")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long creditId;

    @Column(name = "CREDIT_TYPE")
    private String type;

    @ManyToOne
    @JoinColumn(name = "CLIENT")
    @Type(type = "serializable")
    private Client client;

    @Column(name = "POLICY")
    private Long policy;

    @Column(name = "START_DATE")
    private Date startDate;

    @Column(name = "END_DATE")
    private Date endDate;

    @Column(name = "INVOICE_NUMBER")
    private Long invoiceNumber;

    @Column(name = "BEGINNING_DATE")
    private Date beginningDate;

    @Column(name = "DEAD_DUE_DATE")
    private Date dueDate;

    @Column(name = "SOME_NOTES")
    private String notes;

    @Column(name = "DOCUMENT_NUMBER")
    private String documentNo;

    @Column(name = "AMOUNT")
    private Double amount;

    @Column(name = "CONCEPT")
    private String concept;

    @Column(name = "PRIME")
    private Double prime;

    public Long getCreditId() {
        return creditId;
    }

    public void setCreditId(Long creditId) {
        this.creditId = creditId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public Long getPolicy() {
        return policy;
    }

    public void setPolicy(Long policy) {
        this.policy = policy;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Long getInvoiceNumber() {
        return invoiceNumber;
    }

    public void setInvoiceNumber(Long invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }

    public Date getBeginningDate() {
        return beginningDate;
    }

    public void setBeginningDate(Date beginningDate) {
        this.beginningDate = beginningDate;
    }

    public Date getDueDate() {
        return dueDate;
    }

    public void setDueDate(Date dueDate) {
        this.dueDate = dueDate;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getDocumentNo() {
        return documentNo;
    }

    public void setDocumentNo(String documentNo) {
        this.documentNo = documentNo;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getConcept() {
        return concept;
    }

    public void setConcept(String concept) {
        this.concept = concept;
    }

    public Double getPrime() {
        return prime;
    }

    public void setPrime(Double prime) {
        this.prime = prime;
    }
}
