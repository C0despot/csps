package com.csps.gabriel.entities;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "DEPENDENT")
public class Dependent {

    @Id
    @Column(name = "DEPENDENT_ID")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long dependentId;

    @Column(name = "POLICY_LINKED")
    private Long policyLinked;

    @Column(name = "DEPENDENT_NAME")
    private String name;

    @Column(name = "DEPENDENT_AGE")
    private String age;

    @Column(name = "FILIAL_NUMBER")
    private String filialNum;

    @Column(name = "CATEGORY")
    private String category;

    @Column(name = "PLAN_TYPE")
    private String planType;

    @Column(name = "TYPE")
    private String type;

    @Column(name = "VALUE")
    private String value;

    @Column(name = "ID_CARD")
    private String identification;

    @Column(name = "PARENT")
    private String parent;

    @Column(name = "SEX")
    private String sex;

    @Column(name = "BIRTH_DATE")
    private Date birthDate;

    @Column(name = "START_DATE")
    private Date startDate;

    @Column(name = "PLAN")
    private String plan;

    public Long getDependentId() {
        return dependentId;
    }

    public void setDependentId(Long dependentId) {
        this.dependentId = dependentId;
    }

    public Long getPolicyLinked() {
        return policyLinked;
    }

    public void setPolicyLinked(Long policyLinked) {
        this.policyLinked = policyLinked;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getFilialNum() {
        return filialNum;
    }

    public void setFilialNum(String filialNum) {
        this.filialNum = filialNum;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getPlanType() {
        return planType;
    }

    public void setPlanType(String planType) {
        this.planType = planType;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getIdentification() {
        return identification;
    }

    public void setIdentification(String identification) {
        this.identification = identification;
    }

    public String getParent() {
        return parent;
    }

    public void setParent(String parent) {
        this.parent = parent;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public String getPlan() {
        return plan;
    }

    public void setPlan(String plan) {
        this.plan = plan;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
