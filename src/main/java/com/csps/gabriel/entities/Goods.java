package com.csps.gabriel.entities;

import com.csps.gabriel.entities.Policy;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;

/**
  Created by Jose A Rodriguez on 5/20/2017.
 */
@Entity
@Table(name = "goods")
public class Goods implements Serializable{

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "goods_id")
    private Long goodsId;

    @Column(name = "type")
    private String type;

    @Column(name = "year")
    private Long year;

    private String brand;
    private String model;

    @Column(name = "chassis_serial")
    private String chassisSerial;

    @Column (name = "covert_amount")
    private Double covertAmount;
    private String color;
    private Long cylinder;

    @Column(name = "passenger_amount")
    private Long passengerAmount;
    private String weight;
    private String services;

    @Column(name = "car_register")
    private String carRegister;

    @Column(name = "care_type")
    private String careType;
    private Double prime;
    private String covert;

    @Column(name = "vial_cares")
    private String vialCares;

    @Column(name = "mobil_center")
    private String mobilCenter;
    private String deducible;

    @Column(name = "air_emergency")
    private String airEmergency;

    @Column(name = "rent_car")
    private String rentCar;

    @Column(name = "policy_linked")
    private Long policyLinked;

    @Column(name = "SELF_DAMAGE")
    private String selfDamage;

    @Column(name = "NOTES")
    private String notes;


    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getServices() {
        return services;
    }

    public void setServices(String services) {
        this.services = services;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Long getYear() {
        return year;
    }

    public void setYear(Long year) {
        this.year = year;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getChassisSerial() {
        return chassisSerial;
    }

    public void setChassisSerial(String chassisSerial) {
        this.chassisSerial = chassisSerial;
    }

    public Double getCovertAmount() {
        return covertAmount;
    }

    public void setCovertAmount(Double covertAmount) {
        this.covertAmount = covertAmount;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Long getCylinder() {
        return cylinder;
    }

    public void setCylinder(Long cylinder) {
        this.cylinder = cylinder;
    }

    public Long getPassengerAmount() {
        return passengerAmount;
    }

    public void setPassengerAmount(Long passengerAmount) {
        this.passengerAmount = passengerAmount;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getCarRegister() {
        return carRegister;
    }

    public void setCarRegister(String carRegister) {
        this.carRegister = carRegister;
    }

    public String getCareType() {
        return careType;
    }

    public void setCareType(String careType) {
        this.careType = careType;
    }

    public Double getPrime() {
        return prime;
    }

    public void setPrime(Double prime) {
        this.prime = prime;
    }

    public String getCovert() {
        return covert;
    }

    public void setCovert(String covert) {
        this.covert = covert;
    }

    public String getVialCares() {
        return vialCares;
    }

    public void setVialCares(String vialCares) {
        this.vialCares = vialCares;
    }

    public String getMobilCenter() {
        return mobilCenter;
    }

    public void setMobilCenter(String mobilCenter) {
        this.mobilCenter = mobilCenter;
    }

    public String getDeducible() {
        return deducible;
    }

    public void setDeducible(String deducible) {
        this.deducible = deducible;
    }

    public String getAirEmergency() {
        return airEmergency;
    }

    public void setAirEmergency(String airEmergency) {
        this.airEmergency = airEmergency;
    }

    public String getRentCar() {
        return rentCar;
    }

    public void setRentCar(String rentCar) {
        this.rentCar = rentCar;
    }

    public Long getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Long goodsId) {
        this.goodsId = goodsId;
    }

    public Long getPolicyLinked() {
        return policyLinked;
    }

    public void setPolicyLinked(Long policyLinked) {
        this.policyLinked = policyLinked;
    }

    public String getSelfDamage() {
        return selfDamage;
    }

    public void setSelfDamage(String selfDamage) {
        this.selfDamage = selfDamage;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }
}
