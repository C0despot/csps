package com.csps.gabriel.entities;

import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 *  Created by Jose A Rodriguez on 6/3/2017.
 */
@Entity
@Table(name = "invoice")
public class Invoice implements Comparable<Invoice>{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "INVOICE_ID")
    private Long invoiceId;

    @Column(name = "START_DATE")
    private Date startDate;

    @Column(name = "END_DATE")
    private Date endDate;

    @ManyToOne
    @JoinColumn(name = "CLIENT")
    @Type(type = "serializable")
    private Client client;

    @Column(name = "POLICY")
    private Long policy;

    @Column(name = "INVOICE_NUMBER")
    private String invoiceNumber;

    @Column(name = "INVOICE_TYPE")
    private String invRegType;

    @Column(name = "TOTAL_PRIME")
    private Double totalPrime;

    @Column(name = "BEGINNING_DATE")
    private Date beginningDate;

    @Column(name = "DEAD_LINE_DUE_DATE")
    private Date dueDate;

    @Column(name = "SOME_NOTES")
    private String notes;

    public Long getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(Long invoiceId) {
        this.invoiceId = invoiceId;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public Long getPolicy() {
        return policy;
    }

    public void setPolicy(Long policy) {
        this.policy = policy;
    }

    public String getInvoiceNumber() {
        return invoiceNumber;
    }

    public void setInvoiceNumber(String invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }

    public String getInvRegType() {
        return invRegType;
    }

    public void setInvRegType(String invRegType) {
        this.invRegType = invRegType;
    }

    public Double getTotalPrime() {
        return totalPrime;
    }

    public void setTotalPrime(Double totalPrime) {
        this.totalPrime = totalPrime;
    }

    public Date getBeginningDate() {
        return beginningDate;
    }

    public void setBeginningDate(Date beginningDate) {
        this.beginningDate = beginningDate;
    }

    public Date getDueDate() {
        return dueDate;
    }

    public void setDueDate(Date dueDate) {
        this.dueDate = dueDate;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    @Override
    public int compareTo(Invoice invoice) {
        return getTotalPrime().compareTo(invoice.getTotalPrime());
    }
}
