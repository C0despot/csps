package com.csps.gabriel.entities;

import javax.persistence.*;

@Entity
@Table(name = "LOCAL")
public class Local {

    @Id
    @Column(name = "LOCAL_ID")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long localId;

    @Column(name = "LOCAl_NAME")
    private String name;

    @Column(name = "LOCAL_TYPE")
    private String localType;

    @Column(name = "BUILDING_TYPE")
    private String buildingType;

    @Column(name = "LOCAL_ADDRESS")
    private String address;

    @Column(name = "GOOD_LIST")
    private String goodList;

    @Column(name = "POLICY_LINKED")
    private Long policyLinked;


    public Long getFireId() {
        return localId;
    }

    public void setFireId(Long fireId) {
        this.localId = fireId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocalType() {
        return localType;
    }

    public void setLocalType(String localType) {
        this.localType = localType;
    }

    public String getBuildingType() {
        return buildingType;
    }

    public void setBuildingType(String buildingType) {
        this.buildingType = buildingType;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getGoodList() {
        return goodList;
    }

    public void setGoodList(String goodList) {
        this.goodList = goodList;
    }

    public Long getPolicyLinked() {
        return policyLinked;
    }

    public void setPolicyLinked(Long policyLinked) {
        this.policyLinked = policyLinked;
    }
}