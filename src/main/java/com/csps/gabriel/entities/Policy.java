package com.csps.gabriel.entities;

import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;

/**
  Created by Jose A Rodriguez on 5/19/2017.
 */
@Entity
@Table(name = "csps_policy")
public class Policy {


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "policy_id")
    private Long policyId;

    @ManyToOne
    @JoinColumn(name = "client")
    @Type(type = "serializable")
    private Client client;

    @Column(name = "policy_number")
    private String policyNumber;

    @Column(name = "branch_name")
    private Long branch;

    @Column(name = "policer")
    private String policer;

    @Column(name = "prime")
    private Double prime;

    @Column(name = "currency")
    private String currency;

    @Column(name = "start_date")
    private Date startDate;

    @Column(name = "end_date")
    private Date endDate;

    @Column(name = "status")
    private String status;

    @Column(name = "itbis")
    private String itbis;

    @Column(name = "goods_string")
    private String goodsString;

    @Column(name = "NOTES")
    private String notes;

    @Column(name = "BUILDING_TYPE")
    private String buildingType;

    @Column(name = "BUSINESS_TYPE")
    private String businessType;

    @Column(name = "LOCAL_PLACE")
    private String local;

    @Column(name = "BIG_NOTE")
    private String bigNote;

    @Column(name = "MODIFIED_DATE")
    private Date modifiedDate;
    public Long getPolicyId() {
        return policyId;
    }

    public void setPolicyId(Long policyId) {
        this.policyId = policyId;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public String getPolicyNumber() {
        return policyNumber.toUpperCase();
    }

    public void setPolicyNumber(String policyNumber) {
        this.policyNumber = policyNumber;
    }

    public Long getBranch() {
        return branch;
    }

    public void setBranch(Long branch) {
        this.branch = branch;
    }

    public String getPolicer() {
        return policer;
    }

    public void setPolicer(String policer) {
        this.policer = policer;
    }

    public Double getPrime() {
        return prime;
    }

    public void setPrime(Double prime) {
        this.prime = prime;
    }

    public String getCoin() {
        return currency;
    }

    public void setCoin(String currency) {
        this.currency = currency;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getItbis() {
        return itbis;
    }

    public void setItbis(String itbis) {
        this.itbis = itbis;
    }


    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getGoodsString() {
        return goodsString;
    }

    public void setGoodsString(String goodsString) {
        this.goodsString = goodsString;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getBuildingType() {
        return buildingType;
    }

    public void setBuildingType(String buildingType) {
        this.buildingType = buildingType;
    }

    public String getBusinessType() {
        return businessType;
    }

    public void setBusinessType(String businessType) {
        this.businessType = businessType;
    }

    public String getLocal() {
        return local;
    }

    public void setLocal(String local) {
        this.local = local;
    }

    public String getBigNote() {
        return bigNote;
    }

    public void setBigNote(String bigNote) {
        this.bigNote = bigNote;
    }



    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

}
