package com.csps.gabriel.entities;

import com.csps.security.model.User;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 *  Created by Jose A Rodriguez on 6/9/2017.
 */
@Entity
@Table(name = "receipts")
public class Receipt implements Serializable{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "receipt_id")
    private Long id;

    @Column(name = "receipt_number")
    private String receiptNo;

    @ManyToOne
    @JoinColumn(name = "user_editor")
    @Type(type = "serializable")
    private User user;

    @Column(name = "start_date")
    private Date startDate;

    @Column(name = "end_date")
    private Date endDate;

    @ManyToOne
    @JoinColumn(name = "client")
    @Type(type = "serializable")
    private Client client;

    @Column(name = "policy")
    private Long policy;

    @Column(name = "invoice_number")
    private Long invoiceNumber;

    @Column(name = "amount")
    private Double amount;

    @Column(name = "provisional_receipt")
    private String provisional;

    @Column(name = "provisional_company_receipt")
    private String provComp;

    @Column(name = "payment_way")
    private String payment;

    @Column(name = "payment_type")
    private String paymentType;

    @Column(name = "currency")
    private String currency;

    @Column(name = "credit_card")
    private String credit;

    private String concept;

    private String notes;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getReceiptNo() {
        return receiptNo;
    }

    public void setReceiptNo(String receiptNo) {
        this.receiptNo = receiptNo;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public Long getPolicy() {
        return policy;
    }

    public void setPolicy(Long policy) {
        this.policy = policy;
    }

    public Long getInvoiceNumber() {
        return invoiceNumber;
    }

    public void setInvoiceNumber(Long invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getProvisional() {
        return provisional;
    }

    public void setProvisional(String provisional) {
        this.provisional = provisional;
    }

    public String getProvComp() {
        return provComp;
    }

    public void setProvComp(String provComp) {
        this.provComp = provComp;
    }

    public String getPayment() {
        return payment;
    }

    public void setPayment(String payment) {
        this.payment = payment;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getCredit() {
        return credit;
    }

    public void setCredit(String credit) {
        this.credit = credit;
    }

    public String getConcept() {
        return concept;
    }

    public void setConcept(String concept) {
        this.concept = concept;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
