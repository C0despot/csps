package com.csps.gabriel.repositories;

import com.csps.gabriel.entities.Branch;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BranchRepository extends JpaRepository<Branch, Long>{
    List<Branch> findAllByBranchDescriptionContaining(String criteria);
}
