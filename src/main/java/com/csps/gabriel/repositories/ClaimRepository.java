package com.csps.gabriel.repositories;

import com.csps.gabriel.entities.Claim;
import com.csps.gabriel.entities.Client;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 *  Created by Jose A Rodriguez on 6/24/2017.
 */
public interface ClaimRepository extends JpaRepository<Claim, Long> {

    @Query(value = "SELECT MAX(CLAIM_ID) FROM claims", nativeQuery = true)
    Long findHigherId();

    List<Claim> findByPolicyEquals(Long policy);
    List<Claim> findByClient(Client client);
    Claim findByDocumentNo(String documentNo);
    List<Claim> findByDocumentNoContaining(String documentNo);
}
