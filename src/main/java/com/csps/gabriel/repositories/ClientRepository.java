package com.csps.gabriel.repositories;

import com.csps.gabriel.entities.Client;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Jose A Rodriguez on 5/13/2017.
 */
@Repository
public interface ClientRepository extends JpaRepository <Client, Long>{

    List<Client> findAllByClientType(String clientType);
    List<Client> findByFullNameContaining(String fullName);
    Client findByRnc(String rnc);
    Client findByRncEqualsOrIdCardEquals(String rnc, String idCard);
    Client findByIdCard(String idCard);
    List<Client> findAllByPhoneNumberContaining(String phoneNumber);
    List<Client> findAllByIdCardContaining(String idCard);
}
