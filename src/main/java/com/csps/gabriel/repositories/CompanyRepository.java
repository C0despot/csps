package com.csps.gabriel.repositories;

import com.csps.gabriel.entities.Company;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by Jose A Rodriguez on 7/9/2017.
 */
@Repository
public interface CompanyRepository extends JpaRepository<Company, Long> {
    Company findByNameEquals(String name);
}
