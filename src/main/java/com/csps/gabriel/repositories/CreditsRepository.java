package com.csps.gabriel.repositories;

import com.csps.gabriel.entities.Client;
import com.csps.gabriel.entities.Credit;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * Created by Jose A Rodriguez on 6/24/2017.
 */
public interface CreditsRepository extends JpaRepository<Credit, Long> {

    @Query(value = "SELECT MAX(CREDIT_ID) FROM credit", nativeQuery = true)
    Long findHigherId();

    List<Credit> findByPolicyEquals(Long policy);
    List<Credit> findByClient(Client client);
    Credit findByDocumentNo(String creditNo);
    List<Credit> findByDocumentNoContaining(String creditNo);
    List<Credit> findAllByInvoiceNumberEquals(Long invoiceNumber);
}
