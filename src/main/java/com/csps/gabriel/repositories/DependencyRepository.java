package com.csps.gabriel.repositories;

import com.csps.gabriel.entities.Dependent;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface DependencyRepository extends JpaRepository<Dependent, Long> {
    List<Dependent> findAllByPolicyLinked(Long policyLinked);
}
