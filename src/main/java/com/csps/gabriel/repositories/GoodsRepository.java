package com.csps.gabriel.repositories;

import com.csps.gabriel.entities.Goods;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by Jose A Rodriguez on 5/21/2017.
 */
public interface GoodsRepository extends JpaRepository<Goods, Long> {

    List<Goods> findByPolicyLinked(Long policyLinked);
    Goods findByCarRegisterLike(String carRegister);
    Goods findByChassisSerialLike(String chassisSerial);
}
