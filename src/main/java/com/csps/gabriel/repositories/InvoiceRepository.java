package com.csps.gabriel.repositories;

import com.csps.gabriel.entities.Client;
import com.csps.gabriel.entities.Invoice;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import javax.persistence.criteria.CriteriaBuilder;
import java.util.List;

/**
 *  Created by Jose A Rodriguez on 6/3/2017.
 */
public interface InvoiceRepository extends JpaRepository<Invoice, Long> {
    Invoice findByInvoiceNumber(String invoiceNumber);
    List<Invoice> findByPolicyLike(Long policy);

    @Query(value = "SELECT MAX(INVOICE_ID) FROM invoice", nativeQuery = true)
    Long findHigherId();

    List<Invoice> findByPolicyEquals(Long policy);
    List<Invoice> findByClient(Client client);
    Page<Invoice> findAllByTotalPrimeGreaterThan(Double totalPrime, Pageable pageable);
    List<Invoice> findByInvoiceNumberContaining(String invoiceNumber);
}
