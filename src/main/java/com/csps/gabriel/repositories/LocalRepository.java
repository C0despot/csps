package com.csps.gabriel.repositories;

import com.csps.gabriel.entities.Local;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface LocalRepository extends JpaRepository<Local, Long> {
    List<Local> findAllByPolicyLinked(Long policyLinked);
}
