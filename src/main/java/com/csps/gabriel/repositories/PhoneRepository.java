package com.csps.gabriel.repositories;

import com.csps.gabriel.entities.Phone;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *  Created by Jose A Rodriguez on 7/9/2017.
 */
@Repository
public interface PhoneRepository extends JpaRepository<Phone, Long> {

}
