package com.csps.gabriel.repositories;

import com.csps.gabriel.entities.Branch;
import com.csps.gabriel.entities.Client;
import com.csps.gabriel.entities.Local;
import com.csps.gabriel.entities.Policy;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.List;

/**
 * Created by Jose A Rodriguez on 5/20/2017.
 */
public interface PolicyRepository extends JpaRepository<Policy, Long> {
    List<Policy> findByClient(Client client);
    List<Policy> findAllByOrderByEndDateAsc();
    List<Policy> findAllByPrimeGreaterThanOrderByEndDateAsc(Double prime);
    List<Policy> findAllByPolicerAndEndDateGreaterThanEqualAndEndDateLessThanEqual(String company, Date startDate, Date endDate);
    List<Policy> findAllByClientAndEndDateGreaterThanEqualAndStartDateLessThanEqual(Client client, Date today1, Date today2);
    @Query(value = "SELECT p FROM Policy p WHERE p.endDate BETWEEN :startDate AND :endDate")
    List<Policy> findBetweenTwoDates(@Param("startDate")Date startDate, @Param("endDate")Date endDate);

    @Query(value = "SELECT p FROM Policy p WHERE p.endDate < :endDate")
    List<Policy> findBeforeDate(@Param("endDate")Date endDate);

    @Query(value = "SELECT p FROM Policy p WHERE p.endDate < :startDate")
    List<Policy> findAfterDate(@Param("startDate")Date startDate);

    List<Policy> findAllByPolicer(String policer);

    @Query(value = "SELECT MAX(policy_id) FROM csps_policy", nativeQuery = true)
    Long findHigherId();

    List<Policy> findByPolicyNumberContaining(String policyNumber);
    Policy findByPolicyNumber(String policyNumber);
    List<Policy> findAllByBranch(Long branch);

    List<Policy> findAllByPolicerContainingAndEndDateGreaterThanEqualAndEndDateLessThanEqual(String policer, Date startDate, Date endDate);

    Page<Policy> findAllByPrimeGreaterThanAndStatusEqualsOrderByEndDateAsc(Double prime, String status, Pageable pageable);
}
