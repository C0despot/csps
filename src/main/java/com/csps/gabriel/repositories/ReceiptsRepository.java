package com.csps.gabriel.repositories;

import com.csps.gabriel.entities.Client;
import com.csps.gabriel.entities.Receipt;
import com.csps.security.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Date;
import java.util.List;

/**
 * Created by Jose A Rodriguez on 6/9/2017.
 */
public interface ReceiptsRepository extends JpaRepository<Receipt, Long>{
    @Query(value = "SELECT MAX(receipt_id) FROM receipts", nativeQuery = true)
    Long findHigherId();

    List<Receipt> findByClient(Client client);

    List<Receipt> findByPolicyEquals(Long policy);

    Receipt findByReceiptNo(String receiptNo);
    List<Receipt> findByReceiptNoContaining(String receiptNo);
    List<Receipt> findAllByInvoiceNumberEquals(Long invoiceNumber);
    List<Receipt> findAllByUserAndStartDateGreaterThanEqualAndStartDateLessThanEqual(User user, Date startDate, Date endDate);
}

