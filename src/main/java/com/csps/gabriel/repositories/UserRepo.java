package com.csps.gabriel.repositories;

import com.csps.gabriel.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Jose A Rodriguez on 5/8/2017.
 */
@Repository
public interface UserRepo extends JpaRepository<User, Long>{

    User findByUsername(String username);
    User findOneByPasswordLikeAndUsernameLike(String password, String username);
}
