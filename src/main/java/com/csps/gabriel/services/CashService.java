package com.csps.gabriel.services;

import com.csps.gabriel.entities.Receipt;
import com.csps.gabriel.repositories.ClientRepository;
import com.csps.gabriel.repositories.InvoiceRepository;
import com.csps.gabriel.repositories.PolicyRepository;
import com.csps.gabriel.repositories.ReceiptsRepository;
import com.csps.gabriel.usefull.CashManaged;
import com.csps.gabriel.usefull.Filter;
import com.csps.security.model.User;
import com.csps.security.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class CashService {

    @Autowired
    ReceiptsRepository receiptsRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    InvoiceRepository invoiceRepository;

    @Autowired
    ClientRepository clientRepository;

    @Autowired
    PolicyRepository policyRepository;

    public List<CashManaged> getAllDocs(Filter filter){
        User user = this.userRepository.findOne(filter.getUser());
        List<Receipt> receipts =
                this.receiptsRepository.findAllByUserAndStartDateGreaterThanEqualAndStartDateLessThanEqual(user,
                        filter.getStartDate() == null ? new Date() : filter.getStartDate(),
                        filter.getEndDate() == null ? new Date() : filter.getEndDate());
        return getCashManagedList(receipts);
    }

    private List<CashManaged> getCashManagedList(List<Receipt> receipts){
        List<CashManaged> cashManagedList = new ArrayList<>();
        receipts.forEach(receipt -> {
            CashManaged cash = new CashManaged();
            cash.setAmount(receipt.getAmount()+"");
            cash.setInvoice(this.invoiceRepository.findOne(receipt.getInvoiceNumber()).getInvoiceNumber());
            cash.setClient(receipt.getClient().getFullName());
            cash.setModified(receipt.getStartDate());
            cash.setPolicy(this.policyRepository.findOne(receipt.getPolicy()).getPolicyNumber());
            cash.setReceiptId(receipt.getId());
            cash.setReceipt(receipt.getReceiptNo());
            cash.setPaymentMode(receipt.getPayment());
            cash.setPaymentType(receipt.getPaymentType());
            cashManagedList.add(cash);
        });
        return cashManagedList;
    }

    private String getCurrencyValue (String amount){
        return String.format("%,.2f", Double.parseDouble(amount));
    }

}
