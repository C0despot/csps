package com.csps.gabriel.services;

import com.csps.gabriel.dtos.BankAccountDto;
import com.csps.gabriel.dtos.CompanyExtendedDto;
import com.csps.gabriel.dtos.PhoneDto;
import com.csps.gabriel.entities.BankAccount;
import com.csps.gabriel.entities.Company;
import com.csps.gabriel.entities.Phone;
import com.csps.gabriel.repositories.BankAccountRepository;
import com.csps.gabriel.repositories.CompanyRepository;
import com.csps.gabriel.repositories.PhoneRepository;
import com.csps.gabriel.usefull.Response;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.apache.commons.beanutils.BeanToPropertyValueTransformer;
import org.apache.commons.collections.CollectionUtils;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 *  Created by Jose A Rodriguez on 7/9/2017.
 */
@Service
public class CompanyService {

    @Autowired
    ModelMapper modelMapper;

    @Autowired
    private
    Gson gson;

    @Autowired
    PhoneRepository phoneRepository;

    @Autowired
    BankAccountRepository bankAccountRepository;

    @Autowired
    CompanyRepository companyRepository;

    @Transactional
    public Response<?> saveCompany(CompanyExtendedDto companyExtendedDto){

        List<BankAccount> bankAccountList = gson.fromJson(gson.toJson(companyExtendedDto.getBankAccountList()), new TypeToken<List<BankAccount>>() {}.getType());
        List<Phone> phoneList = gson.fromJson(gson.toJson(companyExtendedDto.getPhoneList()), new TypeToken<List<Phone>>() {}.getType());

        phoneList = this.phoneRepository.save(phoneList);
        bankAccountList = this.bankAccountRepository.save(bankAccountList);
        List<Long> ids = (List<Long>) (CollectionUtils.collect(phoneList, new BeanToPropertyValueTransformer("id")));

        String phones = ids.toString()
                .replace("[", "")
                .replace("]", "")
                .replace(", ", ",");
        ids =  (List<Long>) (CollectionUtils.collect(bankAccountList, new BeanToPropertyValueTransformer("id")));

        String accounts = ids.toString()
                .replace("[", "")
                .replace("]", "")
                .replace(", ", ",");
        Company company = this.modelMapper.map(companyExtendedDto, Company.class);

        company.setPhones(phones);
        company.setAccounts(accounts);

        company = this.companyRepository.save(company);

        return new Response<>(company, true, "Nueva Compania Agregada", "SUCCESS");
    }

    public Response<List<Company>> findAllCompanies(){
        return new Response<List<Company>>(this.companyRepository.findAll(), true);
    }

    public Response<CompanyExtendedDto> findOne(Long id){

        Company company = this.companyRepository.findOne(id);
        CompanyExtendedDto companyExtendedDto = this.modelMapper.map(company, CompanyExtendedDto.class);
        String [] phones = company.getPhones().split(",");
        String [] accounts = company.getAccounts().split(",");
        List<Phone> phoneList = new ArrayList<>();
        List<BankAccount> bankAccountList = new ArrayList<>();

        try {
            for (int i = 0; i < phones.length; i++) {
                phoneList.add(this.phoneRepository.findOne(Long.parseLong(phones[i])));
            }
        }catch (Exception e){
            System.out.println("Could not load phoneList");
        }

        try {
            for (int i = 0; i < accounts.length; i++) {
                bankAccountList.add(this.bankAccountRepository.findOne(Long.parseLong(accounts[i])));
            }
        }catch (Exception e){
            System.out.println("Could not load bankAccountList");
        }


        List<BankAccountDto> bankAccounts = gson.fromJson(gson.toJson(bankAccountList), new TypeToken<List<BankAccountDto>>() {}.getType());
        List<PhoneDto> phones1 = gson.fromJson(gson.toJson(phoneList), new TypeToken<List<PhoneDto>>() {}.getType());

        companyExtendedDto.setPhoneList(phones1);
        companyExtendedDto.setBankAccountList(bankAccounts);

        return new Response<>(companyExtendedDto, true, "SUCCESSFUL", "SUCCESS");
    }
}
