package com.csps.gabriel.services;

import com.csps.gabriel.entities.*;
import com.csps.gabriel.repositories.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
public class FilterService {

    @Autowired
    ClientRepository clientRepository;

    @Autowired
    ReceiptsRepository receiptsRepository;

    @Autowired
    PolicyRepository policyRepository;

    @Autowired
    CreditsRepository creditsRepository;

    @Autowired
    InvoiceRepository invoiceRepository;

    @Autowired
    ClaimRepository claimRepository;

    @Autowired
    BranchRepository branchRepository;

    private List<Client> getClientByNameDescription(String fullName){
        List<Client> clients = this.clientRepository.findByFullNameContaining(fullName);
        if (clients.size()==0)
            clients = this.clientRepository.findAllByIdCardContaining(fullName);
        return clients;
    }
    public List<Policy> getPolicyByPolicyNumber(String policyNumber){
        return this.policyRepository.findByPolicyNumberContaining(policyNumber);
    }

    @Transactional
    public List<Receipt> getReceiptWithNameLike(String fullName){
        List<Client> clients = getClientByNameDescription(fullName);
        List<Receipt> receipts = new ArrayList<>();
        clients.forEach((client -> {
            List<Receipt> receiptList = this.receiptsRepository.findByClient(client);
            receiptList.forEach((receipt -> {
                if (receipt != null)
                    receipts.add(receipt);
            }));
        }));
        return receipts;
    }

    @Transactional
    public List<Receipt> getReceiptWithPolicyLike(String policyNumber){
        List<Policy> policies = getPolicyByPolicyNumber(policyNumber);
        List<Receipt> receipts = new ArrayList<>();
        policies.forEach((policy -> {
            List<Receipt> receiptList  = this.receiptsRepository.findByPolicyEquals(policy.getPolicyId());
            receiptList.forEach((receipt -> {
                if (receipt != null)
                    receipts.add(receipt);
            }));
        }));
        return receipts;
    }

    @Transactional
    public List<Credit> getCreditWithNameLike(String fullName){
        List<Client> clients = getClientByNameDescription(fullName);
        List<Credit> credits = new ArrayList<>();
        clients.forEach((client -> {
            List<Credit> creditList = this.creditsRepository.findByClient(client);
            creditList.forEach((credit -> {
                if (credit != null)
                    credits.add(credit);
            }));
        }));
        return credits;
    }

    @Transactional
    public List<Credit> getCreditWithPolicyLike(String policyNumber){
        List<Policy> policies = getPolicyByPolicyNumber(policyNumber);
        List<Credit> credits = new ArrayList<>();
        policies.forEach((policy -> {
            List<Credit> creditList = this.creditsRepository.findByPolicyEquals(policy.getPolicyId());
            creditList.forEach((credit -> {
                if (credit != null)
                    credits.add(credit);
            }));
        }));
        return credits;
    }

    @Transactional
    public List<Invoice> getInvoiceWithNameLike(String fullName){
        List<Client> clients = getClientByNameDescription(fullName);
        List<Invoice> invoices = new ArrayList<>();
        clients.forEach((client -> {
            List<Invoice> invoiceList = this.invoiceRepository.findByClient(client);
            invoiceList.forEach((invoice -> {
                if (invoice != null)
                    invoices.add(invoice);
            }));
        }));
        return invoices;
    }

    @Transactional
    public List<Invoice> getInvoiceWithPolicyLike(String policyNumber){
        List<Policy> policies = getPolicyByPolicyNumber(policyNumber);
        List<Invoice> invoices = new ArrayList<>();
        policies.forEach((policy -> {
            List<Invoice> invoiceList = this.invoiceRepository.findByPolicyEquals(policy.getPolicyId());
            invoiceList.forEach((invoice -> {
                if (invoice != null)
                    invoices.add(invoice);
            }));
        }));
        return invoices;
    }

    @Transactional
    public List<Claim> getClaimWithNameLike(String fullName){
        List<Client> clients = getClientByNameDescription(fullName);
        List<Claim> claims = new ArrayList<>();
        clients.forEach((client -> {
            List<Claim> claimList = this.claimRepository.findByClient(client);
            claimList.forEach((claim -> {
                if (claim != null)
                    claims.add(claim);
            }));
        }));
        return claims;
    }

    @Transactional
    public List<Claim> getClaimWithPolicyLike(String policyNumber){
        List<Policy> policies = getPolicyByPolicyNumber(policyNumber);
        List<Claim> claims = new ArrayList<>();
        policies.forEach((policy -> {
            List<Claim> claimList = this.claimRepository.findByPolicyEquals(policy.getPolicyId());
            claimList.forEach((claim -> {
                if (claim != null)
                    claims.add(claim);
            }));
        }));
        return claims;
    }

    @Transactional
    public List<Policy> getPolicyWithNameLike(String fullName){
        List<Client> clients = getClientByNameDescription(fullName);
        List<Policy> policies = new ArrayList<>();
        clients.forEach((client -> {
            List<Policy> policyList = this.policyRepository.findByClient(client);
            policyList.forEach((policy -> policies.add(policy)));
        }));
        return policies;
    }

    @Transactional
    public List<Policy> getPolicyWithPhoneLike(String phone){
        List<Client> clients = this.clientRepository.findAllByPhoneNumberContaining(phone);
        List<Policy> policies = new ArrayList<>();
        clients.forEach((client -> {
            List<Policy> policyList = this.policyRepository.findByClient(client);
            policyList.forEach((policy -> policies.add(policy)));
        }));
        return policies;
    }

    @Transactional
    public List<Policy> getPolicyWithBranchLike(String criteria){
        List<Branch> branches = this.branchRepository.findAllByBranchDescriptionContaining(criteria);
        List<Policy> policies = new ArrayList<>();
        branches.forEach((branch -> {
            List<Policy> policyList = this.policyRepository.findAllByBranch(branch.getBranchId());
            policyList.forEach((policy -> policies.add(policy)));
        }));
        return policies;
    }
}
