package com.csps.gabriel.services;

import com.csps.gabriel.entities.Client;
import com.csps.gabriel.entities.Goods;
import com.csps.gabriel.entities.Policy;
import com.csps.gabriel.repositories.ClientRepository;
import com.csps.gabriel.repositories.GoodsRepository;
import com.csps.gabriel.repositories.PolicyRepository;
import com.csps.gabriel.repositories.UserRepo;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 *  Created by Jose A Rodriguez on 7/13/2017.
 */
@Service
public class PolicyService {

    @Autowired
    PolicyRepository policyRepository;

    @Autowired
    GoodsRepository goodsRepository;

    @Autowired
    ClientRepository clientRepository;

    @Autowired
    ModelMapper modelMapper;

    @Autowired
    UserRepo userRepo;


    public List<Policy> searchByCarReg (String criteria){
        Goods goods = this.goodsRepository.findByCarRegisterLike(criteria);
        try {
            List<Policy> policies = new ArrayList<>();;
            policies.add(this.policyRepository.findOne(goods.getPolicyLinked()));
            return policies;
        }catch (Exception e){
            return null;
        }
    }

    public List<Policy> searchByChassis (String criteria){
        Goods goods = this.goodsRepository.findByChassisSerialLike(criteria);
        try {
            List<Policy> policies = new ArrayList<>();
            policies.add(this.policyRepository.findOne(goods.getPolicyLinked()));
            return policies;
        }catch (Exception e){
            return null;
        }
    }

    public List<Policy> searchByRNC (String criteria){
        Client client = this.clientRepository.findByRnc(criteria.replace("-", ""));
        try {
            return this.policyRepository.findByClient(client);
        }catch (Exception e){
            return null;
        }
    }

    public List<Policy> searchByIdCard (String criteria){
        Client client = this.clientRepository.findByIdCard(criteria.replace("-", ""));
        try {
            return this.policyRepository.findByClient(client);
        }catch (Exception e){
            return null;
        }
    }
}
