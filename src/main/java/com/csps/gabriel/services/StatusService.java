package com.csps.gabriel.services;

import com.csps.gabriel.entities.*;
import com.csps.gabriel.repositories.CompanyRepository;
import com.csps.gabriel.repositories.CreditsRepository;
import com.csps.gabriel.repositories.InvoiceRepository;
import com.csps.gabriel.repositories.ReceiptsRepository;
import com.csps.gabriel.usefull.CreditDocument;
import com.csps.gabriel.usefull.InvoiceDocument;
import com.csps.gabriel.usefull.PolicyDocument;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

@Service
public class StatusService {

    @Autowired
    InvoiceRepository invoiceRepository;

    @Autowired
    CreditsRepository creditsRepository;

    @Autowired
    ReceiptsRepository receiptsRepository;

    @Autowired
    CompanyRepository companyRepository;

    private Double totalPayedAmount = 0.0;
    private Double totalDebtPolicyAmount = 0.0;


    public PolicyDocument getPolicyDoc(Policy policy){
        totalDebtPolicyAmount = 0.0;
        Company company = this.companyRepository.findOne(Long.parseLong(policy.getPolicer()));
        PolicyDocument document = new PolicyDocument();
        document.setCurrency(getCurrency(policy.getCurrency()));
        document.setStartDate(dateString(policy.getStartDate()));
        document.setEndDate(dateString(policy.getEndDate()));
        document.setCompany(company.getName().toUpperCase());
        document.setInvoiceDocuments(getInvoiceDocumentsList(policy.getPolicyId()));
        document.setBalance(getCurrencyValue(totalDebtPolicyAmount+""));
        document.setPolicy(policy.getPolicyNumber().toUpperCase());
        document.setClient(policy.getClient().getFullName().toUpperCase());
        return document;
    }

    private List<InvoiceDocument> getInvoiceDocumentsList(Long policyId){
        List<InvoiceDocument> invoiceDocumentList = new ArrayList<>();
        List<Invoice> invoiceList = this.invoiceRepository.findByPolicyEquals(policyId);
        invoiceList.forEach((invoice -> invoiceDocumentList.add(getInvoiceDoc(invoice))));
        Collections.sort(invoiceDocumentList);
        return invoiceDocumentList;
    }

    private InvoiceDocument getInvoiceDoc(Invoice invoice){
        totalPayedAmount = 0.0;
        InvoiceDocument document = new InvoiceDocument();
        document.setCreditDocuments(getCreditDocList(invoice));
        document.setDate(invoice.getStartDate());
        document.setModified(dateString(invoice.getEndDate()));
        document.setDocument(invoice.getInvoiceNumber().toUpperCase());
        document.setBalance(getCurrencyValue(invoice.getTotalPrime()+totalPayedAmount+""));
        document.setStartDate(dateString(invoice.getBeginningDate()));
        document.setEndDate(dateString(invoice.getDueDate()));
        totalDebtPolicyAmount += invoice.getTotalPrime();
        document.setDebts(getCurrencyValue(invoice.getTotalPrime()+""));
        document.setType("FACTURA");
        return document;
    }
    private List<CreditDocument> getCreditDocList(Invoice invoice){
        List<CreditDocument> documentList = new ArrayList<>();
        List<Credit> creditList = this.creditsRepository.findAllByInvoiceNumberEquals(invoice.getInvoiceId());
        creditList.forEach((credit -> documentList.add(getCreditDoc(credit, null, "credit"))));
        List<Receipt> receiptList = this.receiptsRepository.findAllByInvoiceNumberEquals(invoice.getInvoiceId());
        receiptList.forEach((receipt -> documentList.add(getCreditDoc(null, receipt, "receipt"))));
        Collections.sort(documentList);
        return documentList;
    }

    private CreditDocument getCreditDoc(Credit credit, Receipt receipt, String type){
        CreditDocument document = new CreditDocument();
        if (type.contains("credit")) {
            totalPayedAmount += credit.getAmount();
            document.setCredit(getCurrencyValue(credit.getAmount()+""));
            document.setType("CREDITO");
            document.setDocument(credit.getDocumentNo().toUpperCase());
            document.setDate(credit.getStartDate());
            document.setModified(dateString(credit.getEndDate()));
        }else {
            totalPayedAmount += receipt.getAmount();
            document.setCredit(getCurrencyValue(receipt.getAmount()+""));
            document.setDate(receipt.getStartDate());
            document.setModified(dateString(receipt.getEndDate()));
            document.setType("RECIBO");
            document.setDocument(receipt.getReceiptNo().toUpperCase());
        }
        return document;
    }

    private String getCurrencyValue (String amount){
        return String.format("%,.2f", Double.parseDouble(amount));
    }


    private String getCurrency(String currency) {
        if (currency == null)
            return "- - -";
        if (currency.length() > 2) {
            currency = currency.substring(0, 2);
        }
        switch (currency.toUpperCase()) {
            case "RD":
            case "1": return "PESO ";
            case "US":
            case "2": return "DOLAR";
            case "EU":
            case "3": return "EURO";
        }
        return "- - -";
    }

    private String dateString(Date date){
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        return sdf.format(date);
    }
}
