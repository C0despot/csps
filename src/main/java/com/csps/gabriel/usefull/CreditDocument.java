package com.csps.gabriel.usefull;

import java.util.Date;

public class CreditDocument implements Comparable<CreditDocument> {

    private String document;
    private String modified;
    private String type;
    private String credit;
    private Date date;

    public String getDocument() {
        return document;
    }

    public void setDocument(String document) {
        this.document = document;
    }

    public String getModified() {
        return modified;
    }

    public void setModified(String modified) {
        this.modified = modified;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCredit() {
        return credit;
    }

    public void setCredit(String credit) {
        this.credit = credit;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public int compareTo(CreditDocument creditDocument) {
        return getDate().compareTo(creditDocument.getDate());
    }
}
