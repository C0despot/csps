package com.csps.gabriel.usefull;

public class EntityFilter {

    private String searchObject;
    private int criteria;

    public String getSearchObject() {
        return searchObject;
    }

    public void setSearchObject(String searchObject) {
        this.searchObject = searchObject;
    }

    public int getCriteria() {
        return criteria;
    }

    public void setCriteria(int criteria) {
        this.criteria = criteria;
    }
}
