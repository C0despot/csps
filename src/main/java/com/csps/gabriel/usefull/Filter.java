package com.csps.gabriel.usefull;

import java.util.Date;

/**
 *  Created by Jose A Rodriguez on 6/24/2017.
 */
public class Filter {

    private Date startDate;
    private Date endDate;
    private String enterprise;
    private Long user;


    public boolean isClosedLimitFilter(){
        return (startDate != null && endDate != null);
    }

    public boolean isBeforeLimitFilter(){
        return (!isClosedLimitFilter()&& endDate != null);
    }

    public boolean isAfterLimitFilter(){
        return (!isClosedLimitFilter() && endDate != null);
    }

    public boolean isEntityFilter(){
        return (!isAfterLimitFilter() && !isClosedLimitFilter() && !isBeforeLimitFilter() && getEnterprise()!= null);
    }


    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getEnterprise() {
        return enterprise;
    }

    public void setEnterprise(String enterprise) {
        this.enterprise = enterprise;
    }

    public Long getUser() {
        return user;
    }

    public void setUser(Long user) {
        this.user = user;
    }
}
