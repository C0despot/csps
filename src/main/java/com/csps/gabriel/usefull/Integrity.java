package com.csps.gabriel.usefull;

/**
 * Created by Jose A Rodriguez on 7/22/2017.
 */
public class Integrity {

    private String nextPolicyNumber;
    private String nextInvoiceNumber;
    private String nextReceiptNumber;
    private String nextCreditNumber;
    private String nextClaimNumber;

    public String getNextPolicyNumber() {
        return nextPolicyNumber;
    }

    public void setNextPolicyNumber(String nextPolicyNumber) {
        this.nextPolicyNumber = nextPolicyNumber;
    }

    public String getNextInvoiceNumber() {
        return nextInvoiceNumber;
    }

    public void setNextInvoiceNumber(String nextInvoiceNumber) {
        this.nextInvoiceNumber = nextInvoiceNumber;
    }

    public String getNextReceiptNumber() {
        return nextReceiptNumber;
    }

    public void setNextReceiptNumber(String nextReceiptNumber) {
        this.nextReceiptNumber = nextReceiptNumber;
    }

    public String getNextCreditNumber() {
        return nextCreditNumber;
    }

    public void setNextCreditNumber(String nextCredictNumber) {
        this.nextCreditNumber = nextCredictNumber;
    }

    public String getNextClaimNumber() {
        return nextClaimNumber;
    }

    public void setNextClaimNumber(String nextClaimNumber) {
        this.nextClaimNumber = nextClaimNumber;
    }
}
