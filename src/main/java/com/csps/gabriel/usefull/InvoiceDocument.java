package com.csps.gabriel.usefull;

import java.util.Date;
import java.util.List;

public class InvoiceDocument implements Comparable<InvoiceDocument> {
    private String type;
    private String document;
    private String modified;
    private String startDate;
    private String endDate;
    private String debts;
    private String balance;
    private List<CreditDocument> creditDocuments;
    private Date date;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDocument() {
        return document;
    }

    public void setDocument(String document) {
        this.document = document;
    }

    public String getModified() {
        return modified;
    }

    public void setModified(String modified) {
        this.modified = modified;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getDebts() {
        return debts;
    }

    public void setDebts(String debts) {
        this.debts = debts;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    public List<CreditDocument> getCreditDocuments() {
        return creditDocuments;
    }

    public void setCreditDocuments(List<CreditDocument> creditDocuments) {
        this.creditDocuments = creditDocuments;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public int compareTo(InvoiceDocument o) {
        return getDate().compareTo(o.getDate());
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }
}