package com.csps.gabriel.usefull;

import java.util.List;

public class PolicyDocument {
    private String policy;
    private String client;
    private String balance;
    private String company;
    private String startDate;
    private String endDate;
    private String currency;
    private List<InvoiceDocument> invoiceDocuments;

    public String getPolicy() {
        return policy;
    }

    public void setPolicy(String policy) {
        this.policy = policy;
    }

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    public List<InvoiceDocument> getInvoiceDocuments() {
        return invoiceDocuments;
    }

    public void setInvoiceDocuments(List<InvoiceDocument> invoiceDocuments) {
        this.invoiceDocuments = invoiceDocuments;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }
}