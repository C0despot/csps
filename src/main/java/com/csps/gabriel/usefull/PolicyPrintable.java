package com.csps.gabriel.usefull;

public class PolicyPrintable {

    private String phone;
    private String celPhone;
    private String client;
    private String company;
    private String policyNumber;
    private String branch;
    private String endDate;
    private String startDate;
    private String debts;
    private String prime;
    private String currency;

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        if (phone == null)
            this.phone = "- - -";
        else
            this.phone = phone;
    }

    public String getCelPhone() {
        return celPhone;
    }

    public void setCelPhone(String celPhone) {
        if (celPhone == null)
            this.celPhone = "- - -";
        else
            this.celPhone = celPhone;
    }

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getPolicyNumber() {
        return policyNumber;
    }

    public void setPolicyNumber(String policyNumber) {
        this.policyNumber = policyNumber;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getDebts() {
        return debts;
    }

    public void setDebts(String debts) {
        this.debts = debts;
    }

    public String getPrime() {
        return prime;
    }

    public void setPrime(String prime) {
        this.prime = prime;
    }

    @Override
    public String toString() {
        return "PolicyPrintable{" +
                "phone='" + phone + '\'' +
                ", celPhone='" + celPhone + '\'' +
                ", client='" + client + '\'' +
                ", company='" + company + '\'' +
                ", policyNumber='" + policyNumber + '\'' +
                ", branch='" + branch + '\'' +
                ", endDate='" + endDate + '\'' +
                ", startDate='" + startDate + '\'' +
                ", debts='" + debts + '\'' +
                ", prime='" + prime + '\'' +
                '}';
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }
}
