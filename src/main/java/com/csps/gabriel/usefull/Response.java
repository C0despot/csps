package com.csps.gabriel.usefull;

/**
 * Created by Jose A Rodriguez on 5/11/2017.
 */

public class Response<T>{

    private boolean status = true;
    private String message;
    private T payload;
    private String messageType;
    public Response(){
        //Default
    }

    public Response(T payload){
        this.payload = payload;
    }

    public Response(T payload, String message){
        this.payload = payload;
        this.message = message;
    }

    public Response(T payload, boolean status){
        this.payload = payload;
        this.status = status;
    }

    public Response(boolean status, String message){
        this.status = status;
        this.message = message;
    }

    public Response(T payload, boolean status, String message, String messageType){
        this.payload = payload;
        this.status = status;
        this.message = message;
        this.messageType = messageType;
    }
    public Response(T payload, boolean status, String message){
        this.payload = payload;
        this.status = status;
        this.message = message;
    }

    public T getPayload(){
        return payload;
    }

    public boolean isStatus() {
        if(this.getPayload() == null){
            this.status = false;
            this.message = ("".equals(this.message) || null == this.message) ? "Unable to find the searched object." : this.message;
        }
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setPayload(T payload) {
        this.payload = payload;
    }

    public String getMessageType() {
        return messageType;
    }

    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }
}

