package com.csps.gabriel.usefull;

import java.awt.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by Jose A Rodriguez on 5/12/2017.
 */
public class Session implements Serializable {

    private String username;
    private Long userId;
    private String firstName;
    private String lastName;
    private Date lastSeen;
    private String role;
    private String firstLogin;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Date getLastSeen() {
        return lastSeen;
    }

    public void setLastSeen(Date lastSeen) {
        this.lastSeen = lastSeen;
    }

    public boolean isNotEmpty(){
        try {
            if (this.userId != null) {
                setLastSeen(new Date());
                return true;
            }
        }catch (Exception e){}
        this.flush();
        return false;
    }
    public void flush(){
        this.userId = null;
        this.username = null;
        this.lastName = null;
        this.firstName = null;
        this.lastSeen = null;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getFirstLogin() {
        return firstLogin;
    }

    public void setFirstLogin(String firstLogin) {
        this.firstLogin = firstLogin;
    }
}
