package com.csps.security.service;

import com.csps.security.model.User;

public interface UserService {
    void save(User user);

    User findByUsername(String username);
}
