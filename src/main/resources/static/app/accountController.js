app.controller('accountController', function ($scope, $rootScope, $routeParams, $location, $http, Data, $timeout, $route) {

    $scope.roles = [
        {"name": "ADMINISTRADOR", "index": "1"},
        {"name": "USUARIO REGULAR", "index": "2"},
        {"name": "CANCELADO", "index": "3"}
    ];

    $scope.$on('$viewContentLoaded', function() {
        $scope.user = {};
        $scope.appSpinner = true;
        $http.get('all-accounts')
            .then(function (response) {
               $scope.accountList = angular.copy(response.data.payload);
                $scope.appSpinner = false;
            });
    });

    $timeout(function () {
         if($location.path()=='/profile')
            $scope.getCurrentUser();
    }, 200);

    $scope.getCurrentUser = function () {
        $scope.appSpinner = true;
        $http.get(
          '/user/'+
              $scope.username
      ).then(function (response) {
            $scope.appSpinner = false;
            $scope.user = angular.copy(response.data.payload);
      });

    };
    $scope.createNewUser = function (user) {
         var newUser = angular.copy(user);
        $scope.appSpinner = true;
        $http.post(
             '/save-user',
             newUser
         ).then(function (response) {
            if (response.data.status ==true)
                $scope.user = {};
            if (response.data.payload.firstLogin == "N"){
                response.data.message = "Debe volver a iniciar session."
                Data.success("Usuario Creado");
                $route.reload();
                $timeout(function () {
                    $rootScope.authenticated = true;
                    $location.path('/do-logout');
                }, 3000);
            }
            $route.reload();
            $scope.appSpinner = false;
        });
    };

    $scope.gridOptions_accounts = {
        rowTemplate: '<div  ng-dblclick=\"grid.appScope.editAccount(row)\" ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader}"   ui-grid-cell></div>',
        infiniteScrollRowsFromEnd: 40,
        enableHorizontalScrollbar : 0,
        infiniteScrollUp: true,
        infiniteScrollDown: true,
        enableFiltering: true,
        enableSearch: true,
        columnDefs: [
            { name:'id', displayName: 'ID', width: '40%'},
            { name:'username', displayName: 'Nombre de Usuario', width: '60%', cellFilter: 'tel' },
        ],
        data: 'accountList',
        onRegisterApi: function(gridApi){

        }
    };

    $scope.editAccount = function (row) {
        $scope.appSpinner = true;
        console.dir(row);
        $http.get('get-account-by-id/'+angular.copy(row.entity).id)
            .then(function (response) {
               $scope.user = angular.copy(response.data.payload);
                $scope.appSpinner = false;
            });
    };

    $scope.accountList= [];
});