var policers = [];
var allPolicy = [];
var allClients = [];
var roles = [
    {"name": "ADMINISTRADOR", "index": "1"},
    {"name": "USUARIO REGULAR", "index": "2"},
    {"name": "CANCELADO", "index": "3"}
];
var branches = [];

 var payments = [
    {"name": "Efectivo", "index": "0"},
    {"name": "Tarjeta", "index": "1"},
    {"name": "Cheque", "index": "2"},
    {"name": "Deposito", "index": "3"}
];

var paymentTypes = [
    {"name": "Inicial", "index": "0"},
    {"name": "Total", "index": "3"},
    {"name": "Abono", "index": "1"},
    {"name": "Saldo", "index": "2"}
];

var app = angular.module('myApp', [
        'ngRoute',
        'ngAnimate',
        'toaster',
        'ngTouch',
        'ui.grid',
        'angucomplete-alt',
        'ngSanitize',
        'ui.select',
        '720kb.datepicker',
        'ui.grid.exporter',
        'ui.grid.selection',
        'ui.mask',
        'ui.utils.masks',
        'ui.bootstrap.showErrors',
        'angularSpinner',
        'ui-notification',
        'ui.grid.infiniteScroll',
        'ui.grid.autoResize',
        'ui.grid.edit',
        'ui.grid.cellNav',
        'ui.grid.expandable',
        'ui.grid.selection',
        'ui.grid.pinning'
]);
app.config(['$routeProvider',
  function ($routeProvider) {
        $routeProvider.
            when('/client', {
                title: 'Nuevo Cliente',
                templateUrl: 'partials/templates/newClient.html',
                controller: 'appCtrl'
            })
            .when('/login', {
                title: 'Inicio',
                templateUrl: 'partials/templates/login.html',
                controller: 'authCtrl'
            })
            .when('/policy', {
                title: 'NewPoliza',
                templateUrl: 'partials/templates/newPolicy.html',
                controller: 'appCtrl'
            })
            .when('/dashboard', {
                title: 'Dashboard',
                templateUrl: 'partials/templates/dashboard.html',
                controller: 'appCtrl'
            })
            .when('/invoice', {
                title: 'Facturacion',
                templateUrl: 'partials/templates/invoices.html',
                controller: 'appCtrl'
            })
            .when('/receipt', {
                title: 'Recibos',
                templateUrl: 'partials/templates/receipts.html',
                controller: 'movController'
            })
            .when('/credit', {
                title: 'Credits',
                templateUrl: 'partials/templates/credits.html',
                controller: 'movController'
            })
            .when('/claims', {
                title: 'Claims',
                templateUrl: 'partials/templates/claims.html',
                controller: 'claimController'
            })
            .when('/account', {
                title: 'account',
                templateUrl: 'partials/templates/account.html',
                controller: 'accountController'
            })
            .when('/profile', {
                title: 'Perfil',
                templateUrl: 'partials/templates/profile.html',
                controller: 'accountController'
            })
            .when('/company', {
                title: 'Company',
                templateUrl: 'partials/templates/companies.html',
                controller: 'companyController'
            })
            .when('/health', {
                title: 'Health',
                templateUrl: 'partials/templates/health.html',
                controller: 'healthController'
            })
            .when('/query', {
                title: 'Health',
                templateUrl: 'partials/templates/query.html',
                controller: 'queryController'
            }).when('/current', {
                title: 'Health',
                templateUrl: 'partials/templates/current.html',
                controller: 'queryController'
            })
            .when('/renew', {
                title: 'Renew',
                templateUrl: 'partials/templates/renew.html',
                controller: 'appCtrl'
            })
            .when('/cash', {
                title: 'Renew',
                templateUrl: 'partials/templates/cash.html',
                controller: 'cashCtrl'
            })
            .otherwise({
                redirectTo: '/dashboard'
            });
  }])
    .run(function ($rootScope, $location, Data, $http) {
        $rootScope.$on("$routeChangeStart", function (event, next, current) {
            $http.get('/get-all-companies')
                .then(function (response) {
                    policers = angular.copy(response.data.payload);
                });
            if ($rootScope.saveLocation != $location.path()) {
                $http.get('/session').then(function (response) {
                    response.data.payload = angular.copy(response.data.payload);
                    var results = response.data.payload;
                        $rootScope.authenticated = true;
                        $rootScope.username = results.username;
                });
                $rootScope.saveLocation = $location.path();
            }
            $http.get('/session').then(function (response) {
                var results = response.data.payload;
                    $rootScope.authenticated = true;
                    $rootScope.username = results.username;
            });

            allClients = [];
            $http.get('/get-all-clients-person').then(function (response) {
                if (response.data.status == true) {
                    allClients = allClients.concat(response.data.payload);
                }
            });
            $http.get('/get-all-branches')
                .then(function (response) {
                    branches = response.data.payload;
                }, function (error) {

                });

            $http.get('/get-all-policy').then(function (response) {
                if (response.data.status == true) {
                    allPolicy = angular.copy(response.data.payload);
                }
            });

            $http.get('/get-all-clients-company').then(function (response) {
                if (response.data.status == true) {
                    allClients = allClients.concat(response.data.payload);
                }
            });

        });
    });


/**
 * Created by Jose A Rodriguez on 7/20/2017.
 */
app.filter('tel', function () {
    return function (tel) {
        if (!tel) { return ''; }

        var value = tel.toString().trim().replace(/^\+/, '');

        if (value.match(/[^0-9]/)) {
            return tel;
        }

        var country, city, number;

        if (value.length==10){
            country='';
            city = value.slice(0, 3);
            number = value.slice(3);
        }
        else {
            city = value.slice(0, 3);
            number = value.slice(3, 10);
            country = ' Ext. '+value.slice(10);
        }
        if (country == 1) {
            country = "";
        }

        number = number.slice(0, 3) + '-' + number.slice(3);

        return (" (" + city + ") " + number + country).trim();
    };
});

app.filter('entities', function () {
    return function (entity) {
        policers.forEach(function (pol) {
            if (pol.id == entity){
                entity= pol.name;
                return;
            }
        });
        return entity.toUpperCase();
    }
});

app.filter('branch', function () {
    return function (value) {
        branches.forEach(function (branch) {
            if (branch.branchId === value) {
                value = branch.branchDescription;
                return;
            }
        });
        return (value+"").toUpperCase();
    }
})

app.filter('roles', function () {
    return function (rol) {
        return roles[rol-1].name;
    }
});

app.filter('policyFilter', function () {
    return function (policyId) {
        allPolicy.forEach(function (policy) {
           if(policy.policyId == policyId)
               policyId = policy.policyNumber;
            return
        });
        return  policyId;
    }
});

app.filter('clientFilter', function () {
    return function (clientId) {
        allClients.forEach(function (client) {
            if (client.clientId == clientId){
                clientId = client.fullName;
                return;
            }
        });
        return clientId;
    }
});

app.filter('paymentMode', function () {
    return function (id) {
        return payments[id].name.toUpperCase();
    }
});

app.filter('paymentType', function () {
    return function (id) {
        return paymentTypes[id].name.toUpperCase();
    }
});

