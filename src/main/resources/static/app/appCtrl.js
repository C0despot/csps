app.controller('appCtrl', function ($scope, $rootScope, $routeParams, $location, $http, Data, $timeout, $route, $filter) {

    $scope.saveLocation = '';
    $scope.login = {};
    $scope.signup = {};
    $scope.appSpinner--;
    $scope.authenticated = false;
    $scope.totalPolicyElements = 0;
    $scope.totalInvoiceElements = 0;
    $scope.policyViewMode = false;
    $scope.clientViewMode = false;
    $scope.addGoodCleaner = function () {
        $scope.goods = {};
        $scope.editGoodFlag = false;
    };

    $scope.fireGoodsList = [];

    $scope.disablePolicyForm = false;

    $scope.$on('$locationChangeStart', function (evt, absNewUrl, absOldUrl) {
        if ('http://localhost:8000/#/invoice' == absNewUrl && 'http://localhost:8000/#/policy' == absOldUrl && Data.policy != null) {
            location.reload();
        }
    });

    $scope.$on('$locationChangeSuccess', function (event, newUrl, oldUrl) {
        if (oldUrl.indexOf("policy") > -1) {
            Data.policyViewMode = false;
            Data.editMode = false;
        }
    });
    var externalDataRetrievedFromServer = [
        {name: 'Bartek', age: 34},
        {name: 'John', age: 27},
        {name: 'Elizabeth', age: 30},
    ];

    function buildTableBody(data, columns) {
        var body = [];

        body.push(columns);

        data.forEach(function (row) {
            var dataRow = [];

            columns.forEach(function (column) {
                dataRow.push(row[column].toString());
            })

            body.push(dataRow);
        });

        return body;
    }

    function table(data, columns) {
        return {
            table: {
                headerRows: 1,
                body: buildTableBody(data, columns),
                footerRows: 1
            }
        };
    }

    var dd = {
        pageOrientation: 'landscape',
        content: [
            {text: 'Dynamic parts', style: 'header'},
            table(externalDataRetrievedFromServer, ['name', 'age']),
        ],
        columnGap: 100,
        styles: {
            header: {
                fontSize: 22,
                bold: true
            }
        }
    }
    $scope.validatePolicyNumber = function (value) {
        $http.get('validate-policy/' + value)
            .then(function (response) {
                if (response.data.payload == true) {
                    $scope.policyNumberStyle = {color: 'black'};
                    $scope.disablePolicyForm = false;
                }
                else {
                    $scope.policyNumberStyle = {color: 'red'};
                    $scope.disablePolicyForm = true;
                    Data.error("Numero de Poliza Existe");
                }
            }, function (error) {
                Data.error("Algo salio mal");
            });
    };

    $scope.validateClientCardId = function (value) {
        $http.get('validate-client/' + value)
            .then(function (response) {
                if (response.data.payload == true) {
                    $scope.clientCardIdStyle = {color: 'black'};
                    $scope.disableClientForm = false;
                }
                else {
                    $scope.clientCardIdStyle = {color: 'red'};
                    $scope.disableClientForm = true;
                    Data.error("Cedula o RNC ya registrado");
                }
            }, function (error) {
                Data.error("Algo salio mal");
            });
    };

    $scope.validateInvoiceNumber = function (value) {
        $http.get('validate-invoice/' + value)
            .then(function (response) {
                if (response.data.payload == true) {
                    $scope.invoiceNumberStyle = {color: 'black'};
                    $scope.disableInvoiceForm = false;
                }
                else {
                    $scope.invoiceNumberStyle = {color: 'red'};
                    $scope.disableInvoiceForm = true;
                    Data.error("Numero de Factura ya registrado");
                }
            }, function (error) {
                Data.error("Algo salio mal");
            });
    };

    $scope.$on('$viewContentLoaded', function () {
        $scope.policyViewMode = Data.policyViewMode;
        $scope.invoiceViewMode = Data.invoiceViewMode;
        $http.get('/get-all-branches')
            .then(function (response) {
                $scope.branches = response.data.payload;
            }, function (error) {

            });
        $http.get(
            'next-integrity-number'
        ).then(function (response) {
            try {
                if (typeof $scope.policy.policyNumber === 'undefined')
                    $scope.policy.policyNumber = response.data.payload.nextPolicyNumber;
            } catch (error) {
            }
            try {
                if (typeof $scope.invReg.invoiceNumber === 'undefined')
                    $scope.invReg.invoiceNumber = response.data.payload.nextInvoiceNumber;
            } catch (error) {
            }
        });

        // $scope.reinit();
        if (Data.invoice !== null) {
            $scope.appSpinner++;
            var clientId = Data.invoice.client.clientId;
            $http.get(
                '/get-by-client/' + clientId
            ).then(function (response) {
                $scope.updatedLinkedPolicies = response.data.payload;
                $scope.invReg = angular.copy(Data.invoice);
                $scope.saveData.client = angular.copy(Data.invoice.client);
                $scope.$broadcast('angucomplete-alt:changeInput', 'invRegClient', Data.invoice.client);
                $scope.appSpinner--;
            });
        }
        $http.get('/get-all-companies')
            .then(function (response) {
                $scope.policers = angular.copy(response.data.payload);
            });
        if ($scope.firstLogin == 'Y')
            $location.path('/profile')
    });

    var that = $scope;
    that.stateName = null;

    that.autoCompleteOptions = {
        minimumChars: 1,
        data: function (term) {
            term = term.toUpperCase();
            var match = _.filter(CSS_COLORS, function (value) {
                return value.name.startsWith(term);
            });
            return _.pluck(match, 'name');
        }
    };

    $scope.ok = function () {
        $.SmartMessageBox({
            title: "<i class='fa fa-trash-o' style='color:red'></i> Delete Promotion?",
            content: "Are you sure you want to delete the selected promotions?",
            buttons: '[No][Si]'
        }, function (ButtonPressed) {
            if (ButtonPressed == "Si") {
                scope.deletep();
            }
        });

    }
    $scope.countriess = {};

    $scope.look = function (str) {
        console.log(str);
        return str.localeCompare('1') == 0;
    };


    $scope.saveClient = function (client) {

        var newClient = angular.copy(client);

        $scope.loading++;
        $http.post('/save-client', newClient).then(function (response) {
            $scope.clientViewMode = false;
            $scope.refreshGrids();
            $scope.newClient = [];
            $scope.loading--;
            Data.success("Completado");
            $route.reload();
        }, function (error) {
            Data.error("Algo Salio Mal");
        });
    };
    $scope.states = [];
    $scope.refreshGrids = function () {
        $http.get('/get-all-clients-person').then(function (response) {
            if (response.data.status == true) {
                $scope.myData_person = response.data.payload;
            }
        });

        $http.get('/get-all-clients-company').then(function (response) {
            if (response.data.status == true) {
                $scope.myData_company = response.data.payload;
            }
        });
    };

    $scope.gridOptions_person = {
        enableFiltering: true,
        rowTemplate: '<div  ng-dblclick=\"grid.appScope.editClient(row)\" ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader}"   ui-grid-cell></div>',
        infiniteScrollRowsFromEnd: 40,
        infiniteScrollUp: true,
        infiniteScrollDown: true,
        columnDefs: [
            {name: 'clientId', displayName: 'Id De Cliente', width: '80'},
            {name: 'fullName', displayName: 'Nombre', width: '220'},
            {name: 'idCard', displayName: 'Cedula', width: '180'},
            {name: 'phoneNumber', displayName: 'Telefono', cellFilter: 'tel', width: '130'},
        ],
        data: 'myData_person',
        onRegisterApi: function (gridApi) {

        }
    };
    $scope.gridOptions_company = {
        enableFiltering: true,
        rowTemplate: '<div  ng-dblclick=\"grid.appScope.editClient(row)\" ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader}"   ui-grid-cell></div>',
        infiniteScrollRowsFromEnd: 40,
        infiniteScrollUp: true,
        infiniteScrollDown: true,
        columnDefs: [
            {name: 'clientId', displayName: 'Id De Cliente', width: '80'},
            {name: 'enterprise', displayName: 'Empresa', width: '220'},
            {name: 'rnc', displayName: 'RNC', width: '180'},
            {name: 'phoneNumber', displayName: 'Telefono', cellFilter: 'tel', width: '130'},
        ],
        data: 'myData_company',
        onRegisterApi: function (gridApi) {

        }
    };

    $scope.cities = [
        {"name": "Santo Domingo Este", "index": "25"},
        {"name": "Santo Domingo Norte", "index": "26"},
        {"name": "Santo Domingo Oeste", "index": "27"},
        {"name": "Distrito Nacional", "index": "28"},
        {"name": "San Cristobal", "index": "1"},
        {"name": "Bajos de Haina", "index": "2"},
        {"name": "Cambita Garabito", "index": "3"},
        {"name": "Los Cacaos", "index": "4"},
        {"name": "Sabana Grande de Palenque", "index": "5"},
        {"name": "San Gregorio Nigua", "index": "6"},
        {"name": "Villa Altagracia", "index": "7"},
        {"name": "Yaguate", "index": "8"},
        {"name": "Peravia", "index": "9"},
        {"name": "Bani", "index": "10"},
        {"name": "Catalina", "index": "11"},
        {"name": "El Carreton", "index": "12"},
        {"name": "El Limonal", "index": "13"},
        {"name": "La Barias", "index": "14"},
        {"name": "Paya", "index": "15"},
        {"name": "Villa Fundacion", "index": "16"},
        {"name": "Villa Sombrero", "index": "17"},
        {"name": "Matanzas", "index": "18"},
        {"name": "Sabana Buey", "index": "19"},
        {"name": "Nizao", "index": "20"},
        {"name": "Santana", "index": "21"},
        {"name": "Pizarrete", "index": "22"},
        {"name": "Villa Altagracia", "index": "23"},
        {"name": "Yaguate", "index": "24"}
    ];

    // POLICYYYYYYYYYYYYYYYYYYYYYYY  //
    $scope.branches = [];
    $scope.policers = [];

    $scope.coins = [
        {"name": "RD $ [PESO]", "index": "1"},
        {"name": "US $ [DOLAR]", "index": "2"},
        {"name": "EU € [EURO]", "index": "3"}
    ];

    $scope.statuses = [
        {"name": "Vigente", "index": "1"},
        {"name": "No Vigente", "index": "2"},
        {"name": "Cancelada", "index": "3"}
    ];

    $scope.itbises = [
        {"name": "No", "index": "1"},
        {"name": "Si", "index": "2"}
    ];

    $scope.removeFilter = function () {
        $scope.filter = {};
        $scope.restartPolicyList();
    };

    $scope.filtering = function (toSearch) {
        var searchObject = angular.copy(toSearch);
        console.log(toSearch);
        searchObject.startDate = searchObject.startDate ? Data.parseToDate(searchObject.startDate, "-") : null;
        searchObject.endDate = searchObject.endDate ? Data.parseToDate(searchObject.endDate, "-") : null;

        $http.post(
            '/filtering-policy',
            searchObject
        ).then(function (response) {
            if (response.data.payload != null)
                $scope.allPolicyDash = response.data.payload;

        });
    };
    $scope.bienes_asegurados = {
        rowTemplate: '<div  ng-dblclick=\"grid.appScope.toggleModal(row, grid.renderContainers.body.visibleRowCache.indexOf(row))\" ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader}"   ui-grid-cell></div>',
        enableColumnResizing: true,
        saveFocus: false,
        saveScroll: false,
        saveSelection: false,
        enableGridMenu: true,
        columnDefs: [
            {
                name: '  ',
                displayName: '  ',
                cellTemplate: '<div class="ui-grid-cell-contents" style="text-align: center;">{{grid.renderContainers.body.visibleRowCache.indexOf(row)+1}}</div>',
                width: '35'
            },
            {
                name: ' ',
                displayName: ' ',
                width: 35,
                cellTemplate: '<div style="text-align: center; margin-top: 6px;"><a data-ng-click="grid.appScope.sliceGood(row);"><i class="fa fa-trash-o" aria-hidden="true"></i></a></div>'
            },
            {name: 'type', displayName: 'Tipo de Vehiculo', width: '160'},
            {name: 'brand', displayName: 'Marca', width: '160'},
            {name: 'model', displayName: 'Modelo', width: '160'},
            {name: 'year', displayName: 'Año', width: '70'},
            {name: 'chassisSerial', displayName: 'Chasis', width: '120'},
            {name: 'prime', displayName: 'Prima', width: '90', cellFilter: 'currency'},
            {name: 'color', displayName: 'color', width: '100'},
            {name: 'cylinder', displayName: 'Cilindraje', width: '100'},
            {name: 'passengerAmount', displayName: 'Pasajeros', width: '100'},
            {name: 'weight', displayName: 'Peso', width: '70'},
            {name: 'services', displayName: 'Servicios', width: '160'},
            {name: 'covertAmount', displayName: 'Valor Asegurado', width: '100', cellFilter: 'currency'}

        ],
        data: 'goodsList',
        onRegisterApi: function (gridApi) {

            $scope.grid = gridApi;
        }
    };

    $scope.toggleModal = function (row, index) {
        $scope.rowIndex = index;
        $scope.goods = angular.copy(row.entity);
        $scope.editGoodFlag = true;
        $("#goodsModal").modal();
    };

    $scope.toggleModalTwo = function (index) {
        $scope.goods = angular.copy($scope.goodsList[index]);
        $scope.editGoodFlag = true;
        $("#goodsModal").modal();
    };

    $scope.sliceGood = function (row) {

        $.SmartMessageBox({
            title: "<i class='fa fa-trash-o' style='color:red'></i> Borrar Registro?",
            content: "Esta seguro que desea eliminar el registro seleccionado?",
            buttons: '[No][Yes]'
        }, function (ButtonPressed) {
            if (ButtonPressed == "Yes") {
                var count = 0;
                $scope.goodsList.forEach(function (good) {
                    if (good.goodsId == row.entity.goodsId) {
                        $http.delete('/delete-one-record/' + good.goodsId + '/' + $scope.username)
                            .then(function (response) {
                                if (response.data.status == true) {
                                    delete $scope.goodsList[count];
                                    $scope.cobGoodsList = [];
                                    $scope.goodsList.forEach(function (goodLine) {
                                        var cobGoodsList = $scope.selectedCoverts[goodLine.covert];
                                        $scope.cobGoodsList.push(cobGoodsList);
                                    });
                                }
                            });

                        return;
                    }
                    count++;
                });
            }
        });

    };

    $scope.slicePolicy = function (row) {

        $.SmartMessageBox({
            title: "<i class='fa fa-trash-o' style='color:red'></i> Borrar Poliza?",
            content: "Esta seguro que desea eliminar el registro seleccionado?",
            buttons: '[No][Yes]'
        }, function (ButtonPressed) {
            if (ButtonPressed == "Yes") {
                $http.delete(
                    '/delete-policy/' +
                    row.entity.policyId
                ).then(function (response) {
                    $scope.restartPolicyList();
                })
            }
        });

    };

    $scope.restartPolicyList = function () {
        $scope.loading++;
        $http.get('/get-all-policy/1')
            .then(function (data) {
                $scope.loading--;
                $scope.startPolicyPage = 1;
                $scope.allPolicyDash = data.data.payload.content;
                $scope.totalPolicyElements = data.data.payload.totalElements;
            });
    };

    $scope.cob_bienes_asegurados = {
        rowTemplate: '<div  ng-dblclick=\"grid.appScope.toggleModalTwo( grid.renderContainers.body.visibleRowCache.indexOf(row))\" ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader}"   ui-grid-cell></div>',
        enableColumnResizing: true,
        saveFocus: false,
        saveScroll: false,
        saveSelection: false,
        enableGridMenu: true,
        columnDefs: [
            {
                name: ' ',
                displayName: ' ',
                cellTemplate: '<div class="ui-grid-cell-contents" style="text-align: center;">{{grid.renderContainers.body.visibleRowCache.indexOf(row)+1}}</div>',
                width: '35'
            },
            {name: 'damage', displayName: 'Daños a la Propiedad Ajena', width: '220', cellFilter: 'currency'},
            {
                name: 'injuriesJustOne',
                displayName: 'Lesiones Corporales o Muerte a Una Persona',
                width: '340',
                cellFilter: 'currency'
            },
            {
                name: 'InjuriesMoreThanOne',
                displayName: 'Lesiones Corporales o Muerte a Mas de Una Persona',
                width: '390',
                cellFilter: 'currency'
            },
            {name: 'risk', displayName: 'Riesgo al conductor', width: '180', cellFilter: 'currency'},
            {
                name: 'riskBack',
                displayName: 'Riesgo Peones [Camiones - Camionetas]',
                width: '320',
                cellFilter: 'currency'
            },
            {name: 'lawBill', displayName: 'Fianza Judicial', width: '160', cellFilter: 'currency'},
            {
                name: 'deadExpenses',
                displayName: 'Plan Gastos Funerarios Conductor',
                width: '290',
                cellFilter: 'currency'
            }
        ],
        data: 'cobGoodsList',
        onRegisterApi: function (gridApi) {

        }
    };

    $scope.fire_goods = {
        rowTemplate: '<div  ng-dblclick=\"grid.appScope.toggleModalThree( grid.renderContainers.body.visibleRowCache.indexOf(row))\" ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader}"   ui-grid-cell></div>',
        enableColumnResizing: true,
        saveFocus: false,
        saveScroll: false,
        saveSelection: false,
        enableGridMenu: true,
        columnDefs: [
            {
                name: ' ',
                displayName: ' ',
                cellTemplate: '<div class="ui-grid-cell-contents" style="text-align: center;">{{grid.renderContainers.body.visibleRowCache.indexOf(row)+1}}</div>',
                width: '35'
            },
            {
                name: 'description',
                displayName: 'Bienes Asegurados',
                cellEditableCondition: true,
                cellFilter: 'uppercase'
            },
            {name: 'securedSum', displayName: 'Suma Asegurada', cellFilter: 'currency', cellEditableCondition: true}
        ],
        data: 'fireGoodsList',
        onRegisterApi: function (gridApi) {

        }
    };

    $scope.local_options = {
        rowTemplate: '<div  ng-dblclick=\"grid.appScope.toggleLocalModal( grid.renderContainers.body.visibleRowCache.indexOf(row))\" ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader}"   ui-grid-cell></div>',
        enableColumnResizing: true,
        saveFocus: false,
        saveScroll: false,
        saveSelection: false,
        enableGridMenu: true,
        columnDefs: [
            {
                name: ' ',
                displayName: ' ',
                cellTemplate: '<div class="ui-grid-cell-contents" style="text-align: center;">{{grid.renderContainers.body.visibleRowCache.indexOf(row)+1}}</div>',
                width: '35'
            },
            {name: 'name', displayName: 'Local', cellFilter: 'uppercase'},
            {name: 'localType', displayName: 'Tipo de Local', cellFilter: 'uppercase'},
            {name: 'buildingType', displayName: 'Tipo de Construccion', cellFilter: 'uppercase'},
            {name: 'address', displayName: 'Direccion', cellFilter: 'uppercase'}
        ],
        data: 'localList',
        onRegisterApi: function (gridApi) {

        }
    };

    var localIndex = 0;
    $scope.local = {};
    $scope.toggleLocalModal = function (index) {
        $scope.rowIndex = index;
        localIndex = angular.copy(index);
        $scope.local = angular.copy($scope.localList[index]);
        $scope.editGoodFlag = true;
        $scope.fireGoodsList = JSON.parse($scope.local.goodList)
        $("#localModal").modal();
        //todo
    }
    $scope.localList = [];
    $scope.saveLocal = function (local) {
        setTimeout(function () {
            if ($scope.editGoodFlag === false) {
                local = angular.copy(local);
                local.goodList = JSON.stringify($scope.fireGoodsList);
                $scope.fireGoodsList = [];
                $scope.local = {};
                $scope.localList.push(local);
                $scope.$apply();
            }
            else {
                local = angular.copy(local);
                local.goodList = JSON.stringify($scope.fireGoodsList);
                $scope.fireGoodsList = [];
                $scope.local = {};
                $scope.localList[localIndex] = local;
                $scope.editGoodFlag = false;
                $scope.$apply();
            }
        }, 500);
    };

    $scope.addNewRow = function () {
        $scope.fireGoodsList.push({description: "Doble Click", securedSum: 0})
    };

    $scope.services = [
        {"name": "Privado", "index": "1"},
        {"name": "Publico", "index": "2"},
        {"name": "Carga", "index": "3"},
        {"name": "Otros", "index": "4"}
    ];

    $scope.includes = [
        {"name": "Incluido", "index": "1"},
        {"name": "No Incluido", "index": "2"}
    ];

    $scope.selfDamages = [
        {"name": "Comprensivo", "index": "1"},
        {"name": "Colision y Vuelco", "index": "2"}
    ];
    $scope.goodsTypes = [
        {"name": "Jeep", "index": "1"},
        {"name": "Automóvil", "index": "2"},
        {"name": "Camioneta", "index": "3"},
        {"name": "Camion", "index": "4"},
        {"name": "Furgoneta", "index": "5"},
        {"name": "Minibus", "index": "6"},
        {"name": "Bus", "index": "7"},
        {"name": "Monta Cargas", "index": "8"},
        {"name": "Montocicleta", "index": "9"}
    ];
    $scope.goodsList = [];

    $scope.saveGood = function (good) {
        var goodToAdd = angular.copy(good);
        goodToAdd.type = Data.map($scope.goodsTypes, goodToAdd.type);
        var cobGoodsList = $scope.selectedCoverts[goodToAdd.covert];

        if ($scope.editGoodFlag == false) {
            $scope.cobGoodsList.push(cobGoodsList);
            $scope.goodsList.push(goodToAdd);
        }
        else {
            $scope.goodsList[$scope.rowIndex] = angular.copy(goodToAdd);
            $scope.cobGoodsList[$scope.rowIndex] = angular.copy(cobGoodsList);
        }

    };

    $scope.initState = function (state) {
        if (typeof state == 'undefined')
            return $scope.statuses[0].index;
        return state;
    };
    $scope.editGoodFlag = false;
    $scope.flag = false;
    $scope.savePolicy = function (policy) {

        $scope.appSpinner++;
        policy.cobGoodsList = angular.copy($scope.cobGoodsList);
        var nPolicy = angular.copy(policy);
        if (typeof nPolicy.policyId == 'undefined')
            $scope.flag = true;

        nPolicy.status = Data.map($scope.statuses, nPolicy.status);
        nPolicy.branch = Data.map($scope.branches, nPolicy.branch);
        nPolicy.policer = Data.map($scope.policers, nPolicy.policer);
        nPolicy.currency = Data.map($scope.coins, nPolicy.currency);
        nPolicy.client = nPolicy.client.originalObject;
        var dateParts = nPolicy.startDate.split('/');
        nPolicy.startDate = new Date(dateParts[2], dateParts[1] - 1, dateParts[0]);
        dateParts = nPolicy.endDate.split('/');
        nPolicy.endDate = new Date(dateParts[2], dateParts[1] - 1, dateParts[0]);

        $http.post('/save-policy', nPolicy)
            .then(function (response) {
                $scope.appSpinner--;
                if (response.data.payload.branch !== 100000001) {
                    $scope.goodsList.forEach(function (good) {
                        $scope.editGoodFlag = false;
                        good.policyLinked = response.data.payload.policyId;
                        $scope.saveGoodList(good);
                    });
                } else if ($scope.localList.length > 0) {
                    var localList = [];
                    $scope.localList.forEach(function (t) {
                        t.policyLinked = response.data.payload.policyId;
                        localList.push(t)
                    });
                    $scope.appSpinner++;
                    $http.post(
                        '/save-locals',
                        localList
                    ).then(function (response) {
                        $scope.appSpinner--;
                        Data.success("Documento Guardado")
                    }, function (error) {
                        Data.error("Oops... Algo Salio Mal :(");
                        $scope.appSpinner--;
                    });
                }
                $scope.fireGoodsList = [];
                $location.path('/invoice');
                Data.policy = angular.copy(response.data.payload);
                Data.policy.goodss = [];
                Data.clientList = angular.copy($scope.clientList);
                $scope.reinit();
                $scope.editMode = false;
                Data.success("Completado");
                $route.reload();
            }, function (error) {
                Data.error("Algo Salio Mal");
            });
    };

    $scope.saveFireGood = function (fireGood) {
        $scope.fireGoodsList.push(fireGood);
    };
    $scope.saveGoodList = function (good) {
        $http.post('/save-goods', good)
            .then(function (response) {
                Data.policy.goodss.push(response.data.payload);
            });
    };

    $scope.filteringBy = function (search) {
        $http.get(
            '/get-policy-by-criteria/' + search.criteria + '/' + search.searchObject
        ).then(function (response) {
            $scope.allPolicyDash = [];
            $scope.allPolicyDash = angular.copy(response.data.payload);
        });
    };

    $scope.filterMask = "";
    $scope.changeMask = function (value) {
        if (value == 2) {
            $scope.filterMask = "9-99-99999-9"
        } else if (value == 3) {
            $scope.filterMask = "999-9999999-9"
        } else {
            $scope.filterMask = "";
        }
        console.log($scope.filterMask);

    };

    $scope.addBranch = function (branch) {

        $scope.appSpinner++;
        $http.post(
            '/save-branch',
            branch
        ).then(function (response) {
            console.log($("#primePolicyForm").val())
            $("#primePolicyForm").val('');
            $scope.branch = '';
            $rootScope.branch = '';
            location.reload();
            $scope.getBranches();
            // $('#addBranchModal').modal().hide();
        }, function (error) {
            $("#primePolicyForm").val('');
            $scope.appSpinner--;
            $scope.branch = '';
            $rootScope.branch = '';
            Data.error("Algo Salio Mal");

            // $('#addBranchModal').modal().hide();
        })
    };

    $scope.getBranches = function () {
        $http.get('/get-all-branches')
            .then(function (response) {
                $scope.appSpinner--;
                $scope.branches = response.data.payload;
                Data.success("Completado");
            }, function (error) {

            });
    };


    $scope.criterias = [
        {"name": "Registro", "index": "0"},
        {"name": "Chassis", "index": "1"},
        {"name": "rnc", "index": "2"},
        {"name": "Cedula", "index": "3"}
    ];

    $scope.invoicesTypes = [
        {"name": "Emision", "index": "0"},
        {"name": "Renovacion", "index": "1"},
        {"name": "Aumento", "index": "2"}
    ];

    $scope.careTypes = [
        {"name": "Seguro de Ley", "index": "1"},
        {"name": "Seguro con Daños Propios", "index": "2"}
    ];


    $scope.coverts = [
        {"name": "100/100/200", "index": "0"},
        {"name": "200/200/400", "index": "1"},
        {"name": "250/250/500", "index": "2"},
        {"name": "300/300/600", "index": "3"},
        {"name": "500/500/1000", "index": "4"},
        {"name": "1000/1000/2000", "index": "5"},
        {"name": "2000/2000/4000", "index": "6"},
        {"name": "50/50/100", "index": "7"}
    ];

    $scope.selectedCoverts = [
        {
            "damage": 100000,
            "injuriesJustOne": 100000,
            "InjuriesMoreThanOne": 200000,
            "risk": 15000,
            "riskBack": 30000,
            "lawBill": 300000,
            "deadExpenses": 20000
        },
        {
            "damage": 200000,
            "injuriesJustOne": 200000,
            "InjuriesMoreThanOne": 400000,
            "risk": 25000,
            "riskBack": 40000,
            "lawBill": 500000,
            "deadExpenses": 25000
        },
        {
            "damage": 250000,
            "injuriesJustOne": 250000,
            "InjuriesMoreThanOne": 500000,
            "risk": 15000,
            "riskBack": 30000,
            "lawBill": 500000,
            "deadExpenses": 20000
        },
        {
            "damage": 300000,
            "injuriesJustOne": 300000,
            "InjuriesMoreThanOne": 600000,
            "risk": 15000,
            "riskBack": 30000,
            "lawBill": 500000,
            "deadExpenses": 20000
        },
        {
            "damage": 500000,
            "injuriesJustOne": 500000,
            "InjuriesMoreThanOne": 1000000,
            "risk": 25000,
            "riskBack": 40000,
            "lawBill": 1000000,
            "deadExpenses": 25000
        },
        {
            "damage": 1000000,
            "injuriesJustOne": 1000000,
            "InjuriesMoreThanOne": 2000000,
            "risk": 50000,
            "riskBack": 50000,
            "lawBill": 1000000,
            "deadExpenses": 40000
        },
        {
            "damage": 2000000,
            "injuriesJustOne": 2000000,
            "InjuriesMoreThanOne": 4000000,
            "risk": 50000,
            "riskBack": 50000,
            "lawBill": 1000000,
            "deadExpenses": 40000
        },
        {
            "damage": 50000,
            "injuriesJustOne": 50000,
            "InjuriesMoreThanOne": 100000,
            "risk": 0,
            "riskBack": 0,
            "lawBill": 100000,
            "deadExpenses": 0
        }
    ];

    $scope.cobGoodsList = [];

    $scope.columns = [
        {title: "Num. Poliza", dataKey: "policyNumber"},
        {title: "Cliente Poliza", dataKey: "client"},
        {title: "Telefono", dataKey: "phone"},
        {title: "Celular", dataKey: "celPhone"},
        {title: "Prima", dataKey: "prime"},
        {title: "Inicio Vigencia", dataKey: "startDate"},
        {title: "Fin Vigencia", dataKey: "endDate"},
        {title: "Balance", dataKey: "debts"},
        {title: "Moneda", dataKey: "currency"}
    ];
    $scope.pdfExport = function () {
        var myWindow = window.open();
        $http.get("/print-all-policy/" + $scope.filter.enterprise + "/" + $scope.filter.startDate + "/" + $scope.filter.endDate).then(function (response) {
            doPrint(response.data.payload, myWindow);
        }, function (error) {

        });
    };

    var doPrint = function (object, x) {
        var enterprise = $scope.filter.enterprise;
        $scope.policers.forEach(function (t) {
            if ($scope.filter.enterprise === t.id) {
                enterprise = t.name;
                return;
            }
        })

        var doc = new jsPDF("landscape");
        doc.text(15, 20, "RESUMEN");
        var textWidth = doc.getStringUnitWidth((enterprise + "").toUpperCase()) * doc.internal.getFontSize() / doc.internal.scaleFactor;
        var textOffset = (doc.internal.pageSize.width - textWidth);
        doc.text(textOffset - 15, 20, (enterprise + "").toUpperCase());
        doc.setLineWidth(0.5);
        doc.line(15, 25, textOffset - 15 + textWidth, 25);
        doc.autoTable($scope.columns, object, {startY: 30, startX: 20});
        doc.setProperties({
            title: 'Polizas - PDF'
        });
        var string = doc.output('datauristring');

        var iframe = "<iframe width='100%' height='100%' src='" + string + "'></iframe>"

        x.document.open();
        x.document.title = 'Polizas - PDF';
        x.document.write(iframe);
        x.document.close();
    }

    var generateDashPdfData = function (object) {
        var anotherObject = [];
        object.forEach(function (obj) {
            var data = {};
            data.fullName = $filter('clientFilter')(obj.client.fullName);
            data.policer = $filter('entities')(obj.policer);
            data.phone = obj.client.phoneNumber;
            data.policyNumber = obj.policyNumber;
            data.endDate = $scope.shortDate(obj.endDate);
            anotherObject.push(data);
        });

        return anotherObject;
    };

    $scope.getBkgColorTable = function (row) {
        var after30Days = new Date(Date.now() + (30 * 24 * 60 * 60 * 1000));
        var policyDate = new Date(row.entity.endDate);
        var todayDate = new Date();
        if (policyDate.getTime() <= todayDate.getTime())
            return 'red';
        else if (after30Days.getTime() >= policyDate.getTime())
            return '#f4d742';
        return '';
    };

    $scope.shouldShow = function (value) {
        return value.branchDescription.toLowerCase().indexOf('salud') === -1;
    }
    $scope.gridOptions_dash = {
        rowHeight: 42,
        enableRowSelection: true,
        enableRowHeaderSelection: false,
        multiSelect: false,
        modifierKeysToMultiSelect: false,
        noUnselect: true,
        enableHorizontalScrollbar: 1,
        enableFiltering: true,
        infiniteScrollDown: true,
        rowTemplate: '<div  ng-style="rowStyle" ng-dblclick=\"grid.appScope.editPolice(row.entity)\" ' +
        'ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" ' +
        'class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader}"' +
        'ng-mouseover="rowStyle={\'color\': \'#114ee8\',\'font-weight\' : \'bold\'}" ng-mouseleave="rowStyle={}" ' +
        'style="background-color: {{grid.appScope.getBkgColorTable(row)}}" ui-grid-cell></div>',
        columnDefs: [
            {
                name: ' ',
                displayName: ' ',
                enableFiltering: false,
                width: '60',
                cellTemplate: '<div style="text-align: center; margin-top: 6px;"><a data-ng-click="grid.appScope.slicePolicy(row);"><i class="fa fa-trash-o" aria-hidden="true"></i></a></div>'
            },
            {
                name: 'policyNumber', displayName: 'Numero De Poliza', width: '150', filter: {
                    noTerm: true,
                    condition: true
                }
            },
            {
                name: 'branch', displayName: 'Ramos', width: '120', cellFilter: 'branch', filter: {
                    noTerm: true,
                    condition: true
                }
            },
            {
                name: 'client.fullName', displayName: 'Cliente', width: '280', filter: {
                    noTerm: true,
                    condition: true
                }
            },
            {
                name: 'client.phoneNumber', displayName: 'Telefono', width: '130', cellFilter: 'tel', filter: {
                    noTerm: true,
                    condition: true
                }
            },
            {
                name: 'startDate',
                enableFiltering: false,
                displayName: 'Inicio de Vigencia',
                width: '120',
                type: 'date',
                cellFilter: 'date:\'dd-MM-yyyy\''
            },
            {
                name: 'endDate',
                enableFiltering: false,
                displayName: 'Fin de Vigencia',
                width: '120',
                type: 'date',
                cellFilter: 'date:\'dd-MM-yyyy\''
            },
            {
                name: 'policer',
                enableFiltering: false,
                displayName: 'Compania Aseguradora',
                width: '300',
                cellFilter: 'entities'
            },
            {name: 'status', enableFiltering: false, displayName: 'Estado', width: '110'}
        ],
        data: 'allPolicyDash',
        onRegisterApi: function (gridApi) {
            $scope.gridDashOption = gridApi;
            $scope.gridDashOption.core.on.filterChanged($scope, $scope.doPolicyFilter);
            gridApi.infiniteScroll.on.needLoadMoreData($scope, $scope.getMorePolicy);
        }
    };


    $scope.doPolicyFilter = function () {
        var policy = this.grid.columns[1].filters[0].term;
        var branch = this.grid.columns[2].filters[0].term;
        var client = this.grid.columns[3].filters[0].term;
        var phone = this.grid.columns[4].filters[0].term;
        var search = {};
        policy = (policy != 'undefined' && policy != null && policy.replace(/[\/|\\]/gi, '') != '') ? policy.trim().replace(/[\/|\\]/gi, "") : '';
        branch = (branch != 'undefined' && branch != null && branch.replace(/[\/|\\]/gi, '') != '') ? branch.trim().replace(/[\/|\\]/gi, "") : '';
        client = (client != 'undefined' && client != null && client.replace(/[\/|\\]/gi, '') != '') ? client.trim().replace(/[\/|\\]/gi, "") : '';
        phone = (phone != 'undefined' && phone != null && phone.replace(/[\/|\\]/gi, '') != '') ? phone.trim().replace(/[\/|\\]/gi, "") : '';

        if (client.length > 0) {
            search.searchObject = client;
            search.criteria = '1';
            $scope.doPolicyFilterSearch(search);
        }
        else if (policy.length > 0) {
            search.searchObject = policy;
            search.criteria = '2';
            $scope.doPolicyFilterSearch(search);
        } else if (phone.length > 0) {
            search.searchObject = phone;
            search.criteria = '3';
            $scope.doPolicyFilterSearch(search);
        } else if (branch.length > 0) {
            search.searchObject = branch;
            search.criteria = '4';
            $scope.doPolicyFilterSearch(search);
        }

        var isEmptyFilter = true;
        this.grid.columns.forEach(function (column) {
            if (typeof column.filters[0].term !== 'undefined' && column.filters[0].term !== null && column.filters[0].term.length > 0) {
                isEmptyFilter = false;
                return;
            }
        });

        if (isEmptyFilter) {
            $scope.startPolicyPage = 0;
            $scope.getMorePolicy();
        }
    };

    $scope.doPolicyFilterSearch = function (searObject) {
        $scope.loading++;
        $http.post(
            '/do-policy-filter',
            searObject
        ).then(function (response) {
            $scope.loading--;
            $scope.allPolicyDash = response.data.payload;
        });
    };


    $scope.startPolicyPage = 1;
    $scope.getMorePolicy = function () {
        if ($scope.totalPolicyElements > $scope.allPolicyDash.length) {
            $scope.startPolicyPage++;
            $scope.loading++;
            $http.get('/get-all-policy/' + $scope.startPolicyPage)
                .then(function (data) {
                    $scope.loading--;
                    $scope.allPolicyDash = $scope.allPolicyDash.concat(data.data.payload.content);
                    $scope.totalPolicyElements = data.data.payload.totalElements;
                    $scope.gridDashOption.infiniteScroll.dataLoaded();
                });
        } else {
            $scope.gridDashOption.infiniteScroll.dataLoaded();
        }
    };

    $scope.clientList = new Array();
    $scope.policy = angular.copy(Data.policy);
    $scope.allPolicyDash = [];

    $scope.loading = 0;

    //AFTER LOAD
    $timeout(function () {

        $scope.loading++;
        $http.get('/get-all-clients-person').then(function (response) {
            if (response.data.status == true) {
                $scope.loading--;
                $scope.myData_person = response.data.payload;
                $scope.clientList = $scope.clientList.concat($scope.myData_person);
            }
        });
        $scope.loading++;
        $http.get('/get-all-clients-company').then(function (response) {
            if (response.data.status == true) {
                $scope.loading--;
                $scope.myData_company = response.data.payload;
                $scope.clientList = $scope.clientList.concat($scope.myData_company);
            }
        });

        $scope.loading++;
        $http.get('/get-all-policy/1')
            .then(function (data) {
                $scope.startPolicyPage = 1;
                $scope.loading--;
                $scope.allPolicyDash = data.data.payload.content;
                $scope.totalPolicyElements = data.data.payload.totalElements;
            });
        $scope.loading++;
        $http.get('/get-all-invoices/0')
            .then(function (response) {
                $scope.loading--;
                $scope.totalInvoiceElements = response.data.payload.totalElements;
                $scope.invoiceList = $scope.invoiceList.concat(response.data.payload.content);
            });
        $scope.reinit();
    }, 200);

    $scope.$watch('loading', function (value) {
        $scope.appSpinner = (value != 0);
    });
    $scope.invoiceList = [];
    $scope.reinit = function () {
        if (Data.invoice != null) {
            $scope.invReg = angular.copy(Data.invoice);
            $scope.saveData.client = angular.copy(Data.invoice.client);
            $scope.saveData.policy = angular.copy(Data.invoice.policy);
            $scope.$broadcast('angucomplete-alt:changeInput', 'invRegClient', Data.invoice.client);
            $scope.$broadcast('angucomplete-alt:changeInput', 'invRegPolicyLinked', Data.invoice.policy);
        }
        try {
            $scope.fireGoodsList = Data.policy.descriptions == null ? [] : Data.policy.descriptions;

            $scope.localList = angular.copy(Data.policy.locals);
            if (typeof Data.policy.goodss != 'undefined') {
                $scope.editMode = true;
                $scope.goodsList = angular.copy(Data.policy.goodss);
                $scope.cobGoodsList = [];
                Data.policy.goodss.forEach(function (goodLine) {
                    var cobGoodsList = $scope.selectedCoverts[goodLine.covert];
                    $scope.cobGoodsList.push(cobGoodsList);
                });
                $scope.$broadcast('angucomplete-alt:changeInput', 'clientName', Data.policy.client);
            }
        } catch (error) {
        }
    };

    $scope.editPolice = function (policy) {
        // $location.path('policy');
        $scope.appSpinner++;
        Data.editMode = true;
        $http.get(
            '/read-policy/' + policy.policyId
        ).then(function (data) {
            $scope.editMode = true;
            Data.policy = angular.copy(data.data.payload);
            Data.policy.branch = Data.deMap($scope.branches, Data.policy.branch);
            Data.policy.status = Data.deMap($scope.statuses, Data.policy.status);
            Data.policy.policer = Data.deMap($scope.policers, Data.policy.policer);
            Data.policy.currency = Data.deMap($scope.coins, Data.policy.currency);
            Data.policy.startDate = $scope.shortDate(Data.policy.startDate);
            Data.policy.endDate = $scope.shortDate(Data.policy.endDate);
            Data.policy.descriptions = JSON.parse(Data.policy.descriptions);
            Data.policyViewMode = true;
            $scope.appSpinner--;
            if ($scope.isHealth(Data.policy.branch)) {
                $location.path('/health');
            } else $location.path('/policy');
        });
    };

    $scope.isHealth = function (branch) {
        var isHealthy = false;
        $scope.branches.forEach(function (t) {
            if (t.branchId === branch && t.branchDescription.toLowerCase().indexOf("salud") !== -1) {
                isHealthy = true;
            }
        });
        return isHealthy;
    };
    $scope.setEndDate = function (startDate) {
        $scope.policy.endDate = startDate.substring(0, startDate.length - 4) + (Number(startDate.substring(startDate.length - 4)) + 1);
    };

    $scope.invoiceViewMode = false;

    $scope.cleanScreen = function () {
        Data.policy = null;
        Data.invoice = null;
        Data.editMode = false;
        Data.policyViewMode = false;
        Data.invoiceViewMode = false;
        $route.reload();
    };

    $scope.setDueDate = function (startDate) {
        $scope.invReg.dueDate = startDate.substring(0, startDate.length - 4) + (Number(startDate.substring(startDate.length - 4)) + 1);
    }
    $scope.editClient = function (row) {
        $scope.appSpinner++;
        $http.get(
            '/get-one/' + row.entity.clientId
        ).then(function (response) {
            $scope.clientViewMode = true;
            $scope.newClient = response.data.payload;
            $scope.appSpinner--;
        });
    };

    $scope.$watch('invReg.client', function (value) {
        try {
            $scope.updateLinkedPolicy($scope.invReg.client.originalObject);
        } catch (error) {
        }
    });

    $scope.updatedLinkedPolicies = [];

    $scope.updateLinkedPolicy = function (client) {
        var clientId = angular.copy(client).clientId;
        $http.get(
            '/get-by-client/' + clientId
        ).then(function (response) {
            $scope.updatedLinkedPolicies = response.data.payload;
        });
    };

    $scope.saveInvoice = function (invoice) {
        $scope.appSpinner++;
        invoice = angular.copy(invoice);
        invoice.client = angular.copy(invoice.client.originalObject);
        if (invoice.client == null)
            invoice.client = angular.copy($scope.saveData.client);

        var dateParts = invoice.startDate.split('/');
        invoice.startDate = new Date(dateParts[2], dateParts[1] - 1, dateParts[0]);
        dateParts = invoice.endDate.split('/');
        invoice.endDate = new Date(dateParts[2], dateParts[1] - 1, dateParts[0]);
        dateParts = invoice.beginningDate.split('/');
        invoice.beginningDate = new Date(dateParts[2], dateParts[1] - 1, dateParts[0]);
        dateParts = invoice.dueDate.split('/');
        invoice.dueDate = new Date(dateParts[2], dateParts[1] - 1, dateParts[0]);

        $http.post('/save-invoice', invoice)
            .then(function (response) {
                $scope.invoiceViewMode = false;
                $scope.invReg = {};
                $("#invRegClient").val('');
                $scope.$broadcast('angucomplete-alt:clearInput', 'invRegClient');
                $scope.saveData.policy = null;
                $scope.saveData.client = null;
                $scope.getAllInvoices();
                $scope.appSpinner--;
                Data.success("Completado");
                $scope.appSpinner++;
                Data.invoice = null;
                $route.reload();
            }, function (ignored) {
                Data.error("Algo Salio Mal");
            });
    };

    $scope.getAllInvoices = function () {
        $http.get('/get-all-invoices/0')
            .then(function (response) {
                $scope.totalInvoiceElements = response.data.payload.totalElements;
                $scope.invoiceList = $scope.invoiceList.concat(response.data.payload.content);
            });
    };

    $scope.shortDate = function (date) {
        var d = new Date(date);
        return ("0" + d.getDate()).slice(-2) + "/" + ("0" + (d.getMonth() + 1)).slice(-2) + "/" + d.getFullYear();
    };

    $scope.editMode = false;

    $scope.newDate = function (date) {
        if (!Data.editMode)
            return $scope.shortDate(new Date());
        else return date;
    };
    $scope.nextYearToday = function (date) {
        if (!Data.editMode) {
            var today = new Date();
            today.setYear(today.getFullYear() + 1);
            return $scope.shortDate(today);
        }
        return date;
    };

    $scope.getBkgColorInvoice = function (row) {
        var after30Days = new Date(Date.now() + (30 * 24 * 60 * 60 * 1000));
        var invoiceDate = new Date(row.entity.dueDate);
        var todayDate = new Date();
        if (invoiceDate.getTime() <= todayDate.getTime())
            return 'red';
        else if (after30Days.getTime() >= invoiceDate.getTime())
            return '#f4d742';
        return '';

        // return after30Days.getTime()>= invoiceDate.getTime() ? '#ef64ae' : '';
    };

    $scope.gridOptions_invoice = {
        rowTemplate: '<div ng-style="rowInvoiceStyle" ng-dblclick=\"grid.appScope.editInvoice(row)\" ' +
        'ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" ' +
        'class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader}"   ' +
        'ng-mouseover="rowInvoiceStyle={\'color\': \'#114ee8\',\'font-weight\' : \'bold\'}; grid.appScope.onRowHover(this);" ng-mouseleave="rowInvoiceStyle={}" ' +
        'style="background-color: {{grid.appScope.getBkgColorInvoice(row)}}" ui-grid-cell></div>',
        infiniteScrollRowsFromEnd: 40,
        rowHeight: 38,
        enableSearch: true,
        enableFiltering: true,
        infiniteScrollUp: true,
        infiniteScrollDown: true,
        columnDefs: [
            {
                name: 'invoiceNumber', displayName: 'Factura', width: '130', filter: {
                    noTerm: true,
                    condition: true
                }
            },
            {
                name: 'client.clientId', width: '220', displayName: 'Cliente', cellFilter: 'clientFilter', filter: {
                    noTerm: true,
                    condition: true
                }
            },
            {
                name: 'policy', displayName: 'Poliza', width: '120', cellFilter: 'policyFilter', filter: {
                    noTerm: true,
                    condition: true
                }
            },
            {
                name: 'dueDate',
                enableFiltering: false,
                displayName: 'Vencimiento',
                cellFilter: 'date:\'dd-MM-yyyy\'',
                type: 'date',
                width: '120'
            }
        ],
        data: 'invoiceList',
        onRegisterApi: function (gridApi) {
            $scope.gridInvoiceApi = gridApi;
            $scope.gridInvoiceApi.core.on.filterChanged($scope, $scope.doInvoiceFilter);
            gridApi.infiniteScroll.on.needLoadMoreData($scope, $scope.getMoreInvoices);
        }
    };
    $scope.saveData = {};
    $scope.startInvoicePage = 1;
    $scope.getMoreInvoices = function () {
        if ($scope.totalInvoiceElements > $scope.invoiceList.length) {
            $scope.loading++;
            $http.get('/get-all-invoices/' + $scope.startInvoicePage)
                .then(function (data) {
                    $scope.startInvoicePage++;
                    $scope.loading--;
                    $scope.invoiceList = $scope.invoiceList.concat(data.data.payload.content);
                    $scope.totalInvoiceElements = data.data.payload.totalElements;
                    $scope.gridInvoiceApi.infiniteScroll.dataLoaded();
                });
        } else {
            $scope.gridInvoiceApi.infiniteScroll.dataLoaded();
        }
    };

    $scope.doInvoiceFilter = function () {

        var client = this.grid.columns[1].filters[0].term;
        var policy = this.grid.columns[2].filters[0].term;
        var id = this.grid.columns[0].filters[0].term;
        var search = {};
        client = (client != 'undefined' && client != null && client.replace(/[\/|\\]/gi, '') != '') ? client.trim().replace(/[\/|\\]/gi, "") : '';
        policy = (policy != 'undefined' && policy != null && policy.replace(/[\/|\\]/gi, '') != '') ? policy.trim().replace(/[\/|\\]/gi, "") : '';
        id = (id != 'undefined' && id != null && id.replace(/[\/|\\]/gi, '') != '') ? id.trim().replace(/[\/|\\]/gi, "") : '';

        if (id.length > 0) {
            search.searchObject = id;
            search.criteria = '0';
            $scope.doInvoiceFilterSearch(search);
        } else if (client.length > 0) {
            search.searchObject = client;
            search.criteria = '1';
            $scope.doInvoiceFilterSearch(search);
        }
        else if (policy.length > 0) {
            search.searchObject = policy;
            search.criteria = '2';
            $scope.doInvoiceFilterSearch(search);
        }

        var isEmptyFilter = true;
        this.grid.columns.forEach(function (column) {
            if (typeof column.filters[0].term !== 'undefined' && column.filters[0].term !== null && column.filters[0].term.length > 0) {
                isEmptyFilter = false;
                return;
            }
        });

        if (isEmptyFilter) {
            search.searchObject = "";
            search.criteria = '1';
            $scope.doInvoiceFilterSearch(search);
        }
    };

    $scope.doInvoiceFilterSearch = function (searObject) {
        $scope.loading++;
        $http.post(
            '/do-invoice-filter',
            searObject
        ).then(function (response) {
            $scope.loading--;
            $scope.invoiceList = response.data.payload;
        });
    };

    $scope.editInvoice = function (row) {
        if ($location.path() == '/invoice') {
            $scope.appSpinner++;
            var clientId = row.entity.client.clientId;
            $http.get(
                '/get-by-client/' + clientId
            ).then(function (response) {
                $scope.invoiceViewMode = true;
                $scope.updatedLinkedPolicies = response.data.payload;
                $scope.invReg = angular.copy(row.entity);
                $scope.invReg.startDate = $scope.shortDate($scope.invReg.startDate);
                $scope.invReg.endDate = $scope.shortDate($scope.invReg.endDate);
                $scope.invReg.beginningDate = $scope.shortDate($scope.invReg.beginningDate);
                $scope.invReg.dueDate = $scope.shortDate($scope.invReg.dueDate);
                $scope.saveData.client = angular.copy(row.entity.client);
                $scope.$broadcast('angucomplete-alt:changeInput', 'invRegClient', $scope.invReg.client);
                $scope.appSpinner--;
            });

        } else {
            $scope.appSpinner++;
            Data.invoice = angular.copy(row.entity);
            Data.invoice.startDate = $scope.shortDate(Data.invoice.startDate);
            Data.invoice.endDate = $scope.shortDate(Data.invoice.endDate);
            Data.invoice.beginningDate = $scope.shortDate(Data.invoice.beginningDate);
            Data.invoice.dueDate = $scope.shortDate(Data.invoice.dueDate);
            Data.invoiceViewMode = true;
            $location.path('/invoice');
        }

    }
});