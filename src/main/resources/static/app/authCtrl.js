app.controller('authCtrl', function ($scope, $rootScope, $routeParams, $location, $http, Data, auth, $route, $window, $route) {
    //initially set those objects to null to avoid undefined error

    $scope.isMenuOpen = false;
    $scope.$on('$viewContentLoaded', function() {
        $("#navBar").css('visibility', 'visible');
        $scope.loginForm={};
        $rootScope.loginForm = {};
        $("#loginFormPassword").val('');
        $("#loginFormUser").val('');
    });


    $scope.cleanValue = function () {
        $scope.loginForm={};
        $rootScope.loginForm = {};
        $("#loginFormPassword").val('');
        $("#loginFormUser").val('');
    };
    $scope.doLogin = function (credentials) {

        auth.authenticate(credentials, function(authenticated) {
            if (authenticated) {
                console.log("Login succeeded")
                $scope.error = false;
            } else {
                console.log("Login failed")
                $scope.error = true;
            }
        });


        $scope.loginForm = {};
        $http.post('/sign-in', angular.copy(credentials)).
        then(function(response) {
            customer = {};
            if (response.data.status == true) {
                $scope.authenticated = true;
                if (response.data.payload.firstLogin == 'Y') {
                    response.data.message = "Debe cambiar contraseña primero";
                    $location.path('profile');
                }
                else {
                    $location.path('dashboard');
                }
            }
        });

    };

    $scope.reset = function () {
        Data.policy = null;
    };

    $scope.doLogout = function () {
        $http.get('/logout').
        then(function(response) {
            $("#loginFormPassword").val('');
            $("#loginFormUser").val('');
            $scope.authenticated = false;
            $scope.loginForm = {};
            $window.location.reload();
            $route.reload();
            Data.success("Completado");
        },function (error) {
            Data.error("Algo Salio Mal");
        });
    };

    $scope.$on('$routeChangeStart', function(next, current) {
        if ($location.path()=='/login'){
            $http.get('/session').then(function (response) {
                var results = response.data.payload;
                if (response.data.status == true ) {
                    $scope.authenticated = true;
                    $scope.userFirstName = results.firstName;
                    $scope.userId = results.userId;
                    $scope.username = results.username;
                    $scope.role = results.role;
                    $scope.firstLogin = results.firstLogin;

                }
                else {
                    $scope.authenticated = false;
                    // $location.path('/login');
                }
            });
        }
    });
});
