app.controller('cashCtrl', function ($scope, $rootScope, $routeParams, $location, $http, Data, $timeout, $route, $filter, uiGridConstants) {
    $scope.$on("$viewContentLoaded", function (event) {

        $scope.allIncomes = [];
        $scope.allUser = [];
        $http.get("all-accounts")
            .then(function (response) {
                $scope.allUser = response.data.payload;
            }, function (error) {

            });

        $scope.doUserSearch = function (filter) {
            var object = angular.copy(filter);
            object.startDate = object.startDate ? Data.parseToDate(object.startDate, "-") : Data.parseToDate($scope.newDate(), "-");
            object.endDate = object.endDate ? Data.parseToDate(object.endDate, "-") :  Data.parseToDate($scope.newDate(), "-");
            $http.post('/get-user-cashing',
                object).then(function (response) {
                $scope.allIncomes = angular.copy(response.data.payload);
            }, function (error) {
                
            })
        };

    });

    $scope.reinitComponents = function () {
        $route.reload();
    }

    $scope.newDate = function () {
         return $scope.shortDate(new Date());
    };

    $scope.shortDate = function (date) {
        var d = new Date(date);
        return ("0" + d.getDate()).slice(-2) + "-" + ("0"+(d.getMonth()+1)).slice(-2) + "-" +  d.getFullYear();
    };

    $scope.gridOptions = {
        rowHeight:32,
        enableRowSelection: true,
        enableRowHeaderSelection: false,
        multiSelect: false,
        modifierKeysToMultiSelect: false,
        noUnselect: true,
        enableHorizontalScrollbar : 1,
        enableFiltering: true,
        infiniteScrollDown: true,
        showColumnFooter: true,
        rowTemplate: '<div  ng-style="rowStyle" ng-dblclick=\"grid.appScope.editPolice(row.entity)\" ' +
        'ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" ' +
        'class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader}"' +
        'ng-mouseover="rowStyle={\'color\': \'#114ee8\',\'font-weight\' : \'bold\'}" ng-mouseleave="rowStyle={}" ' +
        'ui-grid-cell></div>',
        columnDefs: [
            { name:'policy', enableFiltering: true, displayName: 'Poliza', width: '12%', cellFilter: 'uppercase'},
            { name:'client', enableFiltering: true, displayName: 'Cliente', width: '24%', cellFilter: 'uppercase'},
            { name:'invoice', footerCellTemplate: '<div><label>Efec: {{grid.appScope.totalCashAmount}}</label></div>', enableFiltering: true, displayName: 'Factura', width: '12%', cellFilter: 'uppercase'},
            { name:'receipt', footerCellTemplate: '<div><label>Tarj: {{grid.appScope.totalCardAmount}}</label></div>', enableFiltering: true, displayName: 'Recibo', width: '12%', cellFilter: 'uppercase'},
            { name:'modified', footerCellTemplate: '<div><label>Cheq: {{grid.appScope.totalCheqAmount}}</label></div>', enableFiltering: false, displayName: 'Fecha de Creacion', width: '10%' ,type: 'date', cellFilter: 'date:\'dd-MM-yyyy\''  },
            { name:'amount', footerCellTemplate: '<div><label>Dep: {{grid.appScope.totalDepAmount}}</label></div>', enableFiltering: true, displayName: 'Monto', width: '10%' , cellFilter: 'currency'},
            { name:'paymentType', enableFiltering: true, displayName: 'Tipo De Pago', width: '10%', cellFilter: 'paymentType'},
            { name:'paymentMode', footerCellTemplate: '<div style="text-align: right"><label>Total: {{grid.appScope.totalAmount}}</label></div>', enableFiltering: true, displayName: 'Forma De Pago', width: '10%', cellFilter: 'paymentMode'},
            ],
        data: 'allIncomes',
        gridFooterTemplate: '<div><label>Efectivo: {{grid.appScope.totalAmount}}</label></div>',
        onRegisterApi: function(gridApi){
            $scope.gridDashOption = gridApi;
            $scope.gridDashOption.core.on.filterChanged($scope, $scope.doPolicyFilter);
            gridApi.infiniteScroll.on.needLoadMoreData($scope, $scope.getMorePolicy);
        }
    };

    $scope.totalAmount = $filter('currency')(0);
    $scope.totalCashAmount = $filter('currency')(0);
    $scope.totalCheqAmount = $filter('currency')(0);
    $scope.totalCardAmount = $filter('currency')(0);
    $scope.totalDepAmount = $filter('currency')(0);

    $scope.$watch('allIncomes', function (newVal) {
        var sum = 0;
        $scope.totalCashAmount = 0;
        $scope.totalCardAmount = 0;
        $scope.totalCheqAmount = 0;
        $scope.totalDepAmount = 0;

        newVal.forEach(function (income) {
            $scope.setAmountToType(income)
            sum+= Number(income.amount);
        });
        $scope.totalCashAmount = $filter('currency')($scope.totalCashAmount);
        $scope.totalCardAmount = $filter('currency')($scope.totalCardAmount);
        $scope.totalCheqAmount = $filter('currency')($scope.totalCheqAmount);
        $scope.totalDepAmount = $filter('currency')($scope.totalDepAmount);
        $scope.totalAmount = $filter('currency')(sum);
    });

    $scope.setAmountToType = function (receipt) {
        console.log(receipt.paymentMode)
        switch (receipt.paymentMode){
            case '0': $scope.totalCashAmount+=Number(receipt.amount);
            break;
            case '1': $scope.totalCardAmount+=Number(receipt.amount);
            break;
            case '2': $scope.totalCheqAmount+=Number(receipt.amount);
            break;
            case '3': $scope.totalDepAmount+=Number(receipt.amount);
            break;
            default:
                break;
        }
    }

});