app.controller('claimController', function ($scope, $rootScope, $routeParams, $location, $http, Data, $timeout, $route) {

    $scope.$on('$viewContentLoaded', function() {
        if ($scope.firstLogin == 'Y')
            $location.path('/profile')
        $http.get('/get-all-companies')
            .then(function (response) {
                $scope.policers = angular.copy(response.data.payload);
            });
        $http.get(
            'next-integrity-number'
        ).then(function (response) {
            try {
                if (typeof $scope.claim.documentNo === 'undefined')
                    $scope.claim.documentNo = response.data.payload.nextClaimNumber;
            } catch (error) {}
        });

    });
    $timeout(function () {

        $http.get('/get-all-clients-person').then(function (response) {
            if (response.data.status == true) {
                $scope.clientList = $scope.clientList.concat(response.data.payload);
            }
        });

        $http.get('/get-all-clients-company').then(function (response) {
            if (response.data.status == true) {
                $scope.clientList = $scope.clientList.concat(response.data.payload);
            }
        });
        // $http.get('/get-all-policy')
        //     .then(function (data) {
        //         $scope.allPolicyDash = data.data.payload;
        //     });
        $http.get('/get-all-invoices')
            .then(function (response) {
                $scope.invoiceList = response.data.payload;
            });
        $scope.getAllDocs();
    }, 200);

    $scope.validateClaimNumber = function (value) {
        $http.get('validate-claim/'+value)
            .then(function (response) {
                if (response.data.payload==true){
                    $scope.claimNumberStyle = {color:'black'};
                    $scope.disableClaimForm = false;
                }
                else {
                    $scope.claimNumberStyle = {color:'red'};
                    $scope.disableClaimForm = true;
                    Data.error("Numero de Reclamo ya registrado");
                }
            }, function (error) {
                Data.error("Algo salio mal");
            });
    };

    $scope.$watch('claim.client', function() {
        try {
            $scope.updateLinkedPolicy($scope.claim.client.originalObject);
        }catch (error){}
    });

    $scope.updateLinkedPolicy = function (client) {
        var clientId = angular.copy(client).clientId;
        $http.get(
            '/get-by-client/'+clientId
        ).then(function (response) {
            $scope.updatedLinkedPolicies = response.data.payload;
        });
    };

    $scope.saveClaim = function (document) {
        $scope.appSpinner = true;
        var claim = angular.copy(document);
        claim.client = angular.copy(claim.client.originalObject);

        if (claim.client == null)
            claim.client = angular.copy($scope.saveData.client);

        var dateParts = claim.createDate.split('/');
        claim.createDate = new Date(dateParts[2], dateParts[1] - 1, dateParts[0]);
        $http.post(
            '/save-claim-document',
            claim
        ).then(function (response) {
            $scope.appSpinner = false;
            $scope.getAllDocs();
            $scope.claim = {};
            $scope.saveData = {};
            Data.success("Completado");
            $route.reload();
        },function (error) {
            Data.error("Algo Salio Mal");
        });

    };
    $scope.getAllDocs = function () {
        $scope.appSpinner = true;
        $http.get('/get-all-claim')
            .then(function (response) {
                $scope.appSpinner = false;
                $scope.claimList = response.data.payload;
            });
    };

    $scope.editClaim = function (row) {
        $scope.claim = angular.copy(row.entity);
        $scope.claim.createDate = $scope.shortDate($scope.claim.createDate);
        $scope.$broadcast('angucomplete-alt:changeInput', 'claimClient', $scope.claim.client);
        $scope.saveData.client = angular.copy(row.entity.client);
        $scope.claimViewMode = true;
    };

    $scope.gridOptions_claim = {
        rowTemplate: '<div  ng-dblclick=\"grid.appScope.editClaim(row)\" ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader}"   ui-grid-cell></div>',
        infiniteScrollRowsFromEnd: 40,
        infiniteScrollUp: true,
        infiniteScrollDown: true,
        enableFiltering: true,
        columnDefs: [
            { name:'documentNo', displayName: 'Reclamo No.', width: '130', filter: {
                noTerm: true,
                condition: true
            }
            },
            { name:'client.clientId', width: '220', displayName: 'Cliente', cellFilter: 'clientFilter', filter: {
                noTerm: true,
                condition: true
            }
            },
            {
                name: 'policy', displayName: 'Poliza', width: '120', cellFilter: 'policyFilter', filter: {
                noTerm: true,
                condition: true
            }
            },
            { name:'createDate', displayName: 'Fecha de Creacion', cellFilter: 'date:\'dd-MM-yyyy\'', type: 'date', width: '120'}
        ],
        data: 'claimList',
        onRegisterApi: function(gridApi){
            $scope.gridInvoiceApi = gridApi;
            $scope.gridInvoiceApi.core.on.filterChanged($scope, $scope.doClaimFilter);
        }
    };

    $scope.doClaimFilter = function () {

        var id = this.grid.columns[0].filters[0].term;
        var client = this.grid.columns[1].filters[0].term;
        var policy = this.grid.columns[2].filters[0].term;
        var search = {};

        id = (id != 'undefined' && id != null && id.replace(/[\/|\\]/gi, '') != '') ? id.trim().replace(/[\/|\\]/gi, "") : '';
        client = (client != 'undefined' && client != null && client.replace(/[\/|\\]/gi, '') != '') ? client.trim().replace(/[\/|\\]/gi, "") : '';
        policy = (policy != 'undefined' && policy != null && policy.replace(/[\/|\\]/gi, '') != '') ? policy.trim().replace(/[\/|\\]/gi, "") : '';

        if (id.length>0){
            search.searchObject = id;
            search.criteria = '0';
            $scope.doClaimFilterSearch(search);
        }else if (client.length>0){
            search.searchObject = client;
            search.criteria = '1';
            $scope.doClaimFilterSearch(search);
        }
        else if (policy.length>0){
            search.searchObject = policy;
            search.criteria = '2';
            $scope.doClaimFilterSearch(search);
        }

        var isEmptyFilter = true;
        this.grid.columns.forEach(function (column) {
            if (typeof column.filters[0].term !== 'undefined' &&  column.filters[0].term !== null && column.filters[0].term.length>0 ){
                isEmptyFilter = false;
                return;
            }
        });

        if (isEmptyFilter){
            search.searchObject = "";
            search.criteria = '1';
            $scope.doClaimFilterSearch(search);
        }
    };

    $scope.doClaimFilterSearch = function (searObject) {
        $scope.appSpinner = true;
        $http.post(
            '/do-claim-filter',
            searObject
        ).then(function (response) {
            $scope.appSpinner = false;
            $scope.claimList = response.data.payload;
        });
    };


    $scope.shortDate = function (date) {
        var d = new Date(date);
        return  ("0" + d.getDate()).slice(-2) +  "/" +  ("0"+(d.getMonth()+1)).slice(-2) + "/" + d.getFullYear();
    };

    $scope.claimViewMode = false;
    $scope.cleanScreen = function () {
        $scope.claimViewMode = false;
        $route.reload();
    };

    //    OBJECTS DECLARATION
    $scope.invoiceList = [];
    $scope.allPolicyDash = [];
    $scope.updatedLinkedPolicies = [];
    $scope.clientList = [];
    $scope.claimList = [];
    $scope.saveData = {};
    $scope.policers = [];

});