app.controller('companyController', function ($scope, $rootScope, $routeParams, $location, $http, Data, $timeout, $route) {

    $scope.$on('$viewContentLoaded', function() {
        $scope.getAllCompanies();
    });

    $scope.getAllCompanies = function () {
        $scope.company = {};
        $scope.appSpinner = true;
        $http.get('/get-all-companies')
            .then(function (response) {
                $scope.companies = angular.copy(response.data.payload);
                $scope.appSpinner = false;
            });

    };
        $scope.gridOptions_phoneList = {
        rowTemplate: '<div  ng-dblclick=\"grid.appScope.editClaim(row)\" ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader}"   ui-grid-cell></div>',
        infiniteScrollRowsFromEnd: 40,
        enableHorizontalScrollbar : 0,
        infiniteScrollUp: true,
        enableFiltering: true,
        infiniteScrollDown: true,
        columnDefs: [
            { name:'phoneNumber', displayName: 'Telefono', cellFilter: 'tel', width: '180'},
            { name:'branch', displayName: 'Persona o Rama', width: '200'},
            { name:'extraInfo', displayName: 'Extra', width: '180', cellFilter: 'tel'},
            { name:'notes', displayName: 'Notas', width: '300' }
        ],
        data: 'phoneList',
        onRegisterApi: function(gridApi){

        }
    };

    $scope.phoneList = [];

    $scope.gridOptions_bankAccountList = {
        rowTemplate: '<div  ng-dblclick=\"grid.appScope.editClaim(row)\" ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader}"   ui-grid-cell></div>',
        infiniteScrollRowsFromEnd: 40,
        enableHorizontalScrollbar : 0,
        infiniteScrollUp: true,
        enableFiltering: true,
        infiniteScrollDown: true,
        columnDefs: [
            { name:'bankAccountNumber', displayName: 'Numero de Cuenta', width: '120'},
            { name:'bank', displayName: 'Banco', width: '100'},
            { name:'label', displayName: 'Sello', width: '80'},
            { name:'notes', displayName: 'Notas', width: '300' }
        ],
        data: 'bankAccountList',
        onRegisterApi: function(gridApi){

        }
    };

    $scope.gridOptions_company = {
        rowTemplate: '<div  ng-dblclick=\"grid.appScope.editCompany(row)\" ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader}"   ui-grid-cell></div>',
        infiniteScrollRowsFromEnd: 40,
        enableHorizontalScrollbar : 0,
        infiniteScrollUp: true,
        enableFiltering: true,
        infiniteScrollDown: true,
        columnDefs: [
            { name:'id', displayName: 'id', width: '80'},
            { name:'name', displayName: 'Nombre', width: '180'},
            { name:'code', displayName: 'Codigo', width: '100'},
            { name:'contact', displayName: 'Contacto', width: '260' }
        ],
        data: 'companies',
        onRegisterApi: function(gridApi){

        }
    };

    $scope.editCompany = function (row) {
        $scope.appSpinner = true;
        $http.get(
            '/get-company-by-id/'+ row.entity.id
        ).then(function (response) {
            $scope.companyViewMode = true;
            $scope.company = angular.copy(response.data.payload);
            $scope.bankAccountList = angular.copy(response.data.payload.bankAccountList);
            $scope.phoneList = angular.copy(response.data.payload.phoneList);
            $scope.appSpinner = false;
        });
    };

    $scope.companies = [];


    $scope.cleaner = function () {
        $scope.bankAccountCompany = {};
        $scope.phoneList = [];
        $scope.bankAccountList = [];
        $scope.phoneCompany = {};
        $scope.company = {};
        $scope.getAllCompanies();
    };

    $scope.bankAccountList = [];

    $scope.savePhone = function (object) {
        $scope.phoneList.push(angular.copy(object));
    };

    $scope.saveBankAccount = function (object) {
      $scope.bankAccountList.push(angular.copy(object));
    };


    $scope.labels = [
        {"name": "SI", "index": "1"},
        {"name": "NO", "index": "2"}
    ];

    $scope.createNewCompany = function (object) {

        var data = angular.copy(object);
        data.bankAccountList = angular.copy($scope.bankAccountList);
        data.phoneList = angular.copy($scope.phoneList);
        $scope.appSpinner = true;
        $http({
            method: "post",
            url: "/save-company",
            data: data
        }).then(function (response) {
            $scope.cleaner();
            $scope.appSpinner = false;
            Data.success("Completado");
            $route.reload();
        },function (error) {
            Data.error("Algo Salio Mal");
        });
    };

    $scope.companyViewMode = false;
    $scope.cleanScreen = function () {
        $scope.companyViewMode = false;
        $route.reload();
    };
});