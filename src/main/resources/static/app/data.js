app.factory("Data", ['$http', 'toaster', 'Notification',
    function ($http, toaster, Notification) { // This service connects to our REST API

        var serviceBase = '';

        var obj = {};
        obj.toast = function (data) {
            toaster.pop(data.status, "", data.message, 10000, 'trustedHtml');
        }
        obj.get = function (q) {
            return $http.get(serviceBase + q).then(function (results) {
                return results.data;
            });
        };
        obj.post = function (q, object) {
            return $http.post(serviceBase + q, object).then(function (results) {
                return results.data;
            });
        };
        obj.put = function (q, object) {
            return $http.put(serviceBase + q, object).then(function (results) {
                return results.data;
            });
        };
        obj.delete = function (q) {
            return $http.delete(serviceBase + q).then(function (results) {
                return results.data;
            });
        };

        obj.policyViewMode = false;
        obj.invoiceViewMode = false;

        obj.map = function (object, index) {
            object.forEach(function (pos) {
                if (pos.index == index){
                    index = pos.name;
                    return;
                }
            });
            return index;
        };

        obj.deMap = function (object, index) {
            object.forEach(function (pos) {
                if (pos.name == index){
                    index = pos.index;
                    return;
                }
            });
            return index;
        };

        obj.error = function (message){
            Notification.error({message: message, positionY: 'bottom', positionX: 'left'});
        };

        obj.success = function (message){
                Notification.success({message: message, positionY: 'bottom', positionX: 'left'});
        };

        obj.parseToDate = function (dateObject, splitter) {
            splitter = splitter ? splitter : "/";
            var dateParts = dateObject.split(splitter);
            return new Date(dateParts[2], dateParts[1] - 1, dateParts[0]);
        };

        obj.dataTable =[];
        obj.dataClientInfo = {};

        obj.policy = null;

        obj.editModo = false;

        obj.invoice = null;
        obj.clientList = [];

        return obj;
}]);