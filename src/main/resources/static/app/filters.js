/**
 * Created by Jose A Rodriguez on 7/20/2017.
 */
app.filter('tel', function () {
    return function (tel) {
        if (!tel) { return ''; }

        var value = tel.toString().trim().replace(/^\+/, '');

        if (value.match(/[^0-9]/)) {
            return tel;
        }

        var country, city, number;

        if (value.length==10){
            country='';
            city = value.slice(0, 3);
            number = value.slice(3);
        }
        else {
            city = value.slice(0, 3);
            number = value.slice(3, 10);
            country = ' Ext. '+value.slice(10);
        }
        if (country == 1) {
            country = "";
        }

        number = number.slice(0, 3) + '-' + number.slice(3);

        return (" (" + city + ") " + number + country).trim();
    };
});

app.filter('entities', function () {
    return function (entity) {
        policers.forEach(function (pol) {
            if (pol.id == entity){
                entity= pol.name;
                return;
            }
        });
        return entity.toUpperCase();
    }
});

app.filter('roles', function () {
    return function (rol) {
        return roles[rol-1].name;
    }
});

app.filter('policy', function () {
    return function (policyId) {

    }
});

app.filter('clientFilter', function () {
    return function (clientId) {
        allClients.forEach(function (client) {
            if (client.clientId == clientId){
                clientId = client.fullName;
                return;
            }
        });
        return clientId;
    }
})