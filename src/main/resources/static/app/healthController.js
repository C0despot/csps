app.controller('healthController', function ($scope, $rootScope, $routeParams, $location, $http, Data, $timeout, $route) {

    $scope.clientList = [];
    $scope.loading = 0;
    $scope.policyViewMode = false;

    $scope.$on('$locationChangeSuccess', function (event, newUrl, oldUrl) {
        Data.policyViewMode = false;
        Data.editMode = false;
    });
    $scope.$on('$viewContentLoaded', function () {
        $scope.policyViewMode = Data.policyViewMode;
        $scope.getBranches();

        $scope.loading++;
        $http.get('/get-all-clients-person').then(function (response) {
            if (response.data.status == true) {
                $scope.loading--;
                $scope.clientList = $scope.clientList.concat(response.data.payload);
            }
        });
        $scope.loading++;
        $http.get('/get-all-clients-company').then(function (response) {
            if (response.data.status == true) {
                $scope.loading--;
                $scope.clientList = $scope.clientList.concat(response.data.payload);
            }
        });
        $scope.loading++;
        $http.get('/get-all-companies')
            .then(function (response) {
                $scope.loading--;
                $scope.policers = angular.copy(response.data.payload);
            });
        $scope.loading++;
        $http.get(
            'next-integrity-number'
        ).then(function (response) {
            $scope.loading--;
            try {
                if (typeof $scope.policy.policyNumber === 'undefined')
                    $scope.policy.policyNumber = response.data.payload.nextPolicyNumber;
            } catch (error) {}
        });

        if (Data.policy !== null) {
            setTimeout(function () {
                $scope.$broadcast('angucomplete-alt:changeInput', 'clientName', Data.policy.client);
                $scope.policy = angular.copy(Data.policy);
                $scope.dependenciesList = angular.copy(Data.policy.dependentList);
                $scope.$apply();
            }, 400);
        }
    });
    $scope.cleanScreen = function () {
        Data.policy = null;
        Data.invoice = null;
        Data.editMode = false;
        Data.policyViewMode = false;
        $route.reload();
    };

    $scope.$watch('loading', function(value) {
        $scope.appSpinner = (value != 0) ;
    });

    $scope.newDate = function (date) {
        if(!Data.editMode)
            return $scope.shortDate(new Date());
        else return date;
    };
    $scope.nextYearToday = function (date) {
        if(!Data.editMode) {
            var today = new Date();
            today.setYear(today.getFullYear() + 1)
            return $scope.shortDate(today);
        }
        return date;
    };
    $scope.shortDate = function (date) {
        var d = new Date(date);
        return  ("0" + d.getDate()).slice(-2) +  "/" +  ("0"+(d.getMonth()+1)).slice(-2) + "/" + d.getFullYear();
    };

    $scope.statuses = [
        {"name": "Vigente", "index": "1"},
        {"name": "No Vigente", "index": "2"},
        {"name": "Cancelada", "index": "3"}
    ];
    $scope.coins = [
        {"name": "RD $ [PESO]", "index": "1"},
        {"name": "US $ [DOLAR]", "index": "2"},
        {"name": "EU € [EURO]", "index": "3"}
    ];
    $scope.sexs = [
        {"name": "MASCULINO", "index": "M"},
        {"name": "FEMENINO", "index": "F"}
    ];

    $scope.types = [
        {"name": "DEPENDIENTE", "index": "0"},
        {"name": "TITULAR", "index": "1"}
    ];
    $scope.dependence_grid = {
        rowTemplate: '<div  ng-dblclick=\"grid.appScope.toggleModal(row, grid.renderContainers.body.visibleRowCache.indexOf(row))\" ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader}"   ui-grid-cell></div>',
        enableColumnResizing: true,
        saveFocus: false,
        saveScroll: false,
        saveSelection: false,
        enableGridMenu: true,
        columnDefs: [
            { name: '  ', displayName: '  ', cellTemplate: '<div class="ui-grid-cell-contents" style="text-align: center;">{{grid.renderContainers.body.visibleRowCache.indexOf(row)+1}}</div>', width:'5%'},
            { name: ' ', displayName: ' ', width:'5%', cellTemplate: '<div style="text-align: center; margin-top: 6px;"><a data-ng-click="grid.appScope.sliceDependent(row);"><i class="fa fa-trash-o" aria-hidden="true"></i></a></div>'},
            { name:'filialNum', displayName: 'Numero de Afiliado', width: '15%' },
            { name:'identification', displayName: 'Identificacion', width: '10%' },
            { name:'name', displayName: 'Nombre', width: '25%'},
            { name:'parent', displayName: 'Parentesco', width: '10%' },
            { name:'sex', displayName: 'Sexo', width: '5%'},
            { name:'birthDate', displayName: 'Nacimiento', width: '10%', cellFilter: 'date:\'dd-MM-yyyy\''},
            { name:'age', displayName: 'Edad', width: '5%' },
            { name:'startDate', displayName: 'Inicio de Vigencia', width: '10%', cellFilter: 'date:\'dd-MM-yyyy\''},
            { name:'planType', displayName: 'Tipo de Plan', width: '10%'},
            { name: 'value', displayName: 'Valor', width: '10%', cellFilter: 'currency'}
        ],
        data: 'dependenciesList',
        onRegisterApi: function(gridApi){
            $scope.grid = gridApi;
        }
    };

    $scope.saveDependent = function (dependent) {

        try {
            var dateParts = dependent.startDate.split('/');
            dependent.startDate = new Date(dateParts[2], dateParts[1] - 1, dateParts[0]);
        }catch (e){}
        try {
            dateParts = dependent.birthDate.split('/');
            dependent.birthDate = new Date(dateParts[2], dateParts[1] - 1, dateParts[0]);
        }catch (e){}
        if($scope.editGoodFlag === false) {
            $scope.dependenciesList.push(dependent);
        }
        else {
            $scope.dependenciesList[$scope.rowIndex]= angular.copy(dependent);
        }
    };

    $scope.addDependentCleaner = function () {
        $scope.editGoodFlag = false;
        $scope.dependent = {};
    };
    $scope.doSavePolicy = function (healthDoc) {
        $scope.loading++;
        healthDoc = angular.copy(healthDoc);
        var dateParts = healthDoc.startDate.split('/');
        healthDoc.startDate = new Date(dateParts[2], dateParts[1] - 1, dateParts[0]);
        dateParts = healthDoc.endDate.split('/');
        healthDoc.endDate = new Date(dateParts[2], dateParts[1] - 1, dateParts[0]);
        if (typeof healthDoc.client.originalObject === 'undefined'){
            healthDoc.client = angular.copy(Data.policy.client);
        }else {
            healthDoc.client = healthDoc.client.originalObject;
        }

        $scope.branches.forEach(function (t) {
           if (t.branchDescription.toLowerCase().indexOf("salud") !== -1){
               healthDoc.branch = t.branchId;
           }
        });
        $http.post(
            '/save-policy',
            angular.copy(healthDoc)
        ).then(function (response) {
            var id = angular.copy(response.data.payload).policyId;
            $scope.loading--;
            saveDependencies(id);
        }, function (error) {

        });
    };

    $scope.branches = [];
    $scope.getBranches = function () {
        $http.get('/get-all-branches')
            .then(function (response) {
                $scope.appSpinner--;
                $scope.branches = response.data.payload;
            }, function (error) {

            });
    };

    $scope.rowIndex = 0;
    $scope.editGoodFlag = false;
    $scope.dependent = {};
    $scope.toggleModal = function (row, index) {
        var dependent = angular.copy(row.entity);
        dependent.startDate = $scope.shortDate(dependent.startDate);
        dependent.birthDate = $scope.shortDate(dependent.birthDate);
        $scope.rowIndex = index;
        $scope.dependent = dependent;
        $scope.editGoodFlag = true;
        $("#dependenciesModal").modal();
    };

    var saveDependencies = function (linkedId) {
        var dependencyList = [];
        $scope.loading++;
        $scope.dependenciesList.forEach(function (dependency) {
            dependency.policyLinked = linkedId;
            dependencyList.push(dependency);
        });
        $http.post(
            '/save-dependencies',
            angular.copy(dependencyList)
        ).then(function (response) {
            Data.success("Documento salvado")
            $scope.policy = {};
            $scope.dependenciesList = [];
            $scope.loading--;
            Data.policy = null;
            Data.editMode = false;
            $location.path('/invoice');
            $route.reload();
        }, function (error) {
            Data.error("Algo salio mal")
        });
    };
    $scope.dependenciesList = [];

    $scope.sliceDependent = function (row) {

        $.SmartMessageBox({
            title: "<i class='fa fa-trash-o' style='color:red'></i> Borrar Registro?",
            content: "Esta seguro que desea eliminar el registro seleccionado?",
            buttons: '[No][Yes]'
        }, function (ButtonPressed) {
            if (ButtonPressed == "Yes") {
                var count = 0;
                $scope.dependenciesList.forEach(function (good) {
                    if (good.dependentId == row.entity.dependentId) {
                        $http.delete('/delete-one-record/'+good.dependentId)
                            .then(function (response) {
                                if (response.data.status === true) {
                                    Data.success(response.data.message);
                                    delete $scope.dependenciesList[count];
                                }
                            });
                        return;
                    }
                    count++;
                });
            }
        });

    };
});