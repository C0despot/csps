app.controller('movController', function ($scope, $rootScope, $routeParams, $location, $http, Data, $timeout, $route) {

    $scope.$on('$viewContentLoaded', function() {
        if ($scope.firstLogin == 'Y')
            $location.path('/profile')

        $http.get(
            'next-integrity-number'
        ).then(function (response) {
            try {
                if (typeof $scope.receipt.receiptNo === 'undefined')
                    $scope.receipt.receiptNo = response.data.payload.nextReceiptNumber;
            } catch (error) {}
            try {
                if (typeof $scope.credit.documentNo === 'undefined')
                    $scope.credit.documentNo = response.data.payload.nextCreditNumber;
            } catch (error) {}
        });
    });
    $scope.creditViewMode = false;
    $scope.cleanScreen = function () {
        $scope.receiptEditMode = false;
        $scope.receiptViewMode = false;
        $scope.creditViewMode = false;
        $route.reload();
    };

    $scope.validateReceiptNumber = function (value) {
        $http.get('validate-receipt/'+value)
            .then(function (response) {
                if (response.data.payload==true){
                    $scope.receiptNumberStyle = {color:'black'};
                    $scope.disableReceiptForm = false;
                }
                else {
                    $scope.receiptNumberStyle = {color:'red'};
                    $scope.disableReceiptForm = true;
                    Data.error("Numero de Recibo ya registrado");
                }
            }, function (error) {
                Data.error("Algo salio mal");
            });
    };

    $scope.validateCreditNumber = function (value) {
        $http.get('validate-credit/'+value)
            .then(function (response) {
                if (response.data.payload==true){
                    $scope.creditNumberStyle = {color:'black'};
                    $scope.disableCreditForm = false;
                }
                else {
                    $scope.creditNumberStyle = {color:'red'};
                    $scope.disableCreditForm = true;
                    Data.error("Numero de Documento ya registrado");
                }
            }, function (error) {
                Data.error("Algo salio mal");
            });
    };

    $scope.loading--;
    $scope.saveReceipt = function (receipt) {
        receipt = angular.copy(receipt);
        receipt.client = receipt.client.originalObject;
        // receipt.policy = receipt.policy.originalObject;
        var dateParts = receipt.startDate.split('/');
        receipt.startDate = new Date(dateParts[2], dateParts[1] - 1, dateParts[0]);
        dateParts = receipt.endDate.split('/');
        receipt.endDate = new Date(dateParts[2], dateParts[1] - 1, dateParts[0]);
        if (receipt.policy == null)
            receipt.policy = angular.copy($scope.saveData.policy);
        if (receipt.client == null)
            receipt.client = angular.copy($scope.saveData.client);
        $scope.loading++;
        $http.post(
            '/save-receipt',
            receipt
        ).then(function (response) {
            if (response.data.status) {
                $scope.getAllReceipts();
                $scope.$broadcast('angucomplete-alt:clearInput', 'rClient');
                $scope.receipt = {};
                $scope.invoiceTotalAmount = '0';
                $scope.saveData.policy = null;
                $scope.saveData.client = null;
                $scope.loading--;
                Data.success("Completado");
                $scope.receiptEditMode = false;
                $route.reload();
            }else {
                Data.error("Debe digitar un monto valido");
                $scope.loading--;
            }
        },function (error) {
            Data.error("Algo Salio Mal");
        });
    };
    $scope.receiptEditMode = false;

    $scope.verifyAmount = function (a, b) {
        var x = angular.copy(a);
        var y = angular.copy(b);
        x = Math.round(x * 100) / 100;
        y = Math.round(y * 100) / 100;
        if(x < y){
            $scope.receipt.amount = '0';
            Data.error("El monto excede el valor adeudado");
        }
    }
    $scope.saveCredit = function (document) {
        $scope.loading++;
        var credit = angular.copy(document);
        credit.client = angular.copy(credit.client.originalObject);
        if (credit.client == null)
            credit.client = angular.copy($scope.saveData.client);

        var dateParts = credit.startDate.split('/');
        credit.startDate = new Date(dateParts[2], dateParts[1] - 1, dateParts[0]);
        dateParts = credit.endDate.split('/');
        credit.endDate = new Date(dateParts[2], dateParts[1] - 1, dateParts[0]);
        dateParts = credit.beginningDate.split('/');
        credit.beginningDate = new Date(dateParts[2], dateParts[1] - 1, dateParts[0]);
        dateParts = credit.dueDate.split('/');
        credit.dueDate = new Date(dateParts[2], dateParts[1] - 1, dateParts[0]);
        $http.post(
            '/save-document',
            credit
        ).then(function (response) {
            if (response.data.status) {
                $scope.creditViewMode = false;
                $scope.getAllDocs();
                $scope.credit = {};
                $scope.saveData = {};
                $scope.$broadcast('angucomplete-alt:clearInput', 'creditClient');
                $scope.loading--;
                Data.success("Completado");
                $scope.creditEditMode = false;
                $route.reload();
            }else{
                Data.error("Debe digitar un monto valido");
                $scope.loading--;
            }
        },function (error) {
            Data.error("Algo Salio Mal");
        });

    };

    $scope.invoiceTotalAmount = '0';
    $scope.lookForTotalAmount = function (invoice) {
        $scope.loading++;
        $http.get(
            '/get-invoice-by-invoice-number/'+ invoice
        ).then(function (response) {
            $scope.loading--;
            if(response.data.status)
                $scope.invoiceTotalAmount = response.data.payload.totalPrime;
            else $scope.invoiceTotalAmount = '0';
        });
    };

    $scope.lookForTotalAmountCredit = function (invoice) {
        $scope.loading++;
        $http.get(
            '/get-invoice-by-invoice-number/'+ invoice
        ).then(function (response) {
            $scope.loading--;
            if(response.data.status)
                $scope.credit.prime = response.data.payload.totalPrime;
            else $scope.credit.prime = '0';
        });
    };

    $scope.clientList = [];
    //AFTER LOAD
    $timeout(function () {
        $scope.loading++;
        $http.get('/get-all-clients-person').then(function (response) {
            if (response.data.status == true) {
                var myData_person = response.data.payload;
                $scope.loading--;
                $scope.clientList = $scope.clientList.concat(myData_person);
            }
        });

        $scope.loading++;
        $http.get('/get-all-clients-company').then(function (response) {
            if (response.data.status == true) {
                $scope.loading--;
                var myData_company = response.data.payload;
                $scope.clientList = $scope.clientList.concat(myData_company);
            }
        });
        $scope.getAllDocs();
        $scope.getAllReceipts();
    }, 200);

    $scope.updatedLinkedPolicies = null;

    $scope.$watch('receipt.client', function(value) {
        try {
            $scope.updateLinkedPolicy(value.originalObject);
        }catch (error){}
    });

    $scope.$watch('credit.client', function(value) {
        try {
            $scope.updateLinkedPolicy(value.originalObject);
        }catch (error){}
    });

    $scope.updateLinkedPolicy = function (client) {
        var clientId = angular.copy(client).clientId;
        $http.get(
            '/get-by-client/'+clientId
        ).then(function (response) {
            $scope.updatedLinkedPolicies = response.data.payload;
        });
    };

    $scope.shortDate = function (date) {
        var d = new Date(date);
        return  ("0" + d.getDate()).slice(-2) +  "/" +  ("0"+(d.getMonth()+1)).slice(-2) + "/" + d.getFullYear();
    };

    $scope.receiptId = null;
    $scope.submitDeletion= function(password){
        $scope.loading++;
        $http.delete("delete/receipt/"+$scope.receiptId+"/password/" + password)
            .then(function (response) {
                $scope.loading--;
                Data.success("El Recibo Fue Borrado Correctamente");
                setTimeout(function(){location.reload();}, 1000);
            }, function (error) {
                $scope.loading--;
                Data.error("Error inesperado, revise contraseña.")
            });
    };

    $scope.deleteReceipt = function (row) {
        $scope.receiptId = row.entity.id;
    };
    $scope.gridOptions_receipt = {
        rowTemplate: '<div  ng-dblclick=\"grid.appScope.editReceipt(row)\" ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader}"   ui-grid-cell></div>',
        infiniteScrollRowsFromEnd: 40,
        rowHeight: 38,
        // enableHorizontalScrollbar : 0,
        infiniteScrollUp: true,
        infiniteScrollDown: true,
        enableFiltering: true,
        columnDefs: [
            {name: '    ', enableFiltering: false, width: '80', displayName: '', cellTemplate: '<div style="text-align: center"><i data-toggle="modal" data-target="#confirmPasswordModal" ng-click="grid.appScope.deleteReceipt(row)"  style="padding-top: 6px; font-size: 24px" class="fa fa-trash-o text-danger"></i></div>'},
            { name: 'receiptNo', width: '120', displayName: 'Recibo', filter: {
                noTerm: true,
                condition: true
            }
            },
            { name:'client.clientId', width: '220', displayName: 'Cliente', cellFilter: 'clientFilter', filter: {
                noTerm: true,
                condition: true
            }
            },
            { name:'policy', displayName: 'Poliza', width: '120', cellFilter: 'policyFilter', filter: {
                noTerm: true,
                condition: true
            }
            },
            { name:'endDate', displayName: 'Vencimiento', width: '120', cellFilter: 'date:\'dd-MM-yyyy\'', type: 'date' }
        ],
        data: 'receiptList',
        onRegisterApi: function(gridApi){
            $scope.gridReceiptApi = gridApi;
            $scope.gridReceiptApi.core.on.filterChanged($scope, $scope.doReceiptFilter);
            gridApi.infiniteScroll.on.needLoadMoreData($scope, $scope.getMoreReceipts);
        }
    };

    $scope.startReceiptPage = 0;
    $scope.totalReceiptElements = 0;
    $scope.getMoreReceipts = function () {
        if ($scope.totalReceiptElements > $scope.receiptList.length) {
            $scope.loading++;
            $http.get('/get-all-receipt/'+$scope.startReceiptPage)
                .then(function (response) {
                    $scope.startReceiptPage++;
                    $scope.loading--;
                    $scope.receiptList = $scope.receiptList.concat(response.data.payload.content);
                    $scope.totalReceiptElements = response.data.payload.totalElements;
                    $scope.gridReceiptApi.infiniteScroll.dataLoaded();
                });
        }else {
            $scope.gridReceiptApi.infiniteScroll.dataLoaded();
        }
    };


    $scope.doReceiptFilter = function () {

        var client = this.grid.columns[1].filters[0].term;
        var policy = this.grid.columns[2].filters[0].term;
        var id = this.grid.columns[0].filters[0].term;
        var search = {};
        client = (client != 'undefined' && client != null && client.replace(/[\/|\\]/gi, '') != '') ? client.trim().replace(/[\/|\\]/gi, "") : '';
        policy = (policy != 'undefined' && policy != null && policy.replace(/[\/|\\]/gi, '') != '') ? policy.trim().replace(/[\/|\\]/gi, "") : '';
        id = (id != 'undefined' && id != null && id.replace(/[\/|\\]/gi, '') != '') ? id.trim().replace(/[\/|\\]/gi, "") : '';

        if (id.length>0){
            search.searchObject = id;
            search.criteria = '0';
            $scope.doReceiptFilterSearch(search);
        }else if (client.length>0){
            search.searchObject = client;
            search.criteria = '1';
            $scope.doReceiptFilterSearch(search);
        }
        else if (policy.length>0){
            search.searchObject = policy;
            search.criteria = '2';
            $scope.doReceiptFilterSearch(search);
        }

        var isEmptyFilter = true;
        this.grid.columns.forEach(function (column) {
           if (typeof column.filters[0].term !== 'undefined' &&  column.filters[0].term !== null && column.filters[0].term.length>0 ){
               isEmptyFilter = false;
               return;
           }
        });

        if (isEmptyFilter){
            search.searchObject = "";
            search.criteria = '1';
            $scope.getAllReceipts(search);
        }
    };


    $scope.doReceiptFilterSearch = function (searObject) {
        $scope.loading++;
        $http.post(
            '/do-receipt-filter',
            searObject
        ).then(function (response) {
            $scope.loading--;
            $scope.receiptList = response.data.payload;
        });
    };

    $scope.saveData = {};

    $scope.editReceipt = function (row) {

        $scope.loading++;
        var clientId = row.entity.client.clientId;

        $http.get(
            '/get-by-policy/'+row.entity.policy
        ).then(function (response) {
            $scope.updatedLinkedInvoiceNumber = angular.copy(response.data.payload);
            $http.get(
                '/get-by-client/' + clientId
            ).then(function (response) {
                $scope.loading--;
                $scope.updatedLinkedPolicies = response.data.payload;
                $scope.receipt = angular.copy(row.entity);
                $scope.receipt.endDate = $scope.shortDate($scope.receipt.endDate);
                $scope.receipt.startDate = $scope.shortDate($scope.receipt.startDate);
                $scope.lookForTotalAmount(row.entity.invoiceNumber);
                $scope.$broadcast('angucomplete-alt:changeInput', 'rClient', $scope.receipt.client);
                $scope.receiptViewMode = true;
                $scope.receiptEditMode = true;
                $scope.saveData.client = angular.copy(row.entity.client);
            });
        });
    };

    $scope.updatedLinkedclient = [];

    $scope.updateInvoiceList = function (policy) {
        $http.get(
            '/get-by-policy/'+policy
        ).then(function (response) {
            $scope.updatedLinkedInvoiceNumber = angular.copy(response.data.payload);
        })
    };

    $scope.updatedLinkedInvoiceNumber = [];
    $scope.editCredit = function (row) {
        $scope.loading++;
        var clientId = row.entity.client.clientId;
        $http.get(
            '/get-by-policy/'+row.entity.policy
        ).then(function (response) {
            $scope.updatedLinkedInvoiceNumber = response.data.payload;
            $timeout(function () {
                $http.get(
                    '/get-by-client/' + clientId
                ).then(function (response) {
                    $scope.creditViewMode = true;
                    $scope.creditEditMode = true;
                    $scope.loading--;
                    $scope.updatedLinkedPolicies = angular.copy(response.data.payload);
                    $scope.credit = angular.copy(row.entity);
                    $scope.credit.endDate = $scope.shortDate($scope.credit.endDate);
                    $scope.credit.startDate = $scope.shortDate($scope.credit.startDate);
                    $scope.credit.dueDate = $scope.shortDate($scope.credit.dueDate);
                    $scope.credit.beginningDate = $scope.shortDate($scope.credit.beginningDate);
                    $scope.$broadcast('angucomplete-alt:changeInput', 'creditClient', $scope.credit.client);
                    $scope.saveData.client = angular.copy(row.entity.client);
                });
            }, 100);
        });
    };
    $scope.loading = 0;
    $scope.$watch('loading', function(value) {
        $scope.movSpinner = (value != 0) ;
    });
    $scope.getAllReceipts = function () {
        $http.get('/get-all-receipt/0')
            .then(function (response) {
                $scope.startReceiptPage = 1;
                // $scope.loading--;
                $scope.receiptList = response.data.payload.content;
                $scope.totalReceiptElements = response.data.payload.totalElements;
            });
    };

    $scope.getAllDocs = function () {
        $scope.loading++;
        $http.get('/get-all-credit')
            .then(function (response) {
                $scope.loading--;
                $scope.creditList = response.data.payload;
            });
    };
    $scope.editMode = false;

    $scope.creditEditMode = false;

    $scope.newDate = function (date) {
        if(!$scope.editMode)
            return $scope.shortDate(new Date());
        else return date;
    };
    $scope.nextYearToday = function (date) {
        if(!$scope.editMode) {
            var today = new Date();
            today.setYear(today.getFullYear() + 1)
            return $scope.shortDate(today);
        }
        return date;

    };

    $scope.creditList = [];

    $scope.gridOptions_credit = {
        rowTemplate: '<div  ng-dblclick=\"grid.appScope.editCredit(row)\" ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader}"   ui-grid-cell></div>',
        infiniteScrollRowsFromEnd: 40,
        infiniteScrollUp: true,
        infiniteScrollDown: true,
        enableFiltering: true,
        columnDefs: [
            { name: 'documentNo', displayName: 'Documento No.', width: '130', cellFilter: 'uppercase', filter: {
                noTerm: true,
                condition: true
            }
            },
            { name:'client.clientId', width: '220', displayName: 'Cliente', cellFilter: 'clientFilter', filter: {
                noTerm: true,
                condition: true
            }
            },
            {
                name: 'policy', displayName: 'Poliza', width: '120', cellFilter: 'policyFilter', filter: {
                noTerm: true,
                condition: true
            }
            },
            { name:'dueDate', displayName: 'Vencimiento', cellFilter: 'date:\'dd-MM-yyyy\'', type: 'date', width: '120' }
        ],
        data: 'creditList',
        onRegisterApi: function(gridApi){
            $scope.gridCreditApi = gridApi;
            $scope.gridCreditApi.core.on.filterChanged($scope, $scope.doCreditFilter);
        }
    };

    $scope.doCreditFilter = function () {
        var id = this.grid.columns[0].filters[0].term;
        var client = this.grid.columns[1].filters[0].term;
        var policy = this.grid.columns[2].filters[0].term;
        var search = {};
        id = (id != 'undefined' && id != null && id.replace(/[\/|\\]/gi, '') != '') ? id.trim().replace(/[\/|\\]/gi, "") : '';
        client = (client != 'undefined' && client != null && client.replace(/[\/|\\]/gi, '') != '') ? client.trim().replace(/[\/|\\]/gi, "") : '';
        policy = (policy != 'undefined' && policy != null && policy.replace(/[\/|\\]/gi, '') != '') ? policy.trim().replace(/[\/|\\]/gi, "") : '';

        if (id.length>0){
            search.searchObject = id;
            search.criteria = '0';
            $scope.doCreditFilterSearch(search);
        } else if (client.length>0){
            search.searchObject = client;
            search.criteria = '1';
            $scope.doCreditFilterSearch(search);
        }
        else if (policy.length>0){
            search.searchObject = policy;
            search.criteria = '2';
            $scope.doCreditFilterSearch(search);
        }

        var isEmptyFilter = true;
        this.grid.columns.forEach(function (column) {
            if (typeof column.filters[0].term !== 'undefined' &&  column.filters[0].term !== null && column.filters[0].term.length>0 ){
                isEmptyFilter = false;
                return;
            }
        });

        if (isEmptyFilter){
            search.searchObject = "";
            search.criteria = '1';
            $scope.doCreditFilterSearch(search);
        }
    };
    $scope.doCreditFilterSearch = function (searObject) {
        $scope.loading++;
        $http.post(
            '/do-credit-filter',
            searObject
        ).then(function (response) {
            $scope.loading--;
           $scope.creditList = response.data.payload;
        });
    };



    $scope.receiptList = [];
    $scope.payments = [
        {"name": "Efectivo", "index": "0"},
        {"name": "Tarjeta", "index": "1"},
        {"name": "Cheque", "index": "2"},
        {"name": "Deposito", "index": "3"}
    ];

    $scope.creditTypes = [
        {"name": "Cancelacion", "index": "0"},
        {"name": "Disminucion", "index": "1"},
        {"name": "Descuento", "index": "2"},
        {"name": "Ajuste", "index": "3"}
    ];

    $scope.paymentTypes = [
        {"name": "Inicial", "index": "0"},
        {"name": "Total", "index": "3"},
        {"name": "Abono", "index": "1"},
        {"name": "Saldo", "index": "2"}
    ];

    $scope.coins = [
        {"name": "RD $ [PESO]", "index": "1"},
        {"name": "US $ [DOLAR]", "index": "2"},
        {"name": "EU € [EURO]", "index": "3"}
    ];

    $scope.criterias = [
        // {"name": "Recibo", "index": "0"},
        {"name": "Cliente", "index": "1"},
        {"name": "Poliza", "index": "2"},
        {"name": "Factura", "index": "3"}
    ];

});