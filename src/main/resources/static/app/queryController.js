app.controller('queryController', function ($scope, $rootScope, $routeParams, $location, $http, Data, $timeout, $route) {
    $scope.clientList = [];
    $scope.$on('$viewContentLoaded', function () {
        $http.get('/get-all-clients').then(function (response) {
                $scope.clientList = response.data.payload;
        });
        if (Data.dataTable.length > 0){
            data = Data.dataTable;
            createTable();
        }
    });

    $scope.$on('$locationChangeSuccess', function (event) {
        Data.dataTable = [];
        $route.reload();
    });
    $scope.$watch('clientToFilter', function (value) {
       if (typeof value !== 'undefined'){
           Data.dataClientInfo = angular.copy(value.originalObject);
           $scope.doSearch(value.originalObject.clientId);
       }
    });

    $scope.$watch('currentClientToFilter', function (value) {
       if (typeof value !== 'undefined'){
           Data.dataClientInfo = angular.copy(value.originalObject);
           $scope.doCurrentSearch(value.originalObject.clientId);
       }
    });

    $scope.doSearch = function (clientId) {
        $http.get("get-client-state/"+clientId)
            .then(function (response) {
                Data.dataTable = angular.copy(response.data.payload);
                data = angular.copy(response.data.payload);
                $route.reload();
            }, function (error) {

            })
    };
    $scope.doCurrentSearch = function (clientId) {
        $http.get("get-current-client-state/"+clientId)
            .then(function (response) {
                Data.dataTable = angular.copy(response.data.payload);
                data = angular.copy(response.data.payload);
                $route.reload();
            }, function (error) {

            })
    };

    $scope.shortDate = function (date) {
        var d = new Date(date);
        return ("0" + d.getDate()).slice(-2) + "/" + ("0"+(d.getMonth()+1)).slice(-2) + "/" +  d.getFullYear();
    };

    // Bootstrap Table - subtable example with real data, forked from http://jsfiddle.net/myvykf24/1/ and created for https://github.com/wenzhixin/bootstrap-table/issues/2121

    var data = [];
    var $table = $('#table');
    function operateFormatter(value, row, index) {
        return [
            '<a class="print" title="Print">',
            '<i class="glyphicon glyphicon-print"></i>',
            '</a>'
        ].join('');
    }
    window.operateEvents = {
        'click .print': function (e, value, row, index) {
            $scope.pdfExport(row)
        }
    };

    function createTable () {

        $table.bootstrapTable({
            columns: [{
                field: 'policy',
                title: 'Poliza'
            }, {
                field: 'client',
                title: 'Cliente'
            }, {
                field: 'startDate',
                title: 'Inicio Vigencia'
            }, {
                field: 'endDate',
                title: 'Fin Vigencia'
            }, {
                field: 'balance',
                title: 'Balance Poliza'
            }, {
                field: 'currency',
                title: 'Moneda'
            }, {
                field: 'operate',
                title: ' ',
                align: 'center',
                events: operateEvents,
                formatter: operateFormatter
            }],
            data: data,
            // exportTypes: ['pdf', 'csv'],
            // exportDataType: 'all',
            formatNoMatches: function () {
                return 'NO HAY REGISTROS PARA ESTE CLIENTE';
            },
            exportOptions: {
                fileName: 'custom_file_name'
            },
            detailView: true,
            onExpandRow: function(index, row, $detail) {
                $detail.html('<table></table>').find('table').bootstrapTable({
                    columns: [{
                        field: 'type',
                        title: 'Tipo Documento'
                    }, {
                        field: 'document',
                        title: 'Documento'
                    }, {
                        field: 'modified',
                        title: 'Fecha Movimiento'
                    }, {
                        field: 'startDate',
                        title: 'Inicio Vigencia'
                    }, {
                        field: 'endDate',
                        title: 'Fin Vigencia'
                    }, {
                        field: 'balance',
                        title: 'Balance Facturado'
                    }, {
                        field: 'debts',
                        title: 'Balance Pendiente'
                    }],
                    data: row.invoiceDocuments,
                    formatNoMatches: function () {
                        return 'NO HAY REGISTROS PARA ESTA POLIZA';
                    },
                    // Simple contextual, assumes all entries have further nesting
                    // Just shows example of how you might differentiate some rows, though also remember row class and similar possible flags
                    detailView: typeof row.invoiceDocuments[0] != 'undefined' ,
                    onExpandRow: function(indexb, rowb, $detailb) {
                        $detailb.html('<table></table>').find('table').bootstrapTable({
                            columns: [{
                                field: 'type',
                                title: 'Tipo Documento'
                            }, {
                                field: 'document',
                                title: 'Documento'
                            }, {
                                field: 'modified',
                                title: 'Fecha Movimiento'
                            }, {
                                field: 'credit',
                                title: 'Creditos'
                            }],
                            data: rowb.creditDocuments,
                            formatNoMatches: function () {
                                return 'NO HAY REGISTROS PARA ESTA FACTURA';
                            }
                        });
                    }
                });


            }
        });
    }

    $scope.columns = [
        {title: "Num. Poliza", dataKey: "policy" },
        {title: "Inicio Vigencia", dataKey: "startDate" },
        {title: "Fin Vigencia", dataKey: "endDate" },
        {title: "Balance", dataKey: "balance" },
        {title: "Moneda", dataKey: "currency" }
    ];

    $scope.columns2 = [
        {title: "Tipo de Doc.", dataKey: "type" },
        {title: "Doc. No.", dataKey: "document" },
        {title: "Fecha Mov", dataKey: "modified" },
        {title: "Inicio Vig", dataKey: "startDate" },
        {title: "Fin Vig", dataKey: "endDate" },
        {title: "Facturado", dataKey: "balance" },
        {title: "Pendiente", dataKey: "debts" }
    ];
    $scope.columns3 = [
        {title: "Tipo de Doc.", dataKey: "type" },
        {title: "Documento No.", dataKey: "document" },
        {title: "Fecha Movimiento", dataKey: "modified" },
        {title: "Monto Acreditado", dataKey: "credit" }
        ];
    $scope.pdfExport = function (data) {
        var myWindow = window.open();
        doPrint(null, myWindow, [data]);
    };

    var doPrint = function (object, x, printeableData) {
        var enterprise = "Estado De cuenta Cliente";
        var dateString = $scope.shortDate(new Date());
        var company = printeableData[0].company;
        var clientName = Data.dataClientInfo.fullName;
        var clientAddress = Data.dataClientInfo.address;
        var clientPhones = Data.dataClientInfo.phoneNumber +" / " + Data.dataClientInfo.phoneNumber1 + " / " + Data.dataClientInfo.phoneNumber2;

        // var doc = new jsPDF("landscape");
        var doc = new jsPDF();
        var textWidth = doc.getStringUnitWidth(("Estado De cuenta Cliente").toUpperCase()) * doc.internal.getFontSize() / doc.internal.scaleFactor;
        var textOffset = (doc.internal.pageSize.width - dateString.length);
        var center = doc.internal.pageSize.width/2;
        doc.text(15, 20, (company).toUpperCase());
        doc.setFontSize(12);
        doc.text(center - 35, 35, ("Estado De cuenta Cliente").toUpperCase());
        doc.setFontSize(9);
        doc.text(textOffset-20, 18, (dateString+"").toUpperCase());
        doc.text(15, 45, "Asegurado :    "+ clientName.toUpperCase());
        doc.text(15, 50, "Direccion :      "+ clientAddress.toUpperCase());
        doc.text(15, 55, "Telefonos :     "+ clientPhones.toUpperCase());
        doc.setLineWidth(0.5);
        printeableData.forEach(function (row) {
            var rowData = [row];
            doc.autoTable($scope.columns, rowData, {theme: 'grid',startY: doc.autoTableEndPosY() > 0 ? doc.autoTableEndPosY(): 65, startX: 20, headerStyles: {
                lineWidth: 0,
                valign:'top',
                fontStyle: 'bold',
                halign: 'left',    //'center' or 'right'
                fillColor: [51, 51, 51],
                textColor: [255, 255, 255], //White
                fontSize: 10,
                cellPadding: {vertical: 0.5},
            }, bodyStyles: {
                fontSize: 9,
                cellPadding: {vertical: 0.5},
                lineColor: [0, 0, 0]
            }
            });
            row.invoiceDocuments.forEach(function (secondLevelrowData) {
                doc.autoTable($scope.columns2, [secondLevelrowData], {
                    startY: doc.autoTableEndPosY(), startX: '60', theme: 'grid', margin: {left: 20, vertical: 50}, headerStyles: {
                        lineWidth: 0,
                        valign: 'top',
                        fontStyle: 'bold',
                        halign: 'left',    //'center' or 'right'
                        fillColor: [84, 149, 255],
                        textColor: [255, 255, 255], //White
                        fontSize: 10,
                        cellPadding: {vertical: 0.5}
                    }, bodyStyles: {
                        fontSize: 9,
                        cellPadding: {vertical: 0.5},
                        lineColor: [0, 0, 0]
                    }
                });

                if (secondLevelrowData.creditDocuments.length>0) {
                    doc.autoTable($scope.columns3, secondLevelrowData.creditDocuments, {
                        startY: doc.autoTableEndPosY(), startX: '60', theme: 'grid', margin: {left: 26}, headerStyles: {
                            lineWidth: 0,
                            valign: 'top',
                            fontStyle: 'bold',
                            halign: 'left',    //'center' or 'right'
                            fillColor: [237, 132, 244],
                            textColor: [255, 255, 255], //White
                            fontSize: 10,
                            cellPadding: {vertical: 0.5},
                        }, bodyStyles: {
                            fontSize: 9,
                            cellPadding: {vertical: 0.5},
                            lineColor: [0, 0, 0]
                        }
                    });
                }
            });

        });
        doc.setProperties({
            title: 'Polizas - PDF'
        });
        var string = doc.output('datauristring');

        var iframe = "<iframe width='100%' height='100%' src='" + string + "'></iframe>"

        x.document.open();
        x.document.title = 'Polizas - PDF';
        x.document.write(iframe);
        x.document.close();
    }

    var generateDashPdfData = function (object) {
        var anotherObject = [];
        object.forEach(function (obj) {
            var data = {};
            data.fullName = $filter('clientFilter')(obj.client.fullName);
            data.policer = $filter('entities')(obj.policer);
            data.phone = obj.client.phoneNumber;
            data.policyNumber = obj.policyNumber;
            data.endDate = $scope.shortDate(obj.endDate);
            anotherObject.push(data);
        });
    }

});