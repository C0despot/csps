
/**
 * Created by Jose A Rodriguez on 4/28/2017.
 */

$(document).ready(function () {

    $('ul.nav li.dropdown').hover(function() {
        $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(500);
    }, function() {
        $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(500);
    });

    $("#logout").click(function () {
        $.ajax({
            //The URL to process the request
            'url' : 'api/v1/logout',
            //The type of request, also known as the "method" in HTML forms
            //Can be 'GET' or 'POST'
            'type' : 'GET',
            //The response from the server
            'success' : function(data) {
            }
        });
    });

    $( '.dp-class' ).hover(
        function(){
            $(this).children('.sub-menu').slideDown(200);
        },
        function(){
            $(this).children('.sub-menu').slideUp(200);
        }
    );

});

(function($){
    $(document).ready(function(){
        $('ul.dropdown-menu [data-toggle=dropdown]').on('click', function(event) {
            event.preventDefault();
            event.stopPropagation();
            $(this).parent().siblings().removeClass('open');
            $(this).parent().toggleClass('open');
        });
    });
})(jQuery);